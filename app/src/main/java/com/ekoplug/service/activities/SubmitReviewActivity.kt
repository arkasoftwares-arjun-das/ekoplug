package com.ekoplug.service.activities

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.TextView
import androidx.annotation.Nullable
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.ekoplug.model.MyBookingModel
import com.ekoplug.service.R
import com.ekoplug.service.basesetup.BaseResponseModel
import com.ekoplug.service.databinding.ActivitySubmitReviewBinding
import com.ekoplug.service.viewmodel.SubmitReviewViewModel
import com.ekoplug.utils.AppPreference
import com.ekoplug.utils.Utility
import com.ekoplug.utils.Watting

class SubmitReviewActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var submitReviewFragmentBinding: ActivitySubmitReviewBinding
    private lateinit var viewModel: SubmitReviewViewModel
    private var mWaitingDialog: Watting? = null
    var token = ""
    var userId = ""
    var workerId = ""
    var prefrenceSelecting = 0
    var ratingsValue = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        submitReviewFragmentBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_submit_review)
        mWaitingDialog = Watting(this, "")
        userId = intent!!.getStringExtra("userId")
        token = AppPreference.getPreferenceValueByKey(this, "Token").toString()
        workerId = intent!!.getStringExtra("workerId")
        init()
        callApi()
    }

    private fun init() {
        submitReviewFragmentBinding.etRatingComment.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                submitReviewFragmentBinding.etRatingComment.setBackgroundResource(R.drawable.login_textview_blue)
                submitReviewFragmentBinding.etRatingComment.setTextColor(resources.getColor(R.color.colorPrimary))
                submitReviewFragmentBinding.etRatingComment.setHintTextColor(resources.getColor(R.color.colorPrimary))
            } else {
                submitReviewFragmentBinding.etRatingComment.setBackgroundResource(R.drawable.login_textview_grey)
                submitReviewFragmentBinding.etRatingComment.setTextColor(resources.getColor(R.color.grey))
                submitReviewFragmentBinding.etRatingComment.setHintTextColor(resources.getColor(R.color.grey))
            }
        }
        submitReviewFragmentBinding.etRatingCommentTwo.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                submitReviewFragmentBinding.etRatingCommentTwo.setBackgroundResource(R.drawable.login_textview_blue)
                submitReviewFragmentBinding.etRatingCommentTwo.setTextColor(resources.getColor(R.color.colorPrimary))
                submitReviewFragmentBinding.etRatingCommentTwo.setHintTextColor(resources.getColor(R.color.colorPrimary))
            } else {
                submitReviewFragmentBinding.etRatingCommentTwo.setBackgroundResource(R.drawable.login_textview_grey)
                submitReviewFragmentBinding.etRatingCommentTwo.setTextColor(resources.getColor(R.color.grey))
                submitReviewFragmentBinding.etRatingCommentTwo.setHintTextColor(resources.getColor(R.color.grey))
            }
        }
        submitReviewFragmentBinding.rbSubmit.setOnRatingBarChangeListener { ratingBar, rating, fromUser ->
            ratingsValue = rating.toString()
        }
        submitReviewFragmentBinding.ivSubmitReviewBack.setOnClickListener(this)
        submitReviewFragmentBinding.btnSubmitReview.setOnClickListener(this)
        submitReviewFragmentBinding.prefrenceOne.setOnClickListener(this)
        submitReviewFragmentBinding.prefrenceTwo.setOnClickListener(this)
        submitReviewFragmentBinding.prefrenceThree.setOnClickListener(this)
        submitReviewFragmentBinding.prefrenceFour.setOnClickListener(this)
        submitReviewFragmentBinding.prefrenceFive.setOnClickListener(this)
        submitReviewFragmentBinding.prefrenceSix.setOnClickListener(this)
        submitReviewFragmentBinding.prefrenceSeven.setOnClickListener(this)
        submitReviewFragmentBinding.prefrenceEight.setOnClickListener(this)
        submitReviewFragmentBinding.prefrenceNine.setOnClickListener(this)
        submitReviewFragmentBinding.prefrenceTen.setOnClickListener(this)
    }

    private fun callApi() {
        viewModel = ViewModelProviders.of(this).get(SubmitReviewViewModel::class.java)
        viewModel.getReviewData().observe(
            this,
            object : Observer<BaseResponseModel<MyBookingModel>> {
                override
                fun onChanged(@Nullable serviceProviderModel: BaseResponseModel<MyBookingModel>?) {
                    mWaitingDialog!!.dismiss()
                    if (serviceProviderModel != null) {
                        if (serviceProviderModel.getCode() == 200) {
                            Utility.showSnackBar(
                                submitReviewFragmentBinding.tvRatingMsg,
                                serviceProviderModel.getMessage(),
                                this@SubmitReviewActivity
                            )
                            Handler().postDelayed({
                                onBackPressed()
                            }, 1000)
                        } else {
                            Utility.showSnackBar(
                                submitReviewFragmentBinding.tvRatingMsg,
                                serviceProviderModel.getMessage(),
                                this@SubmitReviewActivity
                            )
                        }
                    } else {
                        Utility.showSnackBar(
                            submitReviewFragmentBinding.tvRatingMsg,
                            getString(R.string.server_error),
                            this@SubmitReviewActivity
                        )
                    }
                }
            })

        viewModel.error.observe(this, Observer {
            mWaitingDialog!!.dismiss()
            Utility.showSnackBar(
                submitReviewFragmentBinding.tvRatingMsg, viewModel.error.value.toString(),
                this@SubmitReviewActivity
            )
        })
    }

    override fun onBackPressed() {
        super.onBackPressed()
        startActivity(Intent(this@SubmitReviewActivity, HomeActivity::class.java))
        finish()
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.iv_submitReviewBack -> {
                onBackPressed()
            }
            R.id.btnSubmitReview -> {
                btnSendData()
            }
            R.id.prefrenceOne -> {
                selectPrefrence(submitReviewFragmentBinding.prefrenceOne, 1)
            }
            R.id.prefrenceTwo -> {
                selectPrefrence(submitReviewFragmentBinding.prefrenceTwo, 2)
            }
            R.id.prefrenceThree -> {
                selectPrefrence(submitReviewFragmentBinding.prefrenceThree, 3)
            }
            R.id.prefrenceFour -> {
                selectPrefrence(submitReviewFragmentBinding.prefrenceFour, 4)
            }
            R.id.prefrenceFive -> {
                selectPrefrence(submitReviewFragmentBinding.prefrenceFive, 5)
            }
            R.id.prefrenceSix -> {
                selectPrefrence(submitReviewFragmentBinding.prefrenceSix, 6)
            }
            R.id.prefrenceSeven -> {
                selectPrefrence(submitReviewFragmentBinding.prefrenceSeven, 7)
            }
            R.id.prefrenceEight -> {
                selectPrefrence(submitReviewFragmentBinding.prefrenceEight, 8)
            }
            R.id.prefrenceNine -> {
                selectPrefrence(submitReviewFragmentBinding.prefrenceNine, 9)
            }
            R.id.prefrenceTen -> {
                selectPrefrence(submitReviewFragmentBinding.prefrenceTen, 10)
            }
        }
    }

    private fun selectPrefrence(prefrence: TextView, prefrenceRatings: Int) {
        if (prefrenceSelecting == 1) {
            submitReviewFragmentBinding.prefrenceOne.setBackgroundResource(R.drawable.rectangle_review)
            submitReviewFragmentBinding.prefrenceOne.setTextColor(resources.getColor(R.color.grey))
        } else if (prefrenceSelecting == 2) {

            submitReviewFragmentBinding.prefrenceTwo.setTextColor(resources.getColor(R.color.grey))
            submitReviewFragmentBinding.prefrenceTwo.setBackgroundResource(R.drawable.rectangle_review)
        } else if (prefrenceSelecting == 3) {

            submitReviewFragmentBinding.prefrenceThree.setTextColor(resources.getColor(R.color.grey))
            submitReviewFragmentBinding.prefrenceThree.setBackgroundResource(R.drawable.rectangle_review)
        } else if (prefrenceSelecting == 4) {

            submitReviewFragmentBinding.prefrenceFour.setTextColor(resources.getColor(R.color.grey))
            submitReviewFragmentBinding.prefrenceFour.setBackgroundResource(R.drawable.rectangle_review)
        } else if (prefrenceSelecting == 5) {

            submitReviewFragmentBinding.prefrenceFive.setTextColor(resources.getColor(R.color.grey))
            submitReviewFragmentBinding.prefrenceFive.setBackgroundResource(R.drawable.rectangle_review)
        } else if (prefrenceSelecting == 6) {

            submitReviewFragmentBinding.prefrenceSix.setTextColor(resources.getColor(R.color.grey))
            submitReviewFragmentBinding.prefrenceSix.setBackgroundResource(R.drawable.rectangle_review)
        } else if (prefrenceSelecting == 7) {

            submitReviewFragmentBinding.prefrenceSeven.setTextColor(resources.getColor(R.color.grey))
            submitReviewFragmentBinding.prefrenceSeven.setBackgroundResource(R.drawable.rectangle_review)
        } else if (prefrenceSelecting == 8) {

            submitReviewFragmentBinding.prefrenceEight.setTextColor(resources.getColor(R.color.grey))
            submitReviewFragmentBinding.prefrenceEight.setBackgroundResource(R.drawable.rectangle_review)
        } else if (prefrenceSelecting == 9) {

            submitReviewFragmentBinding.prefrenceNine.setTextColor(resources.getColor(R.color.grey))
            submitReviewFragmentBinding.prefrenceNine.setBackgroundResource(R.drawable.rectangle_review)
        } else if (prefrenceSelecting == 10) {

            submitReviewFragmentBinding.prefrenceTen.setTextColor(resources.getColor(R.color.grey))
            submitReviewFragmentBinding.prefrenceTen.setBackgroundResource(R.drawable.rectangle_review)
        }
        prefrenceSelecting = prefrenceRatings
        prefrence.setBackgroundResource(R.drawable.rectangle_review_blue)
        prefrence.setTextColor(resources.getColor(R.color.white))
        if (prefrenceRatings == 10) {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse("market://details?id=com.android.app")
            try {
                startActivity(intent)
            } catch (e: Exception) {
                intent.data =
                    Uri.parse("https://play.google.com/store/apps/details?id=com.android.app")
            }
        }
    }

    private fun btnSendData() {
        if (ratingsValue.equals("")) {
            Utility.showSnackBar(
                submitReviewFragmentBinding.tvRatingMsg, "Please select the ratings",
                this@SubmitReviewActivity
            )
        } else if (prefrenceSelecting == 0) {
            Utility.showSnackBar(
                submitReviewFragmentBinding.tvRatingMsg,
                "Please select ratings about app reference",
                this@SubmitReviewActivity
            )
        } else {
            mWaitingDialog!!.show()
            viewModel.setReviewData(
                token,
                workerId.toInt(),
                userId.toInt(),
                ratingsValue,
                submitReviewFragmentBinding.etRatingComment.text.toString(),
                submitReviewFragmentBinding.etRatingCommentTwo.text.toString(),
                prefrenceSelecting
            )
        }
    }
}
