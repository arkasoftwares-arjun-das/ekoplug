package com.ekoplug.service.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import com.ekoplug.service.R
import com.ekoplug.service.databinding.ActivitySettingsBinding

class SettingsActivity : AppCompatActivity() {

    lateinit var activitySettings : ActivitySettingsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activitySettings = DataBindingUtil.setContentView(this,R.layout.activity_settings)

    }

    fun backSettingsBtn(view: View) {
        finish()
    }

    fun aboutUsBtn(view: View) {
        startActivity(Intent(this@SettingsActivity,AboutUsActivity::class.java))
    }

    fun contactUsBtn(view: View) {
        startActivity(Intent(this@SettingsActivity,ContactUsActivity::class.java))

    }

    fun privacyPolicyBtn(view: View) {
        startActivity(Intent(this@SettingsActivity,PrivacyPolicyActivity::class.java))

    }

    fun termsConditioonBtn(view: View) {
        startActivity(Intent(this@SettingsActivity,TermsConditionActivity::class.java))

    }

}
