package com.ekoplug.service.activities

import android.app.TimePickerDialog
import android.os.Bundle
import android.os.Handler
import android.text.TextUtils
import android.view.View
import androidx.annotation.Nullable
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.ekoplug.model.UpdateProfileMdel
import com.ekoplug.service.R
import com.ekoplug.service.basesetup.BaseResponseModel
import com.ekoplug.service.databinding.ActivityEstimationBinding
import com.ekoplug.service.viewmodel.EstimateViewModel
import com.ekoplug.utils.AppPreference
import com.ekoplug.utils.Utility
import com.ekoplug.utils.Watting
import java.text.SimpleDateFormat
import java.util.*

class EstimationActivity : AppCompatActivity() {

    lateinit var estimationActivityBinding: ActivityEstimationBinding
    lateinit var viewModel: EstimateViewModel
    private var mWaitingDialog: Watting? = null
    private var mHour: Int = 0
    private var mMinute: Int = 0
    var bookingId = 0
    var token = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        estimationActivityBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_estimation)
        mWaitingDialog = Watting(this, "")
        token = AppPreference.getPreferenceValueByKey(this, "Token").toString()
        bookingId = intent.getIntExtra("bookingId", 0)
        init()
        callApi()
    }

    private fun callApi() {
        viewModel = ViewModelProviders.of(this).get(EstimateViewModel::class.java)

        viewModel.getEstimationData()
            .observe(this, object :
                Observer<BaseResponseModel<UpdateProfileMdel>> {
                override
                fun onChanged(@Nullable estimationModel: BaseResponseModel<UpdateProfileMdel>?) {
                    mWaitingDialog!!.dismiss()
                    if (estimationModel != null) {
                        if (estimationModel.getCode() == 200) {
                            Utility.showSnackBar(
                                estimationActivityBinding.tvEstimation,
                                estimationModel.getMessage(),
                                this@EstimationActivity
                            )
                            Handler().postDelayed({
                                finish()
                            }, 1000)
                        } else {
                            Utility.showSnackBar(
                                estimationActivityBinding.tvEstimation,
                                estimationModel.getMessage(),
                                this@EstimationActivity
                            )
                        }
                    } else {
                        Utility.showSnackBar(
                            estimationActivityBinding.tvEstimation,
                            getString(R.string.server_error),
                            this@EstimationActivity
                        )
                    }
                }
            })
        viewModel.error.observe(this, Observer {
            mWaitingDialog!!.dismiss()
            Utility.showSnackBar(
                estimationActivityBinding.tvEstimation, viewModel.error.value.toString(),
                this@EstimationActivity
            )
        })
    }


    private fun init() {
        estimationActivityBinding.etDetailsAboutWork.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                estimationActivityBinding.etDetailsAboutWork.setCompoundDrawablesWithIntrinsicBounds(
                    ContextCompat.getDrawable(
                        this,
                        R.drawable.job_type_blue
                    ),
                    null,
                    null,
                    null
                )
                estimationActivityBinding.etDetailsAboutWork.setBackgroundResource(
                    R.drawable.login_textview_blue
                )
                estimationActivityBinding.etDetailsAboutWork.setTextColor(
                    resources.getColor(
                        R.color.colorPrimary
                    )
                )
                estimationActivityBinding.etDetailsAboutWork.setHintTextColor(
                    resources.getColor(
                        R.color.colorPrimary
                    )
                )

            } else {
                estimationActivityBinding.etDetailsAboutWork.setCompoundDrawablesWithIntrinsicBounds(
                    ContextCompat.getDrawable(
                        this,
                        R.drawable.job_type_grey
                    ),
                    null,
                    null,
                    null
                )
                estimationActivityBinding.etDetailsAboutWork.setBackgroundResource(
                    R.drawable.login_textview_grey
                )
                estimationActivityBinding.etDetailsAboutWork.setTextColor(
                    resources.getColor(
                        R.color.grey
                    )
                )
                estimationActivityBinding.etDetailsAboutWork.setHintTextColor(
                    resources.getColor(
                        R.color.grey
                    )
                )
            }
        }
        estimationActivityBinding.etTimeDuration.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                estimationActivityBinding.etTimeDuration.setCompoundDrawablesWithIntrinsicBounds(
                    ContextCompat.getDrawable(
                        this,
                        R.drawable.time_blue
                    ),
                    null,
                    null,
                    null
                )
                estimationActivityBinding.etTimeDuration.setBackgroundResource(
                    R.drawable.login_textview_blue
                )
                estimationActivityBinding.etTimeDuration.setTextColor(
                    resources.getColor(
                        R.color.colorPrimary
                    )
                )
                estimationActivityBinding.etTimeDuration.setHintTextColor(
                    resources.getColor(
                        R.color.colorPrimary
                    )
                )

            } else {
                estimationActivityBinding.etTimeDuration.setCompoundDrawablesWithIntrinsicBounds(
                    ContextCompat.getDrawable(
                        this,
                        R.drawable.time_grey
                    ),
                    null,
                    null,
                    null
                )
                estimationActivityBinding.etTimeDuration.setBackgroundResource(
                    R.drawable.login_textview_grey
                )
                estimationActivityBinding.etTimeDuration.setTextColor(
                    resources.getColor(
                        R.color.grey
                    )
                )
                estimationActivityBinding.etTimeDuration.setHintTextColor(
                    resources.getColor(
                        R.color.grey
                    )
                )
            }
        }
        estimationActivityBinding.etCostDuration.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                estimationActivityBinding.etCostDuration.setCompoundDrawablesWithIntrinsicBounds(
                    ContextCompat.getDrawable(
                        this,
                        R.drawable.cost_blue
                    ),
                    null,
                    null,
                    null
                )
                estimationActivityBinding.etCostDuration.setBackgroundResource(
                    R.drawable.login_textview_blue
                )
                estimationActivityBinding.etCostDuration.setTextColor(
                    resources.getColor(
                        R.color.colorPrimary
                    )
                )
                estimationActivityBinding.etCostDuration.setHintTextColor(
                    resources.getColor(
                        R.color.colorPrimary
                    )
                )

            } else {
                estimationActivityBinding.etCostDuration.setCompoundDrawablesWithIntrinsicBounds(
                    ContextCompat.getDrawable(
                        this,
                        R.drawable.cost_grey
                    ),
                    null,
                    null,
                    null
                )
                estimationActivityBinding.etCostDuration.setBackgroundResource(
                    R.drawable.login_textview_grey
                )
                estimationActivityBinding.etCostDuration.setTextColor(
                    resources.getColor(
                        R.color.grey
                    )
                )
                estimationActivityBinding.etCostDuration.setHintTextColor(
                    resources.getColor(
                        R.color.grey
                    )
                )
            }
        }
    }


    fun estimationBackBtn(view: View) {
        finish()
    }

    fun submitEstimationBtn(view: View) {
        if (TextUtils.isEmpty(estimationActivityBinding.etDetailsAboutWork.text.trim())) {
            Utility.showSnackBar(
                estimationActivityBinding.etDetailsAboutWork,
                "Enter details about work ",
                this@EstimationActivity
            )
        } else if (TextUtils.isEmpty(estimationActivityBinding.etTimeDuration.text.trim())) {
            Utility.showSnackBar(
                estimationActivityBinding.etDetailsAboutWork,
                "Select duration",
                this@EstimationActivity
            )
        }else if (estimationActivityBinding.etTimeDuration.text.toString() == "00:00") {
            Utility.showSnackBar(
                estimationActivityBinding.etDetailsAboutWork,
                "Invalid duration",
                this@EstimationActivity
            )
        } else if (TextUtils.isEmpty(estimationActivityBinding.etCostDuration.text.trim())) {
            Utility.showSnackBar(
                estimationActivityBinding.etDetailsAboutWork,
                "Enter cost of work",
                this@EstimationActivity
            )
        } else if (estimationActivityBinding.etCostDuration.text.toString().toInt()<= 0) {
            Utility.showSnackBar(
                estimationActivityBinding.etDetailsAboutWork,
                "Cost should not be zero",
                this@EstimationActivity
            )
        }else if (estimationActivityBinding.etCostDuration.text.toString().startsWith("0")) {
            Utility.showSnackBar(
                estimationActivityBinding.etDetailsAboutWork,
                "Invalid amount",
                this@EstimationActivity
            )
        }else {
            mWaitingDialog!!.show()
            viewModel.setEstimationData(token, bookingId,estimationActivityBinding.etDetailsAboutWork.text.toString(),
                estimationActivityBinding.etTimeDuration.text.toString(),estimationActivityBinding.etCostDuration.text.toString())

        }
    }

    fun timeEstimate(view: View) {
        val timePickerDialog = TimePickerDialog(
            this,
            TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                var time = "$hourOfDay:$minute"
                var date = time
                var spf = SimpleDateFormat("HH:mm")
                val newDate: Date = spf.parse(date)
                spf = SimpleDateFormat("HH:mm")
                date = spf.format(newDate)
                estimationActivityBinding.etTimeDuration.setText(date)
            },
            mHour,
            mMinute,
            true
        )
        timePickerDialog.show()
    }
}
