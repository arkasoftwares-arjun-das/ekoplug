package com.ekoplug.service.activities

import android.os.Bundle
import android.view.View
import androidx.annotation.Nullable
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.ekoplug.model.PaymentHistoryModel
import com.ekoplug.service.R
import com.ekoplug.service.adapters.PaymentHistoryAdapter
import com.ekoplug.service.basesetup.BaseResponseArrayModel
import com.ekoplug.service.databinding.ActivityPaymntHistoryBinding
import com.ekoplug.service.viewmodel.PaymentHistoryViewModel
import com.ekoplug.utils.AppPreference
import com.ekoplug.utils.Utility
import com.ekoplug.utils.Watting

class PaymntHistoryActivity : AppCompatActivity() {

    lateinit var activityPaymntHistoryBinding: ActivityPaymntHistoryBinding
    private lateinit var viewModel: PaymentHistoryViewModel
    private var mWaitingDialog: Watting? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityPaymntHistoryBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_paymnt_history)
        mWaitingDialog = Watting(this, "")
        callApi()
        val token = AppPreference.getPreferenceValueByKey(this, "Token").toString()
        mWaitingDialog!!.show()
        viewModel.setPaymentHistory(token)
    }

    private fun callApi() {
        viewModel = ViewModelProviders.of(this).get(PaymentHistoryViewModel::class.java)

        viewModel.getPaymentHistory()
            .observe(this, object : Observer<BaseResponseArrayModel<PaymentHistoryModel>> {
                override
                fun onChanged(@Nullable paymentHistory: BaseResponseArrayModel<PaymentHistoryModel>) {
                    mWaitingDialog!!.dismiss()
                    if (paymentHistory != null) {
                        if (paymentHistory.getCode() == 200) {
                            if (paymentHistory.getData().isNullOrEmpty()) {
                                activityPaymntHistoryBinding.rvPaymentHistory.visibility = View.GONE
                                activityPaymntHistoryBinding.tvPaymentHistoryNotFound.visibility =
                                    View.VISIBLE
                            } else {
                                activityPaymntHistoryBinding.rvPaymentHistory.visibility =
                                    View.VISIBLE
                                activityPaymntHistoryBinding.tvPaymentHistoryNotFound.visibility =
                                    View.GONE
                                setAdapters(paymentHistory.getData())
                            }


                        } else {
                            Utility.showSnackBar(
                                activityPaymntHistoryBinding.tvPaymentHistoryNotFound,
                                paymentHistory.getMessage(),
                                this@PaymntHistoryActivity
                            )
                        }
                    } else {
                        Utility.showSnackBar(
                            activityPaymntHistoryBinding.tvPaymentHistoryNotFound,
                            getString(R.string.server_error),
                            this@PaymntHistoryActivity
                        )
                    }
                }
            })

        viewModel.error.observe(this, Observer {
            mWaitingDialog!!.dismiss()
            Utility.showSnackBar(
                activityPaymntHistoryBinding.tvPaymentHistoryNotFound,
                viewModel.error.value.toString(),
                this@PaymntHistoryActivity
            )
        })
    }

    private fun setAdapters(data: ArrayList<PaymentHistoryModel>?) {
        activityPaymntHistoryBinding.rvPaymentHistory.layoutManager =
            LinearLayoutManager(this@PaymntHistoryActivity)
        val paymentHistoryAdapter = PaymentHistoryAdapter(this@PaymntHistoryActivity, data)
        activityPaymntHistoryBinding.rvPaymentHistory.adapter = paymentHistoryAdapter
        paymentHistoryAdapter.notifyDataSetChanged()
    }

    fun paymentBackBtn(view : View){
        finish()
    }

}
