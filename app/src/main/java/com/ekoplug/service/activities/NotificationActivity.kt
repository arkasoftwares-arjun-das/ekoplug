package com.ekoplug.service.activities

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.annotation.Nullable
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.ekoplug.model.NotificationModel
import com.ekoplug.service.R
import com.ekoplug.service.adapters.NotificationAdapters
import com.ekoplug.service.basesetup.BaseResponseArrayModel
import com.ekoplug.service.basesetup.BaseResponseModel
import com.ekoplug.service.databinding.ActivityNotificationBinding
import com.ekoplug.service.viewmodel.NotificationClearViewModel
import com.ekoplug.utils.AppPreference
import com.ekoplug.utils.Utility
import com.ekoplug.utils.Watting
import com.ekoplug.viewmodel.NotificationListViewModel

class NotificationActivity : AppCompatActivity() {


    lateinit var activityNotificationBinding: ActivityNotificationBinding
    private var mWaitingDialog: Watting? = null
    lateinit var notificationClearModel: NotificationClearViewModel
    lateinit var notificationViewModel: NotificationListViewModel
    var notificationList = ArrayList<NotificationModel>()
    var token = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityNotificationBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_notification)
        mWaitingDialog = Watting(this, "")
        callApi()
        token = AppPreference.getPreferenceValueByKey(this, "Token").toString()
        mWaitingDialog!!.show()
        notificationViewModel.setNotificationData(token)
    }

    private fun callApi() {
        notificationViewModel =
            ViewModelProviders.of(this).get(NotificationListViewModel::class.java)

        notificationClearModel =
            ViewModelProviders.of(this).get(NotificationClearViewModel::class.java)
        notificationViewModel.getNotificationDataModel()
            .observe(
                this, object : Observer<BaseResponseArrayModel<NotificationModel>> {
                    override
                    fun onChanged(@Nullable notifyData: BaseResponseArrayModel<NotificationModel>?) {
                        mWaitingDialog!!.dismiss()
                        if (notifyData != null) {
                            if (notifyData.getCode() == 200) {

                                if (notifyData.getData()!!.isNullOrEmpty()) {
                                    activityNotificationBinding.tvNotificationNotFound.visibility =
                                        View.VISIBLE
                                    activityNotificationBinding.rvNotifications.visibility =
                                        View.GONE
                                } else {
                                    activityNotificationBinding.rvNotifications.visibility =
                                        View.VISIBLE
                                    activityNotificationBinding.tvNotificationNotFound.visibility =
                                        View.GONE
                                    notificationList = notifyData.getData()!!
                                    setAdapter(notifyData.getData()!!)
                                }
                            } else {
                                Utility.showSnackBar(
                                    activityNotificationBinding.tvNotificationNotFound,
                                    notifyData.getMessage(),
                                    this@NotificationActivity
                                )
                            }
                        } else {
                            Utility.showSnackBar(
                                activityNotificationBinding.tvNotificationNotFound,
                                getString(R.string.server_error),
                                this@NotificationActivity
                            )
                        }
                    }
                })

        notificationClearModel.getNotificationDataClearModel()
            .observe(
                this, object : Observer<BaseResponseModel<NotificationModel>> {
                    override
                    fun onChanged(@Nullable notifyData: BaseResponseModel<NotificationModel>?) {
                        mWaitingDialog!!.dismiss()
                        if (notifyData != null) {
                            if (notifyData.getCode() == 200) {
                                Utility.showSnackBar(
                                    activityNotificationBinding.tvNotificationNotFound,
                                    notifyData.getMessage(),
                                    this@NotificationActivity
                                )
                                Handler().postDelayed({
                                    finish()
                                }, 1000)
                            } else {
                                Utility.showSnackBar(
                                    activityNotificationBinding.tvNotificationNotFound,
                                    notifyData.getMessage(),
                                    this@NotificationActivity
                                )
                            }
                        } else {
                            Utility.showSnackBar(
                                activityNotificationBinding.tvNotificationNotFound,
                                getString(R.string.server_error),
                                this@NotificationActivity
                            )
                        }
                    }
                })

        notificationViewModel.error.observe(this, Observer {
            mWaitingDialog!!.dismiss()
            Utility.showSnackBar(
                activityNotificationBinding.tvNotificationNotFound,
                notificationViewModel.error.value.toString(),
                this
            )
        })
        notificationClearModel.error.observe(this, Observer {
            mWaitingDialog!!.dismiss()
            Utility.showSnackBar(
                activityNotificationBinding.tvNotificationNotFound,
                notificationClearModel.error.value.toString(),
                this
            )
        })
    }


    private fun setAdapter(data: ArrayList<NotificationModel>) {
        activityNotificationBinding.rvNotifications.layoutManager =
            LinearLayoutManager(this)
        val notificationAdapters = NotificationAdapters(this, data) { id ->
            val builder = AlertDialog.Builder(this)
            builder.setTitle("Alert")
            builder.setMessage("Are you sure you want to delete notification !")
            builder.setPositiveButton(android.R.string.yes) { dialog, which ->
                mWaitingDialog!!.show()
                notificationClearModel.setNotificationClearData(token, id.toString())
            }
            builder.setNegativeButton(android.R.string.no) { dialog, which ->
                builder.create().dismiss()
            }
            builder.show().setCancelable(true)
        }
        activityNotificationBinding.rvNotifications.adapter = notificationAdapters

    }

    fun sideMenuClick(view: View) {
        startActivity(Intent(this@NotificationActivity, SideMenuActivity::class.java))
    }

    fun profileClick(view: View) {
        startActivity(Intent(this@NotificationActivity, ProfileActivity::class.java))
    }

    fun homeClick(view: View) {
        startActivity(Intent(this@NotificationActivity, HomeActivity::class.java))
        finishAffinity()
    }

    fun clearNotification(view: View) {
        val notificationArray = ArrayList<String>()
        for (i in 0 until notificationList.size) {
            notificationArray.add(notificationList.get(i).id.toString())
        }
        val result = notificationArray.joinToString(", ")
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Alert")
        builder.setMessage("Are you sure you want to delete all notification !")
        builder.setPositiveButton(android.R.string.yes) { dialog, which ->
            mWaitingDialog!!.show()
            notificationClearModel.setNotificationClearData(token, result)
        }
        builder.setNegativeButton(android.R.string.no) { dialog, which ->
            builder.create().dismiss()
        }
        builder.show().setCancelable(true)
        System.out.println("list" + result)
    }
}
