package com.ekoplug.service.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.annotation.Nullable
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.ekoplug.service.R
import com.ekoplug.service.basesetup.BaseResponseModel
import com.ekoplug.service.databinding.ActivityForgotPasswordBinding
import com.ekoplug.service.model.ForgotPasswardResponseModel
import com.ekoplug.service.viewmodel.ForgotPasswordViewModel
import com.ekoplug.utils.Utility
import com.ekoplug.utils.Watting
import kotlinx.android.synthetic.main.activity_forgot_password.*

class ForgotPasswordActivity : AppCompatActivity() {

    lateinit var activityForgotPassword: ActivityForgotPasswordBinding
    lateinit var forgotViewModel: ForgotPasswordViewModel
    private var mWaitingDialog: Watting? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityForgotPassword =
            DataBindingUtil.setContentView(this, R.layout.activity_forgot_password)
        mWaitingDialog = Watting(this, "")
        init()
        callApi()
    }

    private fun init() {
        activityForgotPassword.etForgotEmail.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                activityForgotPassword.etForgotEmail.setCompoundDrawablesWithIntrinsicBounds(
                    ContextCompat.getDrawable(
                        this,
                        R.drawable.mail_blue_icon
                    ),
                    null,
                    null,
                    null
                )
                activityForgotPassword.etForgotEmail.setBackgroundResource(R.drawable.login_textview_blue)
                activityForgotPassword.etForgotEmail.setTextColor(
                    resources.getColor(
                        R.color.colorPrimary
                    )
                )
                activityForgotPassword.etForgotEmail.setHintTextColor(
                    resources.getColor(
                        R.color.colorPrimary
                    )
                )

            } else {
                activityForgotPassword.etForgotEmail.setCompoundDrawablesWithIntrinsicBounds(
                    ContextCompat.getDrawable(
                        this,
                        R.drawable.mail_grey_icon
                    ),
                    null,
                    null,
                    null
                )
                activityForgotPassword.etForgotEmail.setBackgroundResource(R.drawable.login_textview_grey)
                activityForgotPassword.etForgotEmail.setTextColor(
                    resources.getColor(
                        R.color.grey
                    )
                )
                activityForgotPassword.etForgotEmail.setHintTextColor(
                    resources.getColor(
                        R.color.grey
                    )
                )
            }
        }
    }

    private fun callApi() {
        forgotViewModel = ViewModelProviders.of(this).get(ForgotPasswordViewModel::class.java)
        activityForgotPassword.forgotViewModel = forgotViewModel

        activityForgotPassword.forgotViewModel!!.getForgotResponseModel()
            .observe(this, object : Observer<BaseResponseModel<ForgotPasswardResponseModel>> {
                override
                fun onChanged(@Nullable forgotModel: BaseResponseModel<ForgotPasswardResponseModel>?) {
                    mWaitingDialog!!.dismiss()
                    if (forgotModel != null) {

                        if (forgotModel.getCode() == 200) {
                            Utility.showSnackBar(
                                et_forgotEmail,
                                forgotModel.getMessage(),
                                this@ForgotPasswordActivity
                            )
                            startActivity(
                                Intent(
                                    this@ForgotPasswordActivity,
                                    OtpVerifyActivity::class.java
                                ).putExtra("Email", activityForgotPassword.etForgotEmail.text.toString())
                                    .putExtra("activity","Forgot")
                            )
                            activityForgotPassword.etForgotEmail.setText("")
                            finish()
                        } else {
                            Utility.showSnackBar(
                                et_forgotEmail,
                                forgotModel.getErrorMessage(),
                                this@ForgotPasswordActivity
                            )
                        }

                    } else {
                        Utility.showSnackBar(
                            et_forgotEmail,
                            getString(R.string.server_error),
                            this@ForgotPasswordActivity
                        )

                    }
                }
            })

        activityForgotPassword.forgotViewModel!!.error.observe(this, Observer {
            mWaitingDialog!!.dismiss()
            Utility.showSnackBar(
                et_forgotEmail,
                forgotViewModel!!.error.value.toString(),
                this@ForgotPasswordActivity
            )
        })
    }

    fun forgotBack(view: View) {
        finish()
    }

    fun sendBtn(view: View) {
        mWaitingDialog!!.show()
        activityForgotPassword.forgotViewModel!!.setForgotData()
    }
}
