package com.ekoplug.service.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.annotation.Nullable
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.ekoplug.service.R
import com.ekoplug.service.basesetup.BaseResponseModel
import com.ekoplug.service.databinding.ActivityOtpVerifyBinding
import com.ekoplug.service.model.ForgotPasswardResponseModel
import com.ekoplug.service.model.OtpVerifyResponseModel
import com.ekoplug.service.viewmodel.OtpVerifyViewModel
import com.ekoplug.utils.Utility
import com.ekoplug.utils.Watting
import kotlinx.android.synthetic.main.activity_otp_verify.*
import java.util.concurrent.TimeUnit

class OtpVerifyActivity : AppCompatActivity() {

    lateinit var otpVerfyBinding: ActivityOtpVerifyBinding
    lateinit var ctx: Context
    private val FORMAT = "%02d:%02d"
    var otpViewModel: OtpVerifyViewModel? = null
    var email: String = ""
    var activity: String = ""
    private var mWaitingDialog: Watting? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        otpVerfyBinding = DataBindingUtil.setContentView(this,
            R.layout.activity_otp_verify
        )
        ctx = this
        mWaitingDialog = Watting(this, "")
        email = intent.getStringExtra("Email")
        activity = intent.getStringExtra("activity")
        init()
        callApi()
        callApiReset()
    }

    private fun callApi() {
        otpViewModel = ViewModelProviders.of(this).get(OtpVerifyViewModel::class.java)
        otpVerfyBinding.otpViewModel = otpViewModel

        otpVerfyBinding.otpViewModel!!.getOtpVerifyResponseModel()
            .observe(this, object : Observer<BaseResponseModel<OtpVerifyResponseModel>> {
                override
                fun onChanged(@Nullable otpModel: BaseResponseModel<OtpVerifyResponseModel>?) {
                    mWaitingDialog!!.dismiss()
                    if (otpModel != null) {

                        if (otpModel.getCode() == 200) {
                            Utility.showSnackBar(
                                et_OtpFirst,
                                otpModel.getMessage(),
                                this@OtpVerifyActivity
                            )
                            et_OtpFirst.setText("")
                            et_OtpSecond.setText("")
                            et_OtpThird.setText("")
                            et_OtpFourth.setText("")
                            if (activity.equals("Forgot")) {
                                startActivity(
                                    Intent(
                                        this@OtpVerifyActivity,
                                        ChangePasswordActivity::class.java
                                    ).putExtra("Token",otpModel.getData()!!.auth_token)
                                )
                                finish()
                            } else if(activity.equals("signup")){
                                startActivity(
                                    Intent(
                                        this@OtpVerifyActivity,
                                        LoginActivity::class.java
                                    ))
                                finish()
                            }else  {

                                finish()
                            }
                        } else {
                            Utility.showSnackBar(
                                et_OtpFirst,
                                otpModel.getErrorMessage(),
                                this@OtpVerifyActivity
                            )
                        }

                    } else {
                        Utility.showSnackBar(
                            et_OtpFirst,
                            getString(R.string.server_error),
                            this@OtpVerifyActivity
                        )

                    }
                }
            })


        otpVerfyBinding.otpViewModel!!.error.observe(this, Observer {
            mWaitingDialog!!.dismiss()
            Utility.showSnackBar(
                et_OtpFirst,
                otpViewModel!!.error.value.toString(),
                this@OtpVerifyActivity
            )
        })
    }

    private fun init() {
        otpVerfyBinding.etOtpFirst.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                if (et_OtpFirst.getText().toString().length == 1) {
                    et_OtpSecond.requestFocus()
                }
            }

            override fun afterTextChanged(editable: Editable) {
                // We can call api to verify the OTP here or on an explicit button click
            }

            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

        })
        otpVerfyBinding.etOtpSecond.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                if (et_OtpSecond.getText().toString().length == 1) {
                    et_OtpThird.requestFocus()
                }
            }

            override fun afterTextChanged(editable: Editable) {
                // We can call api to verify the OTP here or on an explicit button click
            }

            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

        })
        otpVerfyBinding.etOtpThird.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                if (et_OtpThird.getText().toString().length == 1) {
                    et_OtpFourth.requestFocus()
                }
            }

            override fun afterTextChanged(editable: Editable) {
                // We can call api to verify the OTP here or on an explicit button click
            }

            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

        })
        otpVerfyBinding.etOtpFirst.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                otpVerfyBinding.etOtpFirst.setBackgroundResource(R.drawable.login_textview_blue)
                otpVerfyBinding.etOtpFirst.setTextColor(resources.getColor(R.color.colorPrimary))
                otpVerfyBinding.etOtpFirst.setHintTextColor(resources.getColor(R.color.colorPrimary))

            } else {
                otpVerfyBinding.etOtpFirst.setBackgroundResource(R.drawable.login_textview_grey)
                otpVerfyBinding.etOtpFirst.setTextColor(resources.getColor(R.color.grey))
                otpVerfyBinding.etOtpFirst.setHintTextColor(resources.getColor(R.color.grey))
            }
        }
        otpVerfyBinding.etOtpSecond.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                otpVerfyBinding.etOtpSecond.setBackgroundResource(R.drawable.login_textview_blue)
                otpVerfyBinding.etOtpSecond.setTextColor(resources.getColor(R.color.colorPrimary))
                otpVerfyBinding.etOtpSecond.setHintTextColor(resources.getColor(R.color.colorPrimary))

            } else {
                otpVerfyBinding.etOtpSecond.setBackgroundResource(R.drawable.login_textview_grey)
                otpVerfyBinding.etOtpSecond.setTextColor(resources.getColor(R.color.grey))
                otpVerfyBinding.etOtpSecond.setHintTextColor(resources.getColor(R.color.grey))
            }
        }
        otpVerfyBinding.etOtpThird.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                otpVerfyBinding.etOtpThird.setBackgroundResource(R.drawable.login_textview_blue)
                otpVerfyBinding.etOtpThird.setTextColor(resources.getColor(R.color.colorPrimary))
                otpVerfyBinding.etOtpThird.setHintTextColor(resources.getColor(R.color.colorPrimary))

            } else {
                otpVerfyBinding.etOtpThird.setBackgroundResource(R.drawable.login_textview_grey)
                otpVerfyBinding.etOtpThird.setTextColor(resources.getColor(R.color.grey))
                otpVerfyBinding.etOtpThird.setHintTextColor(resources.getColor(R.color.grey))
            }
        }
        otpVerfyBinding.etOtpFourth.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                otpVerfyBinding.etOtpFourth.setBackgroundResource(R.drawable.login_textview_blue)
                otpVerfyBinding.etOtpFourth.setTextColor(resources.getColor(R.color.colorPrimary))
                otpVerfyBinding.etOtpFourth.setHintTextColor(resources.getColor(R.color.colorPrimary))

            } else {
                otpVerfyBinding.etOtpFourth.setBackgroundResource(R.drawable.login_textview_grey)
                otpVerfyBinding.etOtpFourth.setTextColor(resources.getColor(R.color.grey))
                otpVerfyBinding.etOtpFourth.setHintTextColor(resources.getColor(R.color.grey))
            }
        }
    }

    fun otpBack(v: View) {
        finish()
    }

    fun resendOtp(v: View) {
        otpVerfyBinding.tvTimer.visibility = VISIBLE
        otpVerfyBinding.tvResendOtp.visibility = GONE
        otpVerfyBinding.otpViewModel!!.setOtpResend(email)
        object : CountDownTimer(120000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                Log.d("timer ", "seconds remaining: " + millisUntilFinished / 1000)
                otpVerfyBinding.tvTimer.setText(
                    "" + String.format(
                        FORMAT,
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(
                            TimeUnit.MILLISECONDS.toHours(millisUntilFinished)
                        ),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(
                            TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished)
                        )
                    )
                )
            }
            override fun onFinish() {
                otpVerfyBinding.tvTimer.visibility = GONE
                otpVerfyBinding.tvResendOtp.visibility = VISIBLE
            }
        }.start()
    }

    private fun callApiReset() {
        otpViewModel = ViewModelProviders.of(this).get(OtpVerifyViewModel::class.java)
        otpVerfyBinding.otpViewModel = otpViewModel

        otpVerfyBinding.otpViewModel!!.getOtpResendRessponse()
            .observe(this, object : Observer<BaseResponseModel<ForgotPasswardResponseModel>> {
                override
                fun onChanged(@Nullable otpModel: BaseResponseModel<ForgotPasswardResponseModel>?) {
                    mWaitingDialog!!.dismiss()
                    if (otpModel != null) {

                        if (otpModel.getCode() == 200) {
                            Utility.showSnackBar(
                                et_OtpFirst,
                                otpModel.getMessage(),
                                this@OtpVerifyActivity
                            )
                            et_OtpFirst.setText("")
                            et_OtpSecond.setText("")
                            et_OtpThird.setText("")
                            et_OtpFourth.setText("")

                        } else {
                            Utility.showSnackBar(
                                et_OtpFirst,
                                otpModel.getMessage(),
                                this@OtpVerifyActivity
                            )
                        }

                    } else {
                        Utility.showSnackBar(
                            et_OtpFirst,
                            getString(R.string.server_error),
                            this@OtpVerifyActivity
                        )

                    }
                }
            })


        otpVerfyBinding.otpViewModel!!.error.observe(this, Observer {
            mWaitingDialog!!.dismiss()
            Utility.showSnackBar(
                et_OtpFirst,
                otpViewModel!!.error.value.toString(),
                this@OtpVerifyActivity
            )
        })

    }

    fun submitBtn(v: View) {
        mWaitingDialog!!.show()
        otpVerfyBinding.otpViewModel!!.setOtpData(email)
    }

    fun otpParent(v: View) {
        Utility.hideKeyboard(this,otpVerfyBinding.etOtpFirst)
    }

}
