package com.ekoplug.service.activities

import android.os.Bundle
import android.view.View
import androidx.annotation.Nullable
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.ekoplug.model.StaticPageModel
import com.ekoplug.service.R
import com.ekoplug.service.basesetup.BaseResponseModel
import com.ekoplug.service.databinding.ActivityPrivacyPolicyBinding
import com.ekoplug.service.viewmodel.ContactUsViewModel
import com.ekoplug.utils.AppPreference
import com.ekoplug.utils.Utility
import com.ekoplug.utils.Watting

class PrivacyPolicyActivity : AppCompatActivity() {

    lateinit var activityPrivacyPolicyBinding: ActivityPrivacyPolicyBinding
    private var mWaitingDialog: Watting? = null
    private lateinit var viewModel: ContactUsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityPrivacyPolicyBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_privacy_policy)
        mWaitingDialog = Watting(this, "")
        val token = AppPreference.getPreferenceValueByKey(this, "Token").toString()
        callApi()
        mWaitingDialog!!.show()
        viewModel.setStaticPageData(token, "Privacy")
    }

    private fun callApi() {
        viewModel = ViewModelProviders.of(this).get(ContactUsViewModel::class.java)

        viewModel.getStaticPageData()
            .observe(this, object : Observer<BaseResponseModel<StaticPageModel>> {
                override
                fun onChanged(@Nullable staticModel: BaseResponseModel<StaticPageModel>?) {
                    mWaitingDialog!!.dismiss()
                    if (staticModel != null) {

                        if (staticModel.getCode() == 200) {
                            activityPrivacyPolicyBinding.tvPrivacyhortDescription.text =
                                staticModel.getData()!!.short_description
                            activityPrivacyPolicyBinding.tvPrivacyDescription.text =
                                staticModel.getData()!!.content

                        } else {
                            Utility.showSnackBar(
                                activityPrivacyPolicyBinding.tvPrivacyPolicy,
                                staticModel.getMessage(),
                                this@PrivacyPolicyActivity
                            )
                        }

                    } else {
                        Utility.showSnackBar(
                            activityPrivacyPolicyBinding.tvPrivacyPolicy,
                            getString(R.string.server_error),
                            this@PrivacyPolicyActivity
                        )
                    }
                }
            })

        viewModel.error.observe(this, Observer {
            mWaitingDialog!!.dismiss()
            Utility.showSnackBar(
                activityPrivacyPolicyBinding.tvPrivacyPolicy, viewModel.error.value.toString(),
                this@PrivacyPolicyActivity
            )
        })
    }


    fun privacyBackBtn(view: View) {
        finish()
    }
}
