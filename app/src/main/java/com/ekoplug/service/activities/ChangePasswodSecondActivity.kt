package com.ekoplug.service.activities

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.text.InputType
import android.text.method.PasswordTransformationMethod
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.Nullable
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.ekoplug.model.AddressProfile
import com.ekoplug.model.ProfileModel
import com.ekoplug.service.R
import com.ekoplug.service.basesetup.BaseResponseModel
import com.ekoplug.service.databinding.ChangePasswodFragmentBinding
import com.ekoplug.service.viewmodel.ChangePasswodViewModel
import com.ekoplug.utils.AppPreference
import com.ekoplug.utils.Utility
import com.ekoplug.utils.Watting
import com.squareup.picasso.Picasso
import java.util.*

class ChangePasswodSecondActivity : AppCompatActivity(),View.OnClickListener {

    companion object {
        fun newInstance() = ChangePasswodSecondActivity()
    }

    private lateinit var viewModel: ChangePasswodViewModel
    lateinit var changePasswordBinding : ChangePasswodFragmentBinding
    private var mWaitingDialog: Watting? = null
    var passwordSee: Boolean = false
    var confirmpasswordSee: Boolean = false
    var currentPasswordSee: Boolean = false
    var token : String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        changePasswordBinding =  DataBindingUtil.setContentView(this, R.layout.change_passwod_fragment)
        mWaitingDialog = Watting(this, "")
        token = AppPreference.getPreferenceValueByKey(this@ChangePasswodSecondActivity, "Token").toString()
        init(changePasswordBinding.root)
        callApi()

    }

    private fun callApi() {
        viewModel = ViewModelProviders.of(this).get(ChangePasswodViewModel::class.java)
        changePasswordBinding.changePasswordViewModel = viewModel

        changePasswordBinding.changePasswordViewModel!!.getCurrentChangePasswordResponseModel()
            .observe(this, object :
                Observer<BaseResponseModel<ProfileModel<AddressProfile>>> {
                override
                fun onChanged(@Nullable profileModel: BaseResponseModel<ProfileModel<AddressProfile>>?) {
                    mWaitingDialog!!.dismiss()
                    if (profileModel != null) {
                        if (profileModel.getCode() == 200) {
                            Utility.showSnackBar(
                                changePasswordBinding.etCpPasswordFrag,
                                profileModel.getMessage(),
                                this@ChangePasswodSecondActivity
                            )
                            changePasswordBinding.etCpPasswordFrag.setText("")
                            changePasswordBinding.etCurrentPasswordFrag.setText("")
                            changePasswordBinding.etCpconfirmPasswordFrag.setText("")
                            supportFragmentManager.popBackStackImmediate()

                        } else {
                            Utility.showSnackBar(
                                changePasswordBinding.etCpPasswordFrag,
                                profileModel.getMessage(),
                                this@ChangePasswodSecondActivity
                            )
                        }
                    } else {
                        Utility.showSnackBar(
                            changePasswordBinding.etCpPasswordFrag,
                            getString(R.string.server_error),
                            this@ChangePasswodSecondActivity
                        )
                    }
                }
            })
        changePasswordBinding.changePasswordViewModel!!.error.observe(this, Observer {
            mWaitingDialog!!.dismiss()
            Utility.showSnackBar(
                changePasswordBinding.etCpPasswordFrag, viewModel.error.value.toString(),
                this@ChangePasswodSecondActivity
            )
        })
    }

    private fun init(root: View) {
        changePasswordBinding.btnSaveChangePassword.setOnClickListener(this)
        changePasswordBinding.ivCurrentPasswordFrag.setOnClickListener(this)
        changePasswordBinding.ivCpPasswordFrag.setOnClickListener(this)
        changePasswordBinding.ivCpconfirmPasswordFrag.setOnClickListener(this)

        changePasswordBinding.etCurrentPasswordFrag.setOnFocusChangeListener { v, hasFocus ->
                if (hasFocus) {
                    changePasswordBinding.etCurrentPasswordFrag.setCompoundDrawablesWithIntrinsicBounds(
                        ContextCompat.getDrawable(this,
                            R.drawable.password_blue_icon),
                        null,
                        null,
                        null
                    )
                    changePasswordBinding.rlChangePasswordCurrentFrag.setBackgroundResource(
                        R.drawable.login_textview_blue
                    )
                    changePasswordBinding.etCurrentPasswordFrag.setTextColor(resources.getColor(
                        R.color.colorPrimary
                    ))
                    changePasswordBinding.etCurrentPasswordFrag.setHintTextColor(resources.getColor(
                        R.color.colorPrimary
                    ))

                } else {
                    changePasswordBinding.etCurrentPasswordFrag.setCompoundDrawablesWithIntrinsicBounds(
                        ContextCompat.getDrawable(this,
                            R.drawable.password_grey_icon
                        ),
                        null,
                        null,
                        null
                    )
                    changePasswordBinding.rlChangePasswordCurrentFrag.setBackgroundResource(
                        R.drawable.login_textview_grey
                    )
                    changePasswordBinding.etCurrentPasswordFrag.setTextColor(resources.getColor(
                        R.color.grey
                    ))
                    changePasswordBinding.etCurrentPasswordFrag.setHintTextColor(resources.getColor(
                        R.color.grey
                    ))
                }

            }
        changePasswordBinding.etCpPasswordFrag.setOnFocusChangeListener { v, hasFocus ->
                if (hasFocus) {
                    changePasswordBinding.etCpPasswordFrag.setCompoundDrawablesWithIntrinsicBounds(
                        ContextCompat.getDrawable(this,
                            R.drawable.password_blue_icon
                        ),
                        null,
                        null,
                        null
                    )
                    changePasswordBinding.rlChangePasswordParentFrag.setBackgroundResource(
                        R.drawable.login_textview_blue
                    )
                    changePasswordBinding.etCpPasswordFrag.setTextColor(resources.getColor(
                        R.color.colorPrimary
                    ))
                    changePasswordBinding.etCpPasswordFrag.setHintTextColor(resources.getColor(
                        R.color.colorPrimary
                    ))

                } else {
                    changePasswordBinding.etCpPasswordFrag.setCompoundDrawablesWithIntrinsicBounds(
                        ContextCompat.getDrawable(this,
                            R.drawable.password_grey_icon
                        ),
                        null,
                        null,
                        null
                    )
                    changePasswordBinding.rlChangePasswordParentFrag.setBackgroundResource(
                        R.drawable.login_textview_grey
                    )
                    changePasswordBinding.etCpPasswordFrag.setTextColor(resources.getColor(
                        R.color.grey
                    ))
                    changePasswordBinding.etCpPasswordFrag.setHintTextColor(resources.getColor(
                        R.color.grey
                    ))
                }

            }
        changePasswordBinding.etCpconfirmPasswordFrag.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                changePasswordBinding.etCpconfirmPasswordFrag.setCompoundDrawablesWithIntrinsicBounds(
                    ContextCompat.getDrawable(this,
                        R.drawable.password_blue_icon
                    ),
                    null,
                    null,
                    null
                )
                changePasswordBinding.rlChangePasswordConfirmParentFrag.setBackgroundResource(
                    R.drawable.login_textview_blue
                )
                changePasswordBinding.etCpconfirmPasswordFrag.setTextColor(resources.getColor(
                    R.color.colorPrimary
                ))
                changePasswordBinding.etCpconfirmPasswordFrag.setHintTextColor(resources.getColor(
                    R.color.colorPrimary
                ))

            } else {
                changePasswordBinding.etCpconfirmPasswordFrag.setCompoundDrawablesWithIntrinsicBounds(
                    ContextCompat.getDrawable(this,
                        R.drawable.password_grey_icon
                    ),
                    null,
                    null,
                    null
                )
                changePasswordBinding.rlChangePasswordConfirmParentFrag.setBackgroundResource(
                    R.drawable.login_textview_grey
                )
                changePasswordBinding.etCpconfirmPasswordFrag.setTextColor(resources.getColor(
                    R.color.grey
                ))
                changePasswordBinding.etCpconfirmPasswordFrag.setHintTextColor(resources.getColor(
                    R.color.grey
                ))
            }

        }
    }

    override fun onClick(v: View?) {
        if(v == changePasswordBinding.btnSaveChangePassword){
            mWaitingDialog!!.show()
            changePasswordBinding.changePasswordViewModel!!.setChangePasswordData(token)
        }
        else if( v == changePasswordBinding.ivBackFrag){
            supportFragmentManager.popBackStackImmediate()
        }
        else if(v == changePasswordBinding.ivCurrentPasswordFrag){
            if (currentPasswordSee) {
                currentPasswordSee = !currentPasswordSee
                changePasswordBinding.etCurrentPasswordFrag.transformationMethod =
                    PasswordTransformationMethod.getInstance()
                changePasswordBinding.ivCurrentPasswordFrag.setImageResource(R.drawable.eye_icon)
            } else {
                currentPasswordSee = !currentPasswordSee
                changePasswordBinding.etCurrentPasswordFrag.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                changePasswordBinding.etCurrentPasswordFrag.setTransformationMethod(null);
                changePasswordBinding.ivCurrentPasswordFrag.setImageResource(R.drawable.ic_open_eye_24dp)
            }
        }
        else if(v == changePasswordBinding.ivCpPasswordFrag){
            if (passwordSee) {
                passwordSee = !passwordSee
                changePasswordBinding.etCpPasswordFrag.transformationMethod =
                    PasswordTransformationMethod.getInstance()
                changePasswordBinding.ivCpPasswordFrag.setImageResource(R.drawable.eye_icon)
            } else {
                passwordSee = !passwordSee
                changePasswordBinding.etCpPasswordFrag.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                changePasswordBinding.etCpPasswordFrag.setTransformationMethod(null);
                changePasswordBinding.ivCpPasswordFrag.setImageResource(R.drawable.ic_open_eye_24dp)
            }
        }
        else if(v == changePasswordBinding.ivCpconfirmPasswordFrag){
            if (confirmpasswordSee) {
                confirmpasswordSee = !confirmpasswordSee
                changePasswordBinding.etCpconfirmPasswordFrag.transformationMethod =
                    PasswordTransformationMethod.getInstance()
                changePasswordBinding.ivCpconfirmPasswordFrag.setImageResource(R.drawable.eye_icon)
            } else {
                confirmpasswordSee = !confirmpasswordSee
                changePasswordBinding.etCpconfirmPasswordFrag.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                changePasswordBinding.etCpconfirmPasswordFrag.setTransformationMethod(null);
                changePasswordBinding.ivCpconfirmPasswordFrag.setImageResource(R.drawable.ic_open_eye_24dp)
            }
        }
    }

    fun changepasswordBack(view: View) {
        finish()
    }
}
