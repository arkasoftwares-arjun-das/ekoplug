package com.ekoplug.service.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.ekoplug.service.R
import com.ekoplug.service.databinding.ActivityServiceStartBinding

class ServiceStartActivity : AppCompatActivity() {

    lateinit var activityServiceStartBinding: ActivityServiceStartBinding
    var bookingId = 0
    var userId = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityServiceStartBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_service_start)
        val amount = intent.getStringExtra("amount")
        val mode = intent.getStringExtra("mode")
        bookingId = intent.getIntExtra("bookingID", 0)
        userId = intent.getIntExtra("userId", 0)
        activityServiceStartBinding.tvPaymentValue.setText(amount)
        activityServiceStartBinding.tvModeOfPayment.setText(mode)
    }

    fun submitStartServiceBtn(view: View) {
        startActivity(
            Intent(
                this@ServiceStartActivity,
                EstimationUploadActivity::class.java
            ).putExtra("bookingID", bookingId).putExtra("userId",userId)
        )
    }

    fun startServiceBtnBack(view: View) {
        finish()
    }
}
