package com.ekoplug.service.activities

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.media.MediaScannerConnection
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.provider.MediaStore
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.annotation.Nullable
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.ekoplug.model.UpdateProfileMdel
import com.ekoplug.service.R
import com.ekoplug.service.basesetup.BaseResponseModel
import com.ekoplug.service.databinding.ActivityEstimationUploadBinding
import com.ekoplug.service.databinding.LayoutServiceTimerBinding
import com.ekoplug.service.model.BookingDetailsModel
import com.ekoplug.service.viewmodel.BookingRequestDetailsViewModel
import com.ekoplug.service.viewmodel.EstimationUploadViewModel
import com.ekoplug.utils.AppPreference
import com.ekoplug.utils.Utility
import com.ekoplug.utils.Watting
import com.github.dhaval2404.imagepicker.ImagePicker
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class EstimationUploadActivity : AppCompatActivity() {

    lateinit var activityEstimationUploadBinding: ActivityEstimationUploadBinding
    var bookingId = 0
    var CAMERA_CAPTURE = 1;
    var userId = 0
    lateinit var bookingRequestDetailsViewModel: BookingRequestDetailsViewModel
    private var mWaitingDialog: Watting? = null
    private lateinit var viewModel: EstimationUploadViewModel
    private var imageUrl: Uri? = null
    private var before: File? = null
    private var after: File? = null
    var firstImage = ""
    var secondImage = ""
    var token = ""
    var count = 0
    var updateTime = ""
    var difference = 0
    var currentTime = ""
    var hour = ""
    var minute = ""
    var second = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityEstimationUploadBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_estimation_upload)
        bookingId = intent.getIntExtra("bookingID", 0)
        userId = intent.getIntExtra("userId", 0)
        token = AppPreference.getPreferenceValueByKey(this@EstimationUploadActivity, "Token").toString()
        callApi()
        mWaitingDialog = Watting(this, "")

    }

    private fun callApi() {
        viewModel = ViewModelProviders.of(this).get(EstimationUploadViewModel::class.java)
        bookingRequestDetailsViewModel =
            ViewModelProviders.of(this).get(BookingRequestDetailsViewModel::class.java)

        viewModel.getUploadEstimatinPictureModel()
            .observe(this, object :
                androidx.lifecycle.Observer<BaseResponseModel<UpdateProfileMdel>> {
                override
                fun onChanged(@Nullable profileModel: BaseResponseModel<UpdateProfileMdel>?) {
                    mWaitingDialog!!.dismiss()
                    if (profileModel != null) {
                        if (profileModel.getCode() == 200) {
                           /* Utility.showSnackBar(
                                activityEstimationUploadBinding.tvPhotoProof,
                                profileModel.getMessage(),
                                this@EstimationUploadActivity
                            )*/
                            mWaitingDialog!!.show()
                            bookingRequestDetailsViewModel.setAcceptRequest(token, bookingId, "END")

                        } else {
                            Utility.showSnackBar(
                                activityEstimationUploadBinding.tvPhotoProof,
                                profileModel.getMessage(),
                                this@EstimationUploadActivity
                            )
                        }
                    } else {
                        Utility.showSnackBar(
                            activityEstimationUploadBinding.tvPhotoProof,
                            getString(R.string.server_error),
                            this@EstimationUploadActivity
                        )
                    }
                }
            })

        bookingRequestDetailsViewModel.getAcceptRequest()
            .observe(
                this,
                object : Observer<BaseResponseModel<BookingDetailsModel>> {
                    override
                    fun onChanged(@Nullable bookingRequestAcceptModel: BaseResponseModel<BookingDetailsModel>?) {
                        mWaitingDialog!!.dismiss()
                        if (bookingRequestAcceptModel != null) {
                            if (bookingRequestAcceptModel.getCode() == 200) {
                                Utility.showSnackBar(
                                    activityEstimationUploadBinding.tvPhotoProof,
                                    bookingRequestAcceptModel.getMessage(),
                                    this@EstimationUploadActivity
                                )
                                Handler().postDelayed({
                                    startActivity(
                                        Intent(
                                            this@EstimationUploadActivity,
                                            SubmitReviewActivity::class.java
                                        ).putExtra("userId", userId.toString()).putExtra(
                                            "workerId",
                                            AppPreference.getIntPreferenceValueByKey(
                                                this@EstimationUploadActivity,
                                                "Id"
                                            ).toString()
                                        )
                                    )
                                    finishAffinity()
                                },1000)
//                                Handler().postDelayed({
//                                    startActivity(
//                                        Intent(
//                                            this@EstimationUploadActivity,
//                                            HomeActivity::class.java
//                                        )
//                                    )
//                                    finishAffinity()
//                                }, 1000)
                            } else {
                                Utility.showSnackBar(
                                    activityEstimationUploadBinding.tvPhotoProof,
                                    bookingRequestAcceptModel.getMessage(),
                                    this@EstimationUploadActivity
                                )
                            }
                        } else {
                            Utility.showSnackBar(
                                activityEstimationUploadBinding.tvPhotoProof,
                                getString(R.string.server_error),
                                this@EstimationUploadActivity
                            )
                        }
                    }
                })

        bookingRequestDetailsViewModel.error.observe(this, Observer {
            mWaitingDialog!!.dismiss()
            Utility.showSnackBar(
                activityEstimationUploadBinding.tvPhotoProof,
                bookingRequestDetailsViewModel.error.value.toString(),
                this
            )
        })

        viewModel.error.observe(this, androidx.lifecycle.Observer {
            mWaitingDialog!!.dismiss()
            Utility.showSnackBar(
                activityEstimationUploadBinding.tvPhotoProof, viewModel.error.value.toString(),
                this@EstimationUploadActivity
            )
        })
    }

    fun proofBackBtn(view: View) {
        finish()
    }

    fun uploadServicePhotoBtn(view: View) {
        if (firstImage.equals("")) {
            Utility.showSnackBar(
                activityEstimationUploadBinding.tvPhotoProof,
                "Upload before service image",
                this@EstimationUploadActivity
            )
        } else if (secondImage.equals("")) {
            Utility.showSnackBar(
                activityEstimationUploadBinding.tvPhotoProof,
                "Upload after service image",
                this@EstimationUploadActivity
            )
        } else {
            uploadPictureApi()
        }
    }

    fun uploadBeforeImageBtn(view: View) {
        onSelectImage()
        count = 1
    }

    fun uploadAfterImageBtn(view: View) {
        onSelectImage()
        count = 2
    }

    private fun onSelectImage() {
        ImagePicker.with(this)
            .cameraOnly()
            .compress(1024)            //Final image size will be less than 1 MB(Optional)
            .maxResultSize(
                1080,
                1080)                   //Final image resolution will be less than 1080 x 1080(Optional)
            .start()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            //Image Uri will not be null for RESULT_OK
            try {
                val fileUri = data?.data
                val bitmap =
                    MediaStore.Images.Media.getBitmap(this.contentResolver, fileUri)
                if (count == 1) {
                    activityEstimationUploadBinding.imgOne.setImageURI(fileUri)
                    val filePath = saveImage(bitmap)
                    before = File(filePath)
                    firstImage = before!!.name
                } else if (count == 2) {
                    activityEstimationUploadBinding.imgOneAfter.setImageURI(fileUri)
                    val filePath = saveImage(bitmap)
                    after = File(filePath)
                    secondImage = after!!.name
                }
            } catch (e: IOException) {
                e.printStackTrace()
            }
        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show()
        }
    }

    private fun uploadPictureApi() {
        val builder = MultipartBody.Builder()
        builder.setType(MultipartBody.FORM)
        builder.addFormDataPart("profile_image", before!!.name)

        if (!before!!.equals("")) {
            val file = before
            val file2 = after
            val requestFile: RequestBody = RequestBody.create(
                MediaType.parse("multipart/form-data"),
                file
            )
            val multipartBody = MultipartBody.Part.createFormData(
                "before_service_image", before!!.name, requestFile
            )

            val requestFile2: RequestBody = RequestBody.create(
                MediaType.parse("multipart/form-data"),
                file2
            )
            val multipartBody2 = MultipartBody.Part.createFormData(
                "after_service_image", after!!.name, requestFile2
            )
            builder.addPart(multipartBody)
            builder.addPart(multipartBody2)
        }
        val requestBody: MultipartBody = builder.build()
        mWaitingDialog!!.show()
        viewModel.setUploadEstimatinPicture(token, bookingId, requestBody)
    }

    private fun saveImage(myBitmap: Bitmap): String {
        val bytes = ByteArrayOutputStream()
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes)
        val wallpaperDirectory = getDir("images", Context.MODE_PRIVATE)

        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs()
        }
        try {
            val f = File(
                wallpaperDirectory,
                ((Calendar.getInstance().timeInMillis).toString() + ".jpg")
            )
            f.createNewFile()
            val fo = FileOutputStream(f)
            fo.write(bytes.toByteArray())
            MediaScannerConnection.scanFile(
                this,
                arrayOf(f.path),
                arrayOf("image/jpeg"), null
            )
            fo.close()
            return f.absolutePath
        } catch (e1: IOException) {
            e1.printStackTrace()
        }
        return ""
    }

    fun startServiceDialog(view: View) {
        if (firstImage.equals("")) {
            Utility.showSnackBar(
                activityEstimationUploadBinding.tvPhotoProof,
                "Upload before service image",
                this@EstimationUploadActivity
            )
        } else {
            val dialogBuilder = Dialog(this)
            val inflater = LayoutInflater.from(this)
            val binding: LayoutServiceTimerBinding = DataBindingUtil.inflate(
                inflater,
                R.layout.layout_service_timer,
                null,
                false
            )
            dialogBuilder.setContentView(binding.root)
            val mWindow = dialogBuilder.window
            mWindow!!.setLayout(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT
            )
            mWindow.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
            mWindow.setDimAmount(0.2f)
            mWindow.setGravity(Gravity.BOTTOM)
            mWindow.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            val lp = WindowManager.LayoutParams()
            lp.copyFrom(mWindow.attributes)
            lp.width = WindowManager.LayoutParams.MATCH_PARENT
            mWindow.attributes = lp
            dialogBuilder.setCancelable(false)
            dialogBuilder.setCanceledOnTouchOutside(true)
            dialogBuilder.show()
            var count = 0
            val sdf = SimpleDateFormat("hh:mm:ss")
            currentTime = sdf.format(Date())
            var timer: Timer? = null

            binding.btnTimer.setOnClickListener { v ->
                if (count == 0) {
                    timer = Timer()
                    count = 1
                    binding.btnTimer.setText("Stop Service")
                    timer?.scheduleAtFixedRate(object : TimerTask() {
                        override fun run() {
                            runOnUiThread {
                                updateTime = "${SimpleDateFormat("hh:mm:ss").format(Date())}"
                                val df: DateFormat = SimpleDateFormat("hh:mm:ss")
                                val date1 = df.parse(updateTime)
                                val date2 = df.parse(currentTime)
                                difference = (date1.time - date2.time).toInt()
                                var timeInSeconds = difference / 1000;
                                val hours = timeInSeconds / 3600;
                                timeInSeconds = timeInSeconds - (hours * 3600);
                                val minutes = timeInSeconds / 60;
                                timeInSeconds = timeInSeconds - (minutes * 60);
                                val seconds = timeInSeconds
                                if (hours < 10) {
                                    hour = "0" + hours
                                } else {
                                    hour = hours.toString()
                                }
                                if (minutes < 10) {
                                    minute = "0" + minutes
                                } else {
                                    minute = minutes.toString()
                                }
                                if (seconds < 10) {
                                    second = "0" + seconds
                                } else {
                                    second = seconds.toString()
                                }
                                binding.tvFirst.setText(hour)
                                binding.tvSecond.setText(minute)
                                binding.tvThird.setText(second)
                            }
                        }
                    }, 0, 1000)
                } else if (count == 1) {
                    count = 2
                    timer?.cancel()
                    binding.btnTimer.setText("Close")
                } else {
                    dialogBuilder.dismiss()
                }
            }
        }
    }
}
