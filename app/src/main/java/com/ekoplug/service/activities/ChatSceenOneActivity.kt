package com.ekoplug.service.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.ekoplug.model.ChatFirebaseModel
import com.ekoplug.service.R
import com.ekoplug.service.adapters.ChatScreenAdapter
import com.ekoplug.service.databinding.ActivityChatSceenBinding
import com.ekoplug.utils.AppPreference
import com.ekoplug.utils.Utility
import com.google.firebase.Timestamp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ListenerRegistration
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_chat_sceen.*

class ChatSceenOneActivity : AppCompatActivity() {

    lateinit var activityChatScreenBinding : ActivityChatSceenBinding
    var chatRegistration: ListenerRegistration? = null
    var bookingId = 0
    var userId = 0
    val auth = FirebaseAuth.getInstance()
    val user = auth.currentUser
    val firestore = FirebaseFirestore.getInstance()
    var roomId = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityChatScreenBinding = DataBindingUtil.setContentView(this,R.layout.activity_chat_sceen)
        bookingId = intent.getIntExtra("bookingId",0)
        userId = AppPreference.getIntPreferenceValueByKey(this@ChatSceenOneActivity, "Id")!!
        roomId = bookingId.toString()
        val name = intent.getStringExtra("name")
        activityChatScreenBinding.tvHeader.text = name
        initChat()
        getChatMessage()
    }

    private fun sendChat() {
        val message = activityChatScreenBinding.tvMessage.text.toString()
        activityChatScreenBinding.tvMessage.setText("")
        firestore.collection("chat").document(roomId).collection("messages")
            .add(
                mapOf(
                    Pair("senderId", userId),
                    Pair("timestamp", Timestamp.now()),
                    Pair("msg", message)
                )
            )
    }

    override fun onDestroy() {
        chatRegistration?.remove()
        super.onDestroy()
    }

    private fun initChat() {
        activityChatScreenBinding.sendBtn.setOnClickListener { v ->
            if(activityChatScreenBinding.tvMessage.text.trim().toString().equals("")){
                Utility.showSnackBar(
                    activityChatScreenBinding.tvHeader,
                    "Please enter message",
                    this@ChatSceenOneActivity
                )
            }else {
                sendChat()
            }
        }
    }

    private fun getChatMessage() {
        FirebaseFirestore.getInstance().collection("chat").document(roomId)
            .collection("messages").orderBy("timestamp").addSnapshotListener { data, error ->
                if (error != null) {
                    Log.d("TAG", "error ")
                } else {
                    if (data != null) {
                        val list: List<ChatFirebaseModel> = data.toObjects(ChatFirebaseModel::class.java)
                        for (x in 0 until list.size) {
                            println("data : " + Gson().toJson(list[x]))
                        }
                        val linearLayout = LinearLayoutManager(this@ChatSceenOneActivity)
                        linearLayout.stackFromEnd = true
                        linearLayout.reverseLayout = true
                        rv_chat.layoutManager = linearLayout
                        val adapter = ChatScreenAdapter(list, this@ChatSceenOneActivity, userId)
                        rv_chat.adapter = adapter

                    } else {
                        Log.d("TAG", "Current data: null")
                    }

                }
            }
    }

}
