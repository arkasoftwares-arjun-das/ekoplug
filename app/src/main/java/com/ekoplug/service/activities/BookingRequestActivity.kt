package com.ekoplug.service.activities

import android.os.Bundle
import android.view.View
import androidx.annotation.Nullable
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.ekoplug.model.BookingRequestModel
import com.ekoplug.service.R
import com.ekoplug.service.adapters.BookingRequestAdapter
import com.ekoplug.service.basesetup.BaseResponseArrayModel
import com.ekoplug.service.databinding.ActivityBookingRequestBinding
import com.ekoplug.service.viewmodel.BookingRequestViewModel
import com.ekoplug.utils.AppPreference
import com.ekoplug.utils.Utility
import com.ekoplug.utils.Watting

class BookingRequestActivity : AppCompatActivity() {

    lateinit var bookingRequestBinding: ActivityBookingRequestBinding
    private var mWaitingDialog: Watting? = null
    lateinit var bookingRequestViewModel: BookingRequestViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bookingRequestBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_booking_request)
        mWaitingDialog = Watting(this, "")
        val token = AppPreference.getPreferenceValueByKey(this, "Token").toString()
        callApi()
        mWaitingDialog!!.show()
        bookingRequestViewModel.setBookingRequestModel(token)
    }

    private fun callApi() {
        bookingRequestViewModel =
            ViewModelProviders.of(this).get(BookingRequestViewModel::class.java)

        bookingRequestViewModel.getBookingData()
            .observe(
                this,
                object : Observer<BaseResponseArrayModel<BookingRequestModel>> {
                    override
                    fun onChanged(@Nullable bookingRequestModel: BaseResponseArrayModel<BookingRequestModel>?) {
                        mWaitingDialog!!.dismiss()
                        if (bookingRequestModel != null) {
                            if (bookingRequestModel.getCode() == 200) {

                                if (bookingRequestModel.getData()!!.isEmpty()) {
                                    bookingRequestBinding.tvDataNotFoundCategory.visibility =
                                        View.VISIBLE
                                    bookingRequestBinding.rvBookingRequest.visibility = View.GONE
                                } else {
                                    bookingRequestBinding.rvBookingRequest.visibility =
                                        View.VISIBLE
                                    bookingRequestBinding.tvDataNotFoundCategory.visibility =
                                        View.GONE
                                    setAdapter(bookingRequestModel.getData()!!)
                                }
                            } else {
                                Utility.showSnackBar(
                                    bookingRequestBinding.tvDataNotFoundCategory,
                                    bookingRequestModel.getMessage(),
                                    this@BookingRequestActivity
                                )
                            }
                        } else {
                            Utility.showSnackBar(
                                bookingRequestBinding.tvDataNotFoundCategory,
                                getString(R.string.server_error),
                                this@BookingRequestActivity
                            )
                        }
                    }
                })

        bookingRequestViewModel.error.observe(this, Observer {
            mWaitingDialog!!.dismiss()
            Utility.showSnackBar(
                bookingRequestBinding.tvDataNotFoundCategory,
                bookingRequestViewModel.error.value.toString(),
                this
            )
        })
    }


    private fun setAdapter(data: ArrayList<BookingRequestModel>) {
        bookingRequestBinding.rvBookingRequest.layoutManager =
            LinearLayoutManager(this)
        val bookingRequestAdapter =
            BookingRequestAdapter(
                this,
                data,
                "bookingRequest"
            )
        bookingRequestBinding.rvBookingRequest.adapter = bookingRequestAdapter

    }

    fun backBtn(view: View) {
        finish()
    }

}
