package com.ekoplug.service.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import com.ekoplug.service.R
import com.ekoplug.service.adapters.ReviewRatingsTabAdapter
import com.ekoplug.service.databinding.ActivityReviewRatingsBinding

class ReviewRatingsActivity : AppCompatActivity() {

    lateinit var activityReviewRatingsBinding : ActivityReviewRatingsBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityReviewRatingsBinding = DataBindingUtil.setContentView(this,R.layout.activity_review_ratings)

        val reviewRatingsAdapter = ReviewRatingsTabAdapter(supportFragmentManager)
        activityReviewRatingsBinding.viewPagerReviewRatings.setAdapter(reviewRatingsAdapter)
        activityReviewRatingsBinding.tabLayoutReviewRating.setupWithViewPager(activityReviewRatingsBinding.viewPagerReviewRatings)
    }
    fun backBtnReview(view : View){
        finish()
    }
}
