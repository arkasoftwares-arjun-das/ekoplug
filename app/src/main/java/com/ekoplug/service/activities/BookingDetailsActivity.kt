package com.ekoplug.service.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.annotation.Nullable
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.ekoplug.service.R
import com.ekoplug.service.basesetup.BaseResponseArrayModel
import com.ekoplug.service.databinding.ActivityBookingDetailsBinding
import com.ekoplug.service.model.MyBookingListModel
import com.ekoplug.service.viewmodel.UpcomingViewModel
import com.ekoplug.utils.AppPreference
import com.ekoplug.utils.Utility
import com.ekoplug.utils.Watting
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.upcoming_fragment.*
import java.text.SimpleDateFormat
import java.util.*


class BookingDetailsActivity : AppCompatActivity() {

    lateinit var activityBookingDetailsActivity: ActivityBookingDetailsBinding
    private var mWaitingDialog: Watting? = null
    private lateinit var viewModel: UpcomingViewModel
    private val FORMAT = "%02d:%02d:%02d"
    var myBookingModel: ArrayList<MyBookingListModel>? = null
    var bookingId = 0
    var token = ""
    var position = 0
    var username = ""
    var userId = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityBookingDetailsActivity =
            DataBindingUtil.setContentView(this, R.layout.activity_booking_details)
        mWaitingDialog = Watting(this, "")
        callApi()
        token = AppPreference.getPreferenceValueByKey(this, "Token").toString()
        position = intent.getIntExtra("bookingData", 0)
        callApi()
    }

    override fun onResume() {
        super.onResume()
        viewModel.setMyBookingData(token)
    }

    private fun callApi() {
        viewModel = ViewModelProviders.of(this).get(UpcomingViewModel::class.java)
        viewModel.getMyBookingData()
            .observe(this, object : Observer<BaseResponseArrayModel<MyBookingListModel>> {
                override
                fun onChanged(@Nullable mybookingModel: BaseResponseArrayModel<MyBookingListModel>?) {
                    mWaitingDialog!!.dismiss()
                    if (mybookingModel != null) {

                        if (mybookingModel.getCode() == 200) {
                            if (mybookingModel.getData()!!.isNotEmpty()) {
                                setData(mybookingModel.getData()!!)
                                myBookingModel = mybookingModel.getData()
                            }
                        } else {
                            Utility.showSnackBar(
                                rv_upcomingBooking,
                                mybookingModel.getMessage(),
                                this@BookingDetailsActivity
                            )
                        }

                    } else {
                        Utility.showSnackBar(
                            rv_upcomingBooking,
                            getString(R.string.server_error),
                            this@BookingDetailsActivity
                        )
                    }
                }
            })

        viewModel.error.observe(this, Observer {
            mWaitingDialog!!.dismiss()
            Utility.showSnackBar(
                rv_upcomingBooking, viewModel.error.value.toString(),
                this@BookingDetailsActivity
            )
        })
    }

    private fun setData(data: ArrayList<MyBookingListModel>) {
        bookingId = data.get(position)!!.id
        userId = data.get(position).user_id
        username = data.get(position).user
        Picasso.get()
            .load(Utility.imageURL + "" + data.get(position).user_profile)
            .into(activityBookingDetailsActivity.civBookingDetailsImage)
        activityBookingDetailsActivity.tvBookingDetailsEmailsType.setText(data.get(position).email)
        activityBookingDetailsActivity.tvBookingDetailsAddress.setText(data.get(position).visit_address)
        activityBookingDetailsActivity.tvBookingDetailsName.setText(data.get(position).user)
        var date1 = data.get(position).appointment_time
        var spf1 = SimpleDateFormat("HH:mm:ss")
        val newDate1: Date = spf1.parse(date1)
        spf1 = SimpleDateFormat("hh:mm aa")
        date1 = spf1.format(newDate1)

        var date = data.get(position).appointment_date
        var spf = SimpleDateFormat("dd-MM-yyyy")
        val newDate: Date = spf.parse(date)
        spf = SimpleDateFormat("dd MMM yyyy")
        date = spf.format(newDate)
        activityBookingDetailsActivity.tvProviderDetailsJobComppleted.setText(
           date+ " " + date1)

        activityBookingDetailsActivity.tvRatingbooking.setText(data.get(position).rating.toString())
        activityBookingDetailsActivity.tvBookingDetailsMobile.setText(data.get(position).phone_number)
        activityBookingDetailsActivity.tvBookingDetailsType.setText(data.get(position).job_category)
        if (data.get(position).estimated) {
            activityBookingDetailsActivity.btnStartService.setOnClickListener { v ->
                if (data.get(position).additional_amount_paid) {
                    startActivity(
                        Intent(this@BookingDetailsActivity, ServiceStartActivity::class.java)
                            .putExtra("amount", data.get(position).additional_pay_amount)
                            .putExtra("mode", data.get(position).additional_pay_method)
                            .putExtra("bookingID", bookingId)
                            .putExtra("userId",userId)
                    )
                } else {
                    Utility.showSnackBar(
                        activityBookingDetailsActivity.tvBookingDetailsAddress,
                        "Estimated payment not done by user !",
                        this@BookingDetailsActivity
                    )
                }
            }

        } else {
            activityBookingDetailsActivity.btnStartService.setOnClickListener { v ->
                startActivity(
                    Intent(this@BookingDetailsActivity, EstimationUploadActivity::class.java)
                        .putExtra("bookingID", bookingId)
                        .putExtra("userId",userId)
                )
            }
        }
        activityBookingDetailsActivity.btnEstimation.setOnClickListener { v ->
            if (data.get(position).estimated) {
                Utility.showSnackBar(
                    activityBookingDetailsActivity.tvBookingDetailsAddress,
                    "Estimation is already done !",
                    this@BookingDetailsActivity
                )
            } else {
                startActivity(
                    Intent(
                        this@BookingDetailsActivity,
                        EstimationActivity::class.java
                    ).putExtra("bookingId", bookingId)
                )
            }
        }
    }

    fun chatBtn(view: View) {
        startActivity(
            Intent(
                this@BookingDetailsActivity,
                ChatScreenActivity::class.java
            ).putExtra("bookingId", bookingId)
                .putExtra("name", username)
                .putExtra(
                    "userId",
                    AppPreference.getIntPreferenceValueByKey(this@BookingDetailsActivity, "Id")
                )
        )
    }

    fun backBtnDetails(view: View) {
        finish()
    }
}
