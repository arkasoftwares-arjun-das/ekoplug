package com.ekoplug.service.activities

import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.annotation.Nullable
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.ekoplug.model.NotificationModel
import com.ekoplug.service.R
import com.ekoplug.service.basesetup.BaseResponseModel
import com.ekoplug.service.databinding.ActivityHomeBinding
import com.ekoplug.service.model.ForgotPasswardResponseModel
import com.ekoplug.service.model.HomeModel
import com.ekoplug.service.viewmodel.HomeViewModel
import com.ekoplug.utils.AppPreference
import com.ekoplug.utils.Utility
import com.ekoplug.utils.Watting
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.InstanceIdResult
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_home.*


class HomeActivity : AppCompatActivity() {

    private var mWaitingDialog: Watting? = null
    private lateinit var viewModel: HomeViewModel
    lateinit var activityHomeBinding: ActivityHomeBinding
    var token = ""
    var fcmToken = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityHomeBinding = DataBindingUtil.setContentView(this, R.layout.activity_home)
        val connected = Utility.isNetworkConnected(this)
        mWaitingDialog = Watting(this, "")
        if (!connected) {
            Toast.makeText(this, "Please check your internet connection !", Toast.LENGTH_LONG)
                .show()
        } else {
            init()
            callDeviceInfo()
            token = AppPreference.getPreferenceValueByKey(this@HomeActivity, "Token").toString()
            val deviceId =
                Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID)
            FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener(
                this
            ) { instanceIdResult: InstanceIdResult ->
                fcmToken = instanceIdResult.token
                Log.e("newToken", fcmToken)
                viewModel.setDeviceInfo(fcmToken!!, token, deviceId)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        mWaitingDialog!!.show()
        viewModel.setHomeData(token)
        viewModel.setNotificationHomeModel(token)
    }

    private fun init() {
        viewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)
        viewModel.getHomeResponseModel()
            .observe(this, object : Observer<BaseResponseModel<HomeModel>> {
                override
                fun onChanged(@Nullable homeModel: BaseResponseModel<HomeModel>?) {
                    mWaitingDialog!!.dismiss()
                    if (homeModel != null) {
                        if (homeModel.getCode() == 200) {
                            activityHomeBinding.tvName.text = homeModel.getData()!!.username
                            activityHomeBinding.tvOne.text =
                                homeModel.getData()!!.booking_request.toString()
                            activityHomeBinding.tvTwo.text =
                                homeModel.getData()!!.current_jobs.toString()
                        } else {
                            Utility.showSnackBar(
                                tvNotification,
                                homeModel.getMessage(),
                                this@HomeActivity
                            )
                        }
                    } else {
                        Utility.showSnackBar(
                            tvNotification,
                            getString(R.string.server_error),
                            this@HomeActivity
                        )
                    }
                }
            })

        viewModel.getNotifidationHome()
            .observe(this, object : Observer<BaseResponseModel<NotificationModel>> {
                override
                fun onChanged(@Nullable homeModel: BaseResponseModel<NotificationModel>?) {
                    mWaitingDialog!!.dismiss()
                    if (homeModel != null) {
                        if (homeModel.getCode() == 200) {
                            activityHomeBinding.clNotificationlayout.visibility = View.VISIBLE
                            activityHomeBinding.tvNotifictionUserName.text =
                                homeModel.getData()!!.username
                            Picasso.get()
                                .load(Utility.imageURL + "" + homeModel.getData()!!.profile_image)
                                .into(activityHomeBinding.civNotificationImage)
                            activityHomeBinding.tvNotifictionUserTemplate.text =
                                homeModel.getData()!!.job_category
                            activityHomeBinding.tvDate.text =
                                homeModel.getData()!!.appointment_date + " " + homeModel.getData()!!.appointment_time
                        } else {
                            /* Utility.showSnackBar(
                                 tvNotification,
                                 homeModel.getMessage(),
                                 this@HomeActivity
                             )*/
                        }
                    } else {
                        Utility.showSnackBar(
                            tvNotification,
                            getString(R.string.server_error),
                            this@HomeActivity
                        )
                    }
                }
            })

        viewModel.error.observe(this, Observer {
            mWaitingDialog!!.dismiss()
            Utility.showSnackBar(
                tvNotification, viewModel.error.value.toString(),
                this@HomeActivity
            )
        })
    }

    private fun callDeviceInfo() {
        viewModel.getDeviceInfo()
            .observe(this, object : Observer<BaseResponseModel<ForgotPasswardResponseModel>> {
                override
                fun onChanged(@Nullable homeModel: BaseResponseModel<ForgotPasswardResponseModel>?) {
                    mWaitingDialog!!.dismiss()
                    if (homeModel != null) {
                        if (homeModel.getCode() == 200) {
                            /* Utility.showSnackBar(
                                 tvName,
                                 homeModel.getMessage(),
                                 requireContext()
                             )*/
                        } else {
                            Utility.showSnackBar(
                                tvName,
                                homeModel.getMessage(),
                                this@HomeActivity
                            )
                        }
                    } else {
                        Utility.showSnackBar(
                            tvName,
                            getString(R.string.server_error),
                            this@HomeActivity
                        )
                    }
                }
            })

        viewModel.error.observe(this, Observer {
            mWaitingDialog!!.dismiss()
            Utility.showSnackBar(
                tvName, viewModel.error.value.toString(),
                this@HomeActivity
            )
        })
    }

    fun cardOne(view: View) {
        startActivity(Intent(this@HomeActivity, BookingRequestActivity::class.java))
    }

    fun seeAllNotification(view: View) {
        startActivity(Intent(this@HomeActivity, NotificationActivity::class.java))
    }

    fun cardTwo(view: View) {
        startActivity(Intent(this@HomeActivity, CurrentActivity::class.java))
    }

    fun notifcationClick(view: View) {
        startActivity(Intent(this@HomeActivity, NotificationActivity::class.java))
    }

    fun profileClick(view: View) {
        startActivity(Intent(this@HomeActivity, ProfileActivity::class.java))
    }

    fun sideMenuClick(view: View) {
        startActivity(Intent(this@HomeActivity, SideMenuActivity::class.java))
    }

}
