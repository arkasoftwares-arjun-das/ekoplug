package com.ekoplug.service.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentManager
import com.ekoplug.service.R
import com.ekoplug.service.databinding.ActivitySideMenuBinding
import com.ekoplug.utils.AppPreference

class SideMenuActivity : AppCompatActivity() {

    lateinit var sideMenuBinding: ActivitySideMenuBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sideMenuBinding = DataBindingUtil.setContentView(this, R.layout.activity_side_menu)
    }

    fun editProfile(view: View) {
        startActivity(Intent(this@SideMenuActivity, EditProfileActivity::class.java))
    }

    fun myBookings(view: View) {
        startActivity(Intent(this@SideMenuActivity, MyBookingActivity::class.java))
    }

    fun bookingRequest(view: View) {
        startActivity(Intent(this@SideMenuActivity, BookingRequestActivity::class.java))
    }

    fun paymentHistory(view: View) {
        startActivity(Intent(this@SideMenuActivity, PaymntHistoryActivity::class.java))
    }

    fun reviewRatings(view: View) {
        startActivity(Intent(this@SideMenuActivity, ReviewRatingsActivity::class.java))
    }

    fun settings(view: View) {
        startActivity(Intent(this@SideMenuActivity, SettingsActivity::class.java))

    }

    fun logout(view: View) {
        val builder = AlertDialog.Builder(this@SideMenuActivity)
        builder.setTitle("Alert")
        builder.setMessage("Are you sure you want to Logout !")
        builder.setPositiveButton(android.R.string.yes) { dialog, which ->
            AppPreference.saveIntPreference(
                this@SideMenuActivity, "Id",
                0
            )
            AppPreference.savePreference(
                this@SideMenuActivity, "Email",
                ""
            )
            AppPreference.savePreference(
                this@SideMenuActivity, "Token",
                ""
            )
            AppPreference.savePreference(
                this@SideMenuActivity, "Name",
                ""
            )
            startActivity(Intent(this@SideMenuActivity, LoginActivity::class.java))
            finishAffinity()
        }
        builder.setNegativeButton(android.R.string.no) { dialog, which ->
            builder.create().dismiss()
        }
        builder.show().setCancelable(true)
    }

    fun homeClick(view: View) {
        startActivity(Intent(this@SideMenuActivity, HomeActivity::class.java))
        finishAffinity()
    }

    fun notificationClick(view: View) {
        startActivity(Intent(this@SideMenuActivity, NotificationActivity::class.java))
    }

    fun profileClick(view: View) {
        startActivity(Intent(this@SideMenuActivity, ProfileActivity::class.java))
    }
}
