package com.ekoplug.service.activities

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.ekoplug.service.R
import com.ekoplug.service.adapters.MyBookingTabAdapter
import com.ekoplug.service.databinding.ActivityMyBookingBinding

class MyBookingActivity : AppCompatActivity() {

    lateinit var myBookingRequestBinding: ActivityMyBookingBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        myBookingRequestBinding = DataBindingUtil.setContentView(this, R.layout.activity_my_booking)


        val myBookingAdapter = MyBookingTabAdapter(supportFragmentManager)
        myBookingRequestBinding.viewPagerBooking.setAdapter(myBookingAdapter)
        myBookingRequestBinding.tabLayout.setupWithViewPager(myBookingRequestBinding.viewPagerBooking)
    }

    fun backBtnBooking(view: View) {
        finish()
    }

}
