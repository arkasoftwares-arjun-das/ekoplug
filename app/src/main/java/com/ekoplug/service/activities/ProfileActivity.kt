package com.ekoplug.service.activities

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.media.MediaScannerConnection
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import androidx.annotation.Nullable
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.ekoplug.model.AddressProfile
import com.ekoplug.model.ProfileModel
import com.ekoplug.model.UpdateProfileMdel
import com.ekoplug.service.R
import com.ekoplug.service.basesetup.BaseResponseModel
import com.ekoplug.service.databinding.ActivityProfileBinding
import com.ekoplug.service.viewmodel.ProfileViewModel
import com.ekoplug.utils.AppPreference
import com.ekoplug.utils.Utility
import com.ekoplug.utils.Watting
import com.squareup.picasso.Picasso
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.*

class ProfileActivity : AppCompatActivity() {

    lateinit var profileActivity: ActivityProfileBinding
    private var mWaitingDialog: Watting? = null
    private lateinit var viewModel: ProfileViewModel
    private var imageUrl: Uri? = null
    private var profilePic: File? = null
    var token = ""
    var userId = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        profileActivity = DataBindingUtil.setContentView(this, R.layout.activity_profile)
        token = AppPreference.getPreferenceValueByKey(this@ProfileActivity, "Token").toString()
        userId = AppPreference.getIntPreferenceValueByKey(this@ProfileActivity, "Id")!!
        callApi()
        mWaitingDialog = Watting(this, "")
        viewModel.setProfileData(token, userId)

    }

    private fun callApi() {
        viewModel = ViewModelProviders.of(this).get(ProfileViewModel::class.java)
        viewModel.getProfileModel()
            .observe(this, object :
                Observer<BaseResponseModel<ProfileModel<AddressProfile>>> {
                override
                fun onChanged(@Nullable profileModel: BaseResponseModel<ProfileModel<AddressProfile>>?) {
                    mWaitingDialog!!.dismiss()
                    if (profileModel != null) {
                        if (profileModel.getCode() == 200) {
                            profileActivity.tvProfileName.text =
                                profileModel.getData()!!.username
                            profileActivity.tvRating.text =
                                profileModel.getData()!!.ratings.toString()
                            profileActivity.tvEmailProfile.text =
                                profileModel.getData()!!.email
                            profileActivity.tvMobileNumber.text =
                                profileModel.getData()!!.phone_number
                            profileActivity.tvProfileAddress.text =
                                profileModel.getData()!!.address!!.address
                            profileActivity.tvGenderMale.text =
                                profileModel.getData()!!.gender

                            Picasso.get()
                                .load(Utility.imageURL + "" + profileModel.getData()!!.profile_image)
                                .into(profileActivity.civProfileImage)
                        } else {
                            Utility.showSnackBar(
                                profileActivity.tvAddress,
                                profileModel.getMessage(),
                                this@ProfileActivity
                            )
                        }
                    } else {
                        Utility.showSnackBar(
                            profileActivity.tvAddress,
                            getString(R.string.server_error),
                            this@ProfileActivity
                        )
                    }
                }
            })
        viewModel.getProfilePictureModel()
            .observe(this, object :
                Observer<BaseResponseModel<UpdateProfileMdel>> {
                override
                fun onChanged(@Nullable profileModel: BaseResponseModel<UpdateProfileMdel>?) {
                    mWaitingDialog!!.dismiss()
                    if (profileModel != null) {
                        if (profileModel.getCode() == 200) {
                            Utility.showSnackBar(
                                profileActivity.tvAddress,
                                profileModel.getMessage(),
                                this@ProfileActivity
                            )

                        } else {
                            Utility.showSnackBar(
                                profileActivity.tvAddress,
                                profileModel.getMessage(),
                                this@ProfileActivity
                            )
                        }
                    } else {
                        Utility.showSnackBar(
                            profileActivity.tvAddress,
                            getString(R.string.server_error),
                            this@ProfileActivity
                        )
                    }
                }
            })
        viewModel.error.observe(this, Observer {
            mWaitingDialog!!.dismiss()
            Utility.showSnackBar(
                profileActivity.tvAddress, viewModel.error.value.toString(),
                this@ProfileActivity
            )
        })
    }

    fun editImage(view: View) {
        val builder = AlertDialog.Builder(this@ProfileActivity)
        builder.setTitle("Alert")
        builder.setMessage("Are you sure you want to update profile picture !")
        //builder.setPositiveButton("OK", DialogInterface.OnClickListener(function = x))
        builder.setPositiveButton(android.R.string.yes) { dialog, which ->
            onSelectImage()
        }

        builder.setNegativeButton(android.R.string.no) { dialog, which ->
            builder.create().dismiss()
        }
        builder.show().setCancelable(true)
    }

    fun notifcationClick(view: View) {
        startActivity(Intent(this@ProfileActivity, NotificationActivity::class.java))
    }

    fun homeClick(view: View) {
        startActivity(Intent(this@ProfileActivity, HomeActivity::class.java))
    }

    fun sideMenu(view: View) {
        startActivity(Intent(this@ProfileActivity, SideMenuActivity::class.java))
    }

    private fun onSelectImage() {
        CropImage.startPickImageActivity(this)
    }

    private fun startCropActivity(imageUri: Uri) {
        CropImage.activity(imageUri).setGuidelines(CropImageView.Guidelines.ON)
            .setMultiTouchEnabled(true)
            .setAspectRatio(1, 1)
            .start(this)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            val imageUri = CropImage.getPickImageResultUri(this, data)
            if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri)) {
                // mCropImageUri = imageUri
                requestPermissions(
                    listOf(Manifest.permission.READ_EXTERNAL_STORAGE).toTypedArray(),
                    0
                )
            } else {
                startCropActivity(imageUri)
            }

        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            val result = CropImage.getActivityResult(data)
            if (resultCode == Activity.RESULT_OK) {
                imageUrl = result.originalUri
                val resultUri = result.uri
                println("Image url >>>>>>>>>>>>>$resultUri")
                try {
                    val bitmap =
                        MediaStore.Images.Media.getBitmap(this.contentResolver, resultUri)
                    profileActivity.civProfileImage.setImageURI(resultUri)
                    val filePath = saveImage(bitmap)
                    profilePic = File(filePath)
                    /*  reqFile = RequestBody.create(
                          MediaType.parse("multipart/form-data"),
                          profilePic!!
                      )
                      body = MultipartBody.Part.createFormData(
                          "profile_image",
                          profilePic!!.name,
                          reqFile
                      )*/
                    uploadPictureApi(profilePic!!)

                    //name ki jgh parm name h wo aa jaayega or multipart m body send kr dena ho jaayega.
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }
        }
    }

    private fun uploadPictureApi(profilePic: File) {
        val builder = MultipartBody.Builder()
        builder.setType(MultipartBody.FORM)
        builder.addFormDataPart("profile_image", profilePic.name)

        if (!profilePic.equals("")) {
            val file = profilePic
            val requestFile: RequestBody = RequestBody.create(
                MediaType.parse("multipart/form-data"),
                file
            )
            val multipartBody = MultipartBody.Part.createFormData(
                "profile_image", profilePic!!.name, requestFile
            )
            builder.addPart(multipartBody)
        }
        val requestBody: MultipartBody = builder.build()
        mWaitingDialog!!.show()
        viewModel.setProfilePictureData(token, userId, requestBody)
    }

    private fun saveImage(myBitmap: Bitmap): String {
        val bytes = ByteArrayOutputStream()
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes)
        val wallpaperDirectory = getDir("images", Context.MODE_PRIVATE)

        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs()
        }
        try {
            val f = File(
                wallpaperDirectory,
                ((Calendar.getInstance().timeInMillis).toString() + ".jpg")
            )
            f.createNewFile()
            val fo = FileOutputStream(f)
            fo.write(bytes.toByteArray())
            MediaScannerConnection.scanFile(
                this,
                arrayOf(f.path),
                arrayOf("image/jpeg"), null
            )
            fo.close()
            return f.absolutePath
        } catch (e1: IOException) {
            e1.printStackTrace()
        }
        return ""
    }

    fun editProfile(view: View) {
        startActivity(Intent(this@ProfileActivity, EditProfileActivity::class.java))
    }

    fun changePassword(view: View) {
        startActivity(Intent(this@ProfileActivity, ChangePasswodSecondActivity::class.java))
    }

    fun reviewsCall(view: View) {
        startActivity(Intent(this@ProfileActivity, ReviewRatingsActivity::class.java))
    }

}
