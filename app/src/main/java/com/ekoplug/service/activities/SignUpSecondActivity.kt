package com.ekoplug.service.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.ekoplug.service.R
import com.ekoplug.service.databinding.ActivitySignUpSecondBinding
import com.ekoplug.service.model.SignUpResquestModel
import com.ekoplug.utils.Utility
import com.ekoplug.utils.Watting
import org.w3c.dom.Text

class SignUpSecondActivity : AppCompatActivity() {

    lateinit var activitySignUpSecondBinding : ActivitySignUpSecondBinding
    private var mWaitingDialog: Watting? = null
    var signUpResquestModel : SignUpResquestModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activitySignUpSecondBinding = DataBindingUtil.setContentView(this,R.layout.activity_sign_up_second)
        init()
        signUpResquestModel = intent!!.getSerializableExtra("sendData") as SignUpResquestModel?
    }

    private fun init() {
        activitySignUpSecondBinding.etHolderName.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                activitySignUpSecondBinding.etHolderName.setCompoundDrawablesWithIntrinsicBounds(
                    ContextCompat.getDrawable(
                        this,
                        R.drawable.profile_blue
                    ),
                    null,
                    null,
                    null
                )
                activitySignUpSecondBinding.etHolderName.setBackgroundResource(R.drawable.login_textview_blue)
                activitySignUpSecondBinding.etHolderName.setTextColor(resources.getColor(R.color.colorPrimary))
                activitySignUpSecondBinding.etHolderName.setHintTextColor(resources.getColor(R.color.colorPrimary))

            } else {
                activitySignUpSecondBinding.etHolderName.setCompoundDrawablesWithIntrinsicBounds(
                    ContextCompat.getDrawable(
                        this,
                        R.drawable.profile_grey
                    ),
                    null,
                    null,
                    null
                )
                activitySignUpSecondBinding.etHolderName.setBackgroundResource(R.drawable.login_textview_grey)
                activitySignUpSecondBinding.etHolderName.setTextColor(resources.getColor(R.color.grey))
                activitySignUpSecondBinding.etHolderName.setHintTextColor(resources.getColor(R.color.grey))
            }
        }
        activitySignUpSecondBinding.etHolderBank.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                activitySignUpSecondBinding.etHolderBank.setCompoundDrawablesWithIntrinsicBounds(
                    ContextCompat.getDrawable(
                        this,
                        R.drawable.bank_blue
                    ),
                    null,
                    null,
                    null
                )
                activitySignUpSecondBinding.etHolderBank.setBackgroundResource(R.drawable.login_textview_blue)
                activitySignUpSecondBinding.etHolderBank.setTextColor(resources.getColor(R.color.colorPrimary))
                activitySignUpSecondBinding.etHolderBank.setHintTextColor(resources.getColor(R.color.colorPrimary))

            } else {
                activitySignUpSecondBinding.etHolderBank.setCompoundDrawablesWithIntrinsicBounds(
                    ContextCompat.getDrawable(
                        this,
                        R.drawable.bank_grey
                    ),
                    null,
                    null,
                    null
                )
                activitySignUpSecondBinding.etHolderBank.setBackgroundResource(R.drawable.login_textview_grey)
                activitySignUpSecondBinding.etHolderBank.setTextColor(resources.getColor(R.color.grey))
                activitySignUpSecondBinding.etHolderBank.setHintTextColor(resources.getColor(R.color.grey))
            }
        }
        activitySignUpSecondBinding.etHolderAccount.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                activitySignUpSecondBinding.etHolderAccount.setCompoundDrawablesWithIntrinsicBounds(
                    ContextCompat.getDrawable(
                        this,
                        R.drawable.account_blue
                    ),
                    null,
                    null,
                    null
                )
                activitySignUpSecondBinding.etHolderAccount.setBackgroundResource(R.drawable.login_textview_blue)
                activitySignUpSecondBinding.etHolderAccount.setTextColor(resources.getColor(R.color.colorPrimary))
                activitySignUpSecondBinding.etHolderAccount.setHintTextColor(resources.getColor(R.color.colorPrimary))

            } else {
                activitySignUpSecondBinding.etHolderAccount.setCompoundDrawablesWithIntrinsicBounds(
                    ContextCompat.getDrawable(
                        this,
                        R.drawable.account_grey
                    ),
                    null,
                    null,
                    null
                )
                activitySignUpSecondBinding.etHolderAccount.setBackgroundResource(R.drawable.login_textview_grey)
                activitySignUpSecondBinding.etHolderAccount.setTextColor(resources.getColor(R.color.grey))
                activitySignUpSecondBinding.etHolderAccount.setHintTextColor(resources.getColor(R.color.grey))
            }
        }
    }

    fun signUpSecondBack(view: View) {
        finish()
    }

    fun signUpSecond(view: View) {
        Utility.hideKeyboard(this@SignUpSecondActivity, activitySignUpSecondBinding.etHolderAccount)
    }

    fun signUpBtnSecond(view: View) {
        val holderName = activitySignUpSecondBinding.etHolderName.text.toString()
        val accountNumber = activitySignUpSecondBinding.etHolderAccount.text.toString()
        val holderBank = activitySignUpSecondBinding.etHolderBank.text.toString()
        if(TextUtils.isEmpty(activitySignUpSecondBinding.etHolderName.text.trim())){
            Utility.showSnackBar(
                activitySignUpSecondBinding.etHolderName,
                "Enter account holder name",
                this@SignUpSecondActivity
            )
        }else if(TextUtils.isEmpty(activitySignUpSecondBinding.etHolderBank.text.trim())){
            Utility.showSnackBar(
                activitySignUpSecondBinding.etHolderName,
                "Enter bank name",
                this@SignUpSecondActivity
            )
        }else if(TextUtils.isEmpty(activitySignUpSecondBinding.etHolderAccount.text.trim())){
            Utility.showSnackBar(
                activitySignUpSecondBinding.etHolderName,
                "Enter account number",
                this@SignUpSecondActivity
            )
        }else{
            val signUpResquestModel = SignUpResquestModel(signUpResquestModel!!.username,signUpResquestModel!!.email,signUpResquestModel!!.phone_number,signUpResquestModel!!.gender,
                    signUpResquestModel!!.age,signUpResquestModel!!.password,signUpResquestModel!!.confirm_password,signUpResquestModel!!.address,"","","",
                signUpResquestModel!!.latitude,signUpResquestModel!!.longitude,holderName,holderBank,accountNumber,signUpResquestModel!!.profile_image,signUpResquestModel!!.jobcategories)
            startActivity(Intent(this,SignUpThirdActivity::class.java).putExtra("sendData",signUpResquestModel))
        }
    }

    fun loginInSignUp(view: View) {
        startActivity(Intent(this,LoginActivity::class.java))
        finishAffinity()
    }

}
