package com.ekoplug.service.activities

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.annotation.Nullable
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.ekoplug.model.WorkerReviewModel
import com.ekoplug.service.R
import com.ekoplug.service.adapters.ReviewsAdapter
import com.ekoplug.service.basesetup.BaseResponseArrayModel
import com.ekoplug.service.basesetup.BaseResponseModel
import com.ekoplug.service.databinding.ActivityBookingReqeustDetailsBinding
import com.ekoplug.service.model.BookingDetailsModel
import com.ekoplug.service.viewmodel.BookingRequestDetailsViewModel
import com.ekoplug.utils.AppPreference
import com.ekoplug.utils.Utility
import com.ekoplug.utils.Watting
import com.squareup.picasso.Picasso
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class BookingReqeustDetailsActivity : AppCompatActivity() {

    lateinit var activityBookingReqeustDetailsActivity: ActivityBookingReqeustDetailsBinding
    private var mWaitingDialog: Watting? = null
    lateinit var bookingRequestDetailsViewModel: BookingRequestDetailsViewModel
    var bookingDetailsModel : BookingDetailsModel? = null
    var token = ""
    var id = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityBookingReqeustDetailsActivity =
            DataBindingUtil.setContentView(this, R.layout.activity_booking_reqeust_details)
        mWaitingDialog = Watting(this, "")
        callApi()
         token = AppPreference.getPreferenceValueByKey(this, "Token").toString()
         id = intent.getIntExtra("id", 0)
        mWaitingDialog!!.show()
        bookingRequestDetailsViewModel.setBookingDetailsData(token, id)
    }

    private fun callApi() {
        bookingRequestDetailsViewModel =
            ViewModelProviders.of(this).get(BookingRequestDetailsViewModel::class.java)

        bookingRequestDetailsViewModel.getBookingDetailsData()
            .observe(
                this,
                object : Observer<BaseResponseModel<BookingDetailsModel>> {
                    override
                    fun onChanged(@Nullable bookingRequestDetailsModel: BaseResponseModel<BookingDetailsModel>?) {
                        mWaitingDialog!!.dismiss()
                        if (bookingRequestDetailsModel != null) {
                            if (bookingRequestDetailsModel.getCode() == 200) {
                                var date =  bookingRequestDetailsModel.getData()!!.appointment_date
                                var spf = SimpleDateFormat("dd-MM-yyyy")
                                val newDate: Date = spf.parse(date)
                                spf = SimpleDateFormat("dd MMM yyyy")
                                date = spf.format(newDate)

                                var date1 = bookingRequestDetailsModel.getData()!!.appointment_time
                                var spf1 = SimpleDateFormat("HH:mm:ss")
                                val newDate1: Date = spf1.parse(date1)
                                spf1 = SimpleDateFormat("hh:mm aa")
                                date1 = spf1.format(newDate1)
                                activityBookingReqeustDetailsActivity.tvProviderDetailsGender.text =
                                    bookingRequestDetailsModel.getData()!!.gender
                                activityBookingReqeustDetailsActivity.tvProviderDetailsJobComppleted.text =
                                    date+" "+ date1
                                activityBookingReqeustDetailsActivity.tvProviderDetailsName.text =
                                    bookingRequestDetailsModel.getData()!!.user_name
                                activityBookingReqeustDetailsActivity.tvRating.text =
                                    bookingRequestDetailsModel.getData()!!.rating.toString()
                                Picasso.get()
                                    .load(Utility.imageURL + "" + bookingRequestDetailsModel.getData()!!.user_profile)
                                    .into(activityBookingReqeustDetailsActivity.civProfile)

                                bookingDetailsModel = bookingRequestDetailsModel.getData()
                                bookingRequestDetailsViewModel.setReviewList(token,bookingDetailsModel!!.user_id)


                            } else {
                                Utility.showSnackBar(
                                    activityBookingReqeustDetailsActivity.tvDataNotFoundReviews,
                                    bookingRequestDetailsModel.getMessage(),
                                    this@BookingReqeustDetailsActivity
                                )
                            }
                        } else {
                            Utility.showSnackBar(
                                activityBookingReqeustDetailsActivity.tvDataNotFoundReviews,
                                getString(R.string.server_error),
                                this@BookingReqeustDetailsActivity
                            )
                        }
                    }
                })
        bookingRequestDetailsViewModel.getAcceptRequest()
            .observe(
                this,
                object : Observer<BaseResponseModel<BookingDetailsModel>> {
                    override
                    fun onChanged(@Nullable bookingRequestAcceptModel: BaseResponseModel<BookingDetailsModel>?) {
                        mWaitingDialog!!.dismiss()
                        if (bookingRequestAcceptModel != null) {
                            if (bookingRequestAcceptModel.getCode() == 200) {
                                Utility.showSnackBar(
                                    activityBookingReqeustDetailsActivity.tvDataNotFoundReviews,
                                    bookingRequestAcceptModel.getMessage(),
                                    this@BookingReqeustDetailsActivity
                                )
                                Handler().postDelayed({
                                    startActivity(Intent(this@BookingReqeustDetailsActivity,HomeActivity::class.java))
                                    finishAffinity()
                                }, 1000)

                            } else {
                                Utility.showSnackBar(
                                    activityBookingReqeustDetailsActivity.tvDataNotFoundReviews,
                                    bookingRequestAcceptModel.getMessage(),
                                    this@BookingReqeustDetailsActivity
                                )
                            }
                        } else {
                            Utility.showSnackBar(
                                activityBookingReqeustDetailsActivity.tvDataNotFoundReviews,
                                getString(R.string.server_error),
                                this@BookingReqeustDetailsActivity
                            )
                        }
                    }
                })

        bookingRequestDetailsViewModel.getReviewList()
            .observe(
                this,
                object : Observer<BaseResponseArrayModel<WorkerReviewModel>> {
                    override
                    fun onChanged(@Nullable bookingRequestAcceptModel: BaseResponseArrayModel<WorkerReviewModel>?) {
                        mWaitingDialog!!.dismiss()
                        if (bookingRequestAcceptModel != null) {
                            if (bookingRequestAcceptModel.getCode() == 200) {
                                if(bookingRequestAcceptModel.getData().isNullOrEmpty()){

                                    activityBookingReqeustDetailsActivity.tvDataNotFoundReviews.visibility =
                                        View.VISIBLE
                                    activityBookingReqeustDetailsActivity.rvProviderDetails.visibility =
                                        View.GONE
                                } else {
                                    activityBookingReqeustDetailsActivity.rvProviderDetails.visibility =
                                        View.VISIBLE
                                    activityBookingReqeustDetailsActivity.tvDataNotFoundReviews.visibility =
                                        View.GONE
                                    setAdapter(bookingRequestAcceptModel.getData())
                                }
                            } else {
                                Utility.showSnackBar(
                                    activityBookingReqeustDetailsActivity.tvDataNotFoundReviews,
                                    bookingRequestAcceptModel.getMessage(),
                                    this@BookingReqeustDetailsActivity
                                )
                            }
                        } else {
                            Utility.showSnackBar(
                                activityBookingReqeustDetailsActivity.tvDataNotFoundReviews,
                                getString(R.string.server_error),
                                this@BookingReqeustDetailsActivity
                            )
                        }
                    }
                })

        bookingRequestDetailsViewModel.error.observe(this, Observer {
            mWaitingDialog!!.dismiss()
            Utility.showSnackBar(
                activityBookingReqeustDetailsActivity.tvDataNotFoundReviews,
                bookingRequestDetailsViewModel.error.value.toString(),
                this
            )
        })
    }

    private fun setAdapter(data: ArrayList<WorkerReviewModel>?) {
        activityBookingReqeustDetailsActivity.rvProviderDetails.layoutManager =
            LinearLayoutManager(this)
        val reivewAdapter = ReviewsAdapter(this, data)
        activityBookingReqeustDetailsActivity.rvProviderDetails.adapter = reivewAdapter
    }


    fun providerDetailsBack(view: View) {
        finish()
    }

    fun cancelBooking(view: View) {
        startActivity(Intent(this@BookingReqeustDetailsActivity,CancelRequestActivity::class.java).putExtra("id",id))
    }
    fun acceptBooking(view: View) {
        mWaitingDialog!!.show()
        bookingRequestDetailsViewModel.setAcceptRequest(token,id,"ACCEPT")
    }
}
