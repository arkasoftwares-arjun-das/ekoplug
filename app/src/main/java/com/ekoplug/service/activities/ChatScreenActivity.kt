package com.ekoplug.service.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.ekoplug.model.ChatFirebaseModel
import com.ekoplug.service.R
import com.ekoplug.service.adapters.ChatScreenAdapter
import com.ekoplug.service.databinding.ActivityChatSceenBinding
import com.ekoplug.service.databinding.ActivityChatScreenBinding
import com.ekoplug.utils.AppPreference
import com.ekoplug.utils.Utility
import com.google.firebase.Timestamp
import com.google.firebase.firestore.FirebaseFirestore
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_chat_sceen.*

class ChatScreenActivity : AppCompatActivity() {

    lateinit var activityChatScreenBinding: ActivityChatScreenBinding
    val firestore = FirebaseFirestore.getInstance()
    var roomId = ""
    var bookingId = 0
    var workerID = 0
    lateinit var chatAdapter : ChatScreenAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityChatScreenBinding = DataBindingUtil.setContentView(this,R.layout.activity_chat_screen)
        bookingId = intent.getIntExtra("bookingId",0)
        workerID = intent.getIntExtra("userId",0)
        roomId = bookingId.toString()
        val name = intent.getStringExtra("name")
        activityChatScreenBinding.tvHeader.text = name
        getChatMessage()
        init()
    }

    private fun init() {
        System.out.println("userId "+workerID)
        activityChatScreenBinding.sendBtn.setOnClickListener { v ->
            if(activityChatScreenBinding.tvMessage.text.trim().toString().equals("")){
                Utility.showSnackBar(
                    activityChatScreenBinding.tvHeader,
                    "Please enter message",
                    this@ChatScreenActivity
                )
            }else {
             System.out.println("Message"+activityChatScreenBinding.tvMessage.text.toString())
                sendChat()
            }
        }
    }

    private fun getChatMessage() {
        FirebaseFirestore.getInstance().collection("chat").document(roomId)
            .collection("messages").orderBy("timestamp").addSnapshotListener { data, error ->
                if (error != null) {
                    Log.d("TAG", "error ")
                } else {
                    if (data != null) {
                        val list: List<ChatFirebaseModel> = data.toObjects(ChatFirebaseModel::class.java)
                        for (x in 0 until list.size) {
                            println("data : " + Gson().toJson(list[x]))
                        }
                        val linearLayout = LinearLayoutManager(this@ChatScreenActivity)
                        linearLayout.stackFromEnd = true
                        linearLayout.reverseLayout = false
                        activityChatScreenBinding.rvChat.layoutManager = linearLayout
                        chatAdapter = ChatScreenAdapter(list, this@ChatScreenActivity, workerID)
                        rv_chat.adapter = chatAdapter
                        moveToBottom()
                    } else {
                        Log.d("TAG", "Current data: null")
                    }

                }
            }
    }

    private fun moveToBottom() {
        try {
            activityChatScreenBinding.rvChat.scrollToPosition(chatAdapter!!.itemCount - 1)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun sendChat() {
        val message = activityChatScreenBinding.tvMessage.text.toString()
        activityChatScreenBinding.tvMessage.setText("")
        firestore.collection("chat").document(roomId).collection("messages")
            .add(
                mapOf(
                    Pair("senderId", workerID),
                    Pair("timestamp", Timestamp.now()),
                    Pair("msg", message)
                )
            )
    }

    fun chatBack(view: View) {
        finish()
    }
}
