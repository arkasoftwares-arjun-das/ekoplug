package com.ekoplug.service.activities

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.media.MediaScannerConnection
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.text.InputType
import android.text.method.PasswordTransformationMethod
import android.util.Log
import android.util.Patterns
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import android.widget.RadioGroup
import android.widget.Toast
import androidx.annotation.Nullable
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.ekoplug.service.R
import com.ekoplug.service.adapters.JobTypeAdapter
import com.ekoplug.service.basesetup.BaseResponseArrayModel
import com.ekoplug.service.databinding.ActivitySignUpBinding
import com.ekoplug.service.databinding.DialogJobTypeBinding
import com.ekoplug.service.model.CategoryModel
import com.ekoplug.service.model.SignUpResquestModel
import com.ekoplug.service.viewmodel.CategoryViewModel
import com.ekoplug.utils.Utility
import com.ekoplug.utils.Watting
import com.google.android.gms.common.api.Status
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList


class SignUpActivity : AppCompatActivity() {

    lateinit var signUpBinding: ActivitySignUpBinding
    lateinit var ctx: Context
    lateinit var cateogryViewModel: CategoryViewModel
    var categoryData: ArrayList<CategoryModel>? = null
    var signUpResquestModel: SignUpResquestModel? = null
    var passwordSee: Boolean = false
    var confirmpasswordSee: Boolean = false
    private var mWaitingDialog: Watting? = null
    private var imageUrl: Uri? = null
    private var profilePic: File? = null
    var AUTOCOMPLETE_REQUEST_CODE = 1010
    var latitude = 0.0
    var longitude = 0.0
    var gender = ""
    var resultUri: Uri? = null
    var jobArray = ArrayList<String>()
    var jobTypeArray = ArrayList<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        signUpBinding = DataBindingUtil.setContentView(
            this,
            R.layout.activity_sign_up
        )
        ctx = this
        mWaitingDialog = Watting(this, "")
        init()
        callApi()
        cateogryViewModel.setCategoryResponse()
    }

    private fun callApi() {
        cateogryViewModel = ViewModelProviders.of(this).get(CategoryViewModel::class.java)

        cateogryViewModel.getCategoryResponse()
            .observe(this, object :
                androidx.lifecycle.Observer<BaseResponseArrayModel<CategoryModel>> {
                override
                fun onChanged(@Nullable categoryModel: BaseResponseArrayModel<CategoryModel>?) {
                    mWaitingDialog!!.dismiss()
                    if (categoryModel != null) {
                        if (categoryModel.getCode() == 200) {
                            categoryData = categoryModel.getData()
//                            spinnerAdapter(categoryModel.getData())
                        }
                    } else {
                        Utility.showSnackBar(
                            signUpBinding.etSignUpAge,
                            getString(R.string.server_error),
                            this@SignUpActivity
                        )
                    }
                }
            })

        cateogryViewModel.error.observe(this, androidx.lifecycle.Observer {
            mWaitingDialog!!.dismiss()
            Utility.showSnackBar(
                signUpBinding.etSignUpAge, cateogryViewModel.error.value.toString(),
                this@SignUpActivity
            )
        })

    }


    private fun init() {
        Places.initialize(
            this@SignUpActivity,
            resources.getString(R.string.google_place_picker_key)
        )
        signUpBinding.etSignUpName.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                signUpBinding.etSignUpName.setCompoundDrawablesWithIntrinsicBounds(
                    ContextCompat.getDrawable(
                        ctx,
                        R.drawable.profile_blue
                    ),
                    null,
                    null,
                    null
                )
                signUpBinding.etSignUpName.setBackgroundResource(R.drawable.login_textview_blue)
                signUpBinding.etSignUpName.setTextColor(resources.getColor(R.color.colorPrimary))
                signUpBinding.etSignUpName.setHintTextColor(resources.getColor(R.color.colorPrimary))

            } else {
                signUpBinding.etSignUpName.setCompoundDrawablesWithIntrinsicBounds(
                    ContextCompat.getDrawable(
                        ctx,
                        R.drawable.profile_grey
                    ),
                    null,
                    null,
                    null
                )
                signUpBinding.etSignUpName.setBackgroundResource(R.drawable.login_textview_grey)
                signUpBinding.etSignUpName.setTextColor(resources.getColor(R.color.grey))
                signUpBinding.etSignUpName.setHintTextColor(resources.getColor(R.color.grey))
            }

        }
        signUpBinding.etSignUpEmail.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                signUpBinding.etSignUpEmail.setCompoundDrawablesWithIntrinsicBounds(
                    ContextCompat.getDrawable(
                        ctx,
                        R.drawable.mail_blue_icon
                    ),
                    null,
                    null,
                    null
                )
                signUpBinding.etSignUpEmail.setBackgroundResource(R.drawable.login_textview_blue)
                signUpBinding.etSignUpEmail.setTextColor(resources.getColor(R.color.colorPrimary))
                signUpBinding.etSignUpEmail.setHintTextColor(resources.getColor(R.color.colorPrimary))

            } else {
                signUpBinding.etSignUpEmail.setCompoundDrawablesWithIntrinsicBounds(
                    ContextCompat.getDrawable(
                        ctx,
                        R.drawable.mail_grey_icon
                    ),
                    null,
                    null,
                    null
                )
                signUpBinding.etSignUpEmail.setBackgroundResource(R.drawable.login_textview_grey)
                signUpBinding.etSignUpEmail.setTextColor(resources.getColor(R.color.grey))
                signUpBinding.etSignUpEmail.setHintTextColor(resources.getColor(R.color.grey))
            }

        }
        signUpBinding.etSignUpPassword.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                signUpBinding.etSignUpPassword.setCompoundDrawablesWithIntrinsicBounds(
                    ContextCompat.getDrawable(
                        ctx,
                        R.drawable.password_blue_icon
                    ),
                    null,
                    null,
                    null
                )
                signUpBinding.rlSignUpPasswordParent.setBackgroundResource(R.drawable.login_textview_blue)
                signUpBinding.etSignUpPassword.setTextColor(resources.getColor(R.color.colorPrimary))
                signUpBinding.etSignUpPassword.setHintTextColor(
                    resources.getColor(
                        R.color.colorPrimary
                    )
                )

            } else {
                signUpBinding.etSignUpPassword.setCompoundDrawablesWithIntrinsicBounds(
                    ContextCompat.getDrawable(
                        ctx,
                        R.drawable.password_grey_icon
                    ),
                    null,
                    null,
                    null
                )
                signUpBinding.rlSignUpPasswordParent.setBackgroundResource(R.drawable.login_textview_grey)
                signUpBinding.etSignUpPassword.setTextColor(resources.getColor(R.color.grey))
                signUpBinding.etSignUpPassword.setHintTextColor(
                    resources.getColor(
                        R.color.grey
                    )
                )
            }
        }
        signUpBinding.etConfirmPassword.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                signUpBinding.etConfirmPassword.setCompoundDrawablesWithIntrinsicBounds(
                    ContextCompat.getDrawable(
                        ctx,
                        R.drawable.password_blue_icon
                    ),
                    null,
                    null,
                    null
                )
                signUpBinding.rlConfirmPasswordParent.setBackgroundResource(R.drawable.login_textview_blue)
                signUpBinding.etConfirmPassword.setTextColor(resources.getColor(R.color.colorPrimary))
                signUpBinding.etConfirmPassword.setHintTextColor(
                    resources.getColor(
                        R.color.colorPrimary
                    )
                )

            } else {
                signUpBinding.etConfirmPassword.setCompoundDrawablesWithIntrinsicBounds(
                    ContextCompat.getDrawable(
                        ctx,
                        R.drawable.password_grey_icon
                    ),
                    null,
                    null,
                    null
                )
                signUpBinding.rlConfirmPasswordParent.setBackgroundResource(R.drawable.login_textview_grey)
                signUpBinding.etConfirmPassword.setTextColor(resources.getColor(R.color.grey))
                signUpBinding.etConfirmPassword.setHintTextColor(
                    resources.getColor(
                        R.color.grey
                    )
                )
            }
        }
        signUpBinding.etSignUpPhone.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                signUpBinding.etSignUpPhone.setCompoundDrawablesWithIntrinsicBounds(
                    ContextCompat.getDrawable(
                        ctx,
                        R.drawable.phone_blue
                    ),
                    null,
                    null,
                    null
                )
                signUpBinding.etSignUpPhone.setBackgroundResource(R.drawable.login_textview_blue)
                signUpBinding.etSignUpPhone.setTextColor(resources.getColor(R.color.colorPrimary))
                signUpBinding.etSignUpPhone.setHintTextColor(resources.getColor(R.color.colorPrimary))

            } else {
                signUpBinding.etSignUpPhone.setCompoundDrawablesWithIntrinsicBounds(
                    ContextCompat.getDrawable(
                        ctx,
                        R.drawable.phone_grey
                    ),
                    null,
                    null,
                    null
                )
                signUpBinding.etSignUpPhone.setBackgroundResource(R.drawable.login_textview_grey)
                signUpBinding.etSignUpPhone.setTextColor(resources.getColor(R.color.grey))
                signUpBinding.etSignUpPhone.setHintTextColor(resources.getColor(R.color.grey))
            }

        }
        signUpBinding.etSignUpAddress.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                signUpBinding.etSignUpAddress.setCompoundDrawablesWithIntrinsicBounds(
                    ContextCompat.getDrawable(
                        ctx,
                        R.drawable.ic_location_blue
                    ),
                    null,
                    null,
                    null
                )
                signUpBinding.etSignUpAddress.setBackgroundResource(R.drawable.login_textview_blue)
                signUpBinding.etSignUpAddress.setTextColor(resources.getColor(R.color.colorPrimary))
                signUpBinding.etSignUpAddress.setHintTextColor(resources.getColor(R.color.colorPrimary))

            } else {
                signUpBinding.etSignUpAddress.setCompoundDrawablesWithIntrinsicBounds(
                    ContextCompat.getDrawable(
                        ctx,
                        R.drawable.ic_location_grey
                    ),
                    null,
                    null,
                    null
                )
                signUpBinding.etSignUpAddress.setBackgroundResource(R.drawable.login_textview_grey)
                signUpBinding.etSignUpAddress.setTextColor(resources.getColor(R.color.grey))
                signUpBinding.etSignUpAddress.setHintTextColor(resources.getColor(R.color.grey))
            }

        }
        signUpBinding.etSignUpJobType.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                signUpBinding.etSignUpJobType.setCompoundDrawablesWithIntrinsicBounds(
                    ContextCompat.getDrawable(
                        ctx,
                        R.drawable.job_type_blue
                    ),
                    null,
                    null,
                    null
                )
                signUpBinding.etSignUpJobType.setBackgroundResource(R.drawable.login_textview_blue)
                signUpBinding.etSignUpJobType.setTextColor(resources.getColor(R.color.colorPrimary))
                signUpBinding.etSignUpJobType.setHintTextColor(resources.getColor(R.color.colorPrimary))

            } else {
                signUpBinding.etSignUpJobType.setCompoundDrawablesWithIntrinsicBounds(
                    ContextCompat.getDrawable(
                        ctx,
                        R.drawable.ic_location_grey
                    ),
                    null,
                    null,
                    null
                )
                signUpBinding.etSignUpJobType.setBackgroundResource(R.drawable.login_textview_grey)
                signUpBinding.etSignUpJobType.setTextColor(resources.getColor(R.color.grey))
                signUpBinding.etSignUpJobType.setHintTextColor(resources.getColor(R.color.grey))
            }

        }
        signUpBinding.etSignUpAge.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                signUpBinding.etSignUpAge.setCompoundDrawablesWithIntrinsicBounds(
                    ContextCompat.getDrawable(
                        ctx,
                        R.drawable.calendar_blue
                    ),
                    null,
                    null,
                    null
                )
                signUpBinding.etSignUpAge.setBackgroundResource(R.drawable.login_textview_blue)
                signUpBinding.etSignUpAge.setTextColor(resources.getColor(R.color.colorPrimary))
                signUpBinding.etSignUpAge.setHintTextColor(resources.getColor(R.color.colorPrimary))

            } else {
                signUpBinding.etSignUpAge.setCompoundDrawablesWithIntrinsicBounds(
                    ContextCompat.getDrawable(
                        ctx,
                        R.drawable.calendar_grey
                    ),
                    null,
                    null,
                    null
                )
                signUpBinding.etSignUpAge.setBackgroundResource(R.drawable.login_textview_grey)
                signUpBinding.etSignUpAge.setTextColor(resources.getColor(R.color.grey))
                signUpBinding.etSignUpAge.setHintTextColor(resources.getColor(R.color.grey))
            }
        }
        signUpBinding.rgGender.setOnCheckedChangeListener(
            RadioGroup.OnCheckedChangeListener { group, checkedId ->
                when (checkedId) {
                    R.id.rbMale -> {
                        gender = "MALE"
                        signUpBinding.rbMale.setTextColor(resources.getColor(R.color.colorPrimary))
                        signUpBinding.rbFemale.setTextColor(resources.getColor(R.color.grey))
                    }
                    R.id.rbFemale -> {
                        gender = "FEMALE"
                        signUpBinding.rbMale.setTextColor(resources.getColor(R.color.grey))
                        signUpBinding.rbFemale.setTextColor(resources.getColor(R.color.colorPrimary))
                    }
                }
            })

    }

    fun loginInSignUp(v: View) {
        finish()
    }

    fun signUpParent(v: View) {
        Utility.hideKeyboard(this@SignUpActivity, signUpBinding.etSignUpAddress)
    }

    fun signUpPassword(v: View) {
        if (passwordSee) {
            passwordSee = !passwordSee
            signUpBinding.etSignUpPassword.transformationMethod =
                PasswordTransformationMethod.getInstance()
            signUpBinding.ivSignUpPassword.setImageResource(R.drawable.eye_icon)
        } else {
            passwordSee = !passwordSee
            signUpBinding.etSignUpPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
            signUpBinding.etSignUpPassword.setTransformationMethod(null);
            signUpBinding.ivSignUpPassword.setImageResource(R.drawable.ic_open_eye_24dp)
        }
    }

    fun signUpConfirmPassword(v: View) {
        if (confirmpasswordSee) {
            confirmpasswordSee = !confirmpasswordSee
            signUpBinding.etConfirmPassword.transformationMethod =
                PasswordTransformationMethod.getInstance()
            signUpBinding.ivConfirmPassword.setImageResource(R.drawable.eye_icon)
        } else {
            confirmpasswordSee = !confirmpasswordSee
            signUpBinding.etConfirmPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
            signUpBinding.etConfirmPassword.setTransformationMethod(null);
            signUpBinding.ivConfirmPassword.setImageResource(R.drawable.ic_open_eye_24dp)
        }
    }

    fun signUpBack(v: View) {
        finish()
    }

    fun signUpBtn(v: View) {
        val address = signUpBinding.etSignUpAddress.text.toString()
        val username = signUpBinding.etSignUpName.text.toString()
        val email = signUpBinding.etSignUpEmail.text.toString()
        val age = signUpBinding.etSignUpAge.text.toString()
        val phone = signUpBinding.etSignUpPhone.text.toString()
        val password = signUpBinding.etSignUpPassword.text.toString()
        val confirmPassword = signUpBinding.etConfirmPassword.text.toString()

        val result = jobArray.joinToString(", ")
        Log.e("result category", result)
        if (gender.equals("")) {
            Utility.showSnackBar(
                signUpBinding.etSignUpAge,
                "Select gender",
                this@SignUpActivity
            )
        } else if (username.equals("")) {
            Utility.showSnackBar(
                signUpBinding.etSignUpAge,
                "Enter username",
                this@SignUpActivity
            )
        } else if (email.equals("")) {
            Utility.showSnackBar(
                signUpBinding.etSignUpAge,
                "Enter email",
                this@SignUpActivity
            )
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            Utility.showSnackBar(
                signUpBinding.etSignUpAge,
                "Invalid email address",
                this@SignUpActivity
            )
        } else if (phone.equals("")) {
            Utility.showSnackBar(
                signUpBinding.etSignUpAge,
                "Enter phone number",
                this@SignUpActivity
            )
        } else if (age.equals("")) {
            Utility.showSnackBar(
                signUpBinding.etSignUpAge,
                "Enter age",
                this@SignUpActivity
            )
        } else if (password.equals("")) {
            Utility.showSnackBar(
                signUpBinding.etSignUpAge,
                "Enter password",
                this@SignUpActivity
            )
        } else if (password.length < 6) {
            Utility.showSnackBar(
                signUpBinding.etSignUpAge,
                "Password should be of 6 character",
                this@SignUpActivity
            )
        } else if (confirmPassword.equals("")) {
            Utility.showSnackBar(
                signUpBinding.etSignUpAge,
                "Enter confirm password",
                this@SignUpActivity
            )
        } else if (confirmPassword.length < 6) {
            Utility.showSnackBar(
                signUpBinding.etSignUpAge,
                "Enter confirm password",
                this@SignUpActivity
            )
        } else if (!password.equals(confirmPassword)) {
            Utility.showSnackBar(
                signUpBinding.etSignUpAge,
                "Password doesn't match",
                this@SignUpActivity
            )
        } else if (address.equals("")) {
            Utility.showSnackBar(
                signUpBinding.etSignUpAge,
                "Select address",
                this@SignUpActivity
            )
        } else if (resultUri.toString().equals("null")) {
            Utility.showSnackBar(
                signUpBinding.etSignUpAge,
                "Select image",
                this@SignUpActivity
            )
        } else if (jobArray.isNullOrEmpty()) {
            Utility.showSnackBar(
                signUpBinding.etSignUpAge,
                "Select job category",
                this@SignUpActivity
            )
        } else {
            signUpResquestModel = SignUpResquestModel(
                username, email, phone, gender, age, password, confirmPassword, address, "", "", "",
                latitude, longitude, "", "", "", profilePic!!, result
            )
            startActivity(
                Intent(
                    this@SignUpActivity,
                    SignUpSecondActivity::class.java
                ).putExtra("sendData", signUpResquestModel)
            )
        }
    }

    fun etPlacePicker(view: View) {
        callAutoSearch()
    }

    private fun callAutoSearch() {
        val fields =
            listOf(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG)
        val intent = Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY, fields)
            .build(this@SignUpActivity)
        startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE)
    }

    fun openImagePicker(view: View) {
        onSelectImage()
    }

    private fun onSelectImage() {
        CropImage.startPickImageActivity(this)
    }

    private fun startCropActivity(imageUri: Uri) {
        CropImage.activity(imageUri).setGuidelines(CropImageView.Guidelines.ON)
            .setMultiTouchEnabled(true)
            .setAspectRatio(1, 1)
            .start(this)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                val place = Autocomplete.getPlaceFromIntent(data!!)
                latitude = place.latLng!!.latitude
                longitude = place.latLng!!.longitude
                Log.i(
                    "FragmentActivity.TAG",
                    "Place: " + place.name + ", " + place.latLng + ", " + place.address
                )
                val address = place.address

                signUpBinding.etSignUpAddress.setText(address.toString())
                // do query with address
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                val status: Status = Autocomplete.getStatusFromIntent(data!!)
                Toast.makeText(
                    this@SignUpActivity,
                    "Error: " + status.getStatusMessage(),
                    Toast.LENGTH_LONG
                ).show()
                Log.i("FragmentActivity.TAG", status.getStatusMessage())
            } else if (resultCode == Activity.RESULT_CANCELED) {
                // The user canceled the operation.
            }
        } else {
            if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
                val imageUri = CropImage.getPickImageResultUri(this, data)
                if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri)) {
                    // mCropImageUri = imageUri
                    requestPermissions(
                        listOf(Manifest.permission.READ_EXTERNAL_STORAGE).toTypedArray(),
                        0
                    )
                } else {
                    startCropActivity(imageUri)
                }

            }

            if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                val result = CropImage.getActivityResult(data)
                if (resultCode == Activity.RESULT_OK) {
                    imageUrl = result.originalUri
                    resultUri = result.uri
                    println("Image url >>>>>>>>>>>>>$resultUri")
                    try {
                        val bitmap =
                            MediaStore.Images.Media.getBitmap(this.contentResolver, resultUri)
                        signUpBinding.civImage.setImageURI(resultUri)
                        val filePath = saveImage(bitmap)
                        profilePic = File(filePath)
                        /*  reqFile = RequestBody.create(
                              MediaType.parse("multipart/form-data"),
                              profilePic!!
                          )
                          body = MultipartBody.Part.createFormData(
                              "profile_image",
                              profilePic!!.name,
                              reqFile
                          )*/
                        uploadPictureApi(profilePic!!)

                        //name ki jgh parm name h wo aa jaayega or multipart m body send kr dena ho jaayega.
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                }
            }
        }
    }

    private fun uploadPictureApi(profilePic: File) {
        val builder = MultipartBody.Builder()
        builder.setType(MultipartBody.FORM)
        builder.addFormDataPart("profile_image", profilePic.name)

        if (!profilePic.equals("")) {
            val file = profilePic
            val requestFile: RequestBody = RequestBody.create(
                MediaType.parse("multipart/form-data"),
                file
            )
            val multipartBody = MultipartBody.Part.createFormData(
                "profile_image", profilePic!!.name, requestFile
            )
            builder.addPart(multipartBody)
        }
        val requestBody: MultipartBody = builder.build()
//        mWaitingDialog!!.show()
//            viewModel.setProfilePictureData(token, userId, requestBody)
    }

    private fun saveImage(myBitmap: Bitmap): String {
        val bytes = ByteArrayOutputStream()
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes)
        val wallpaperDirectory = getDir("images", Context.MODE_PRIVATE)

        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs()
        }
        try {
            val f = File(
                wallpaperDirectory,
                ((Calendar.getInstance().timeInMillis).toString() + ".jpg")
            )
            f.createNewFile()
            val fo = FileOutputStream(f)
            fo.write(bytes.toByteArray())
            MediaScannerConnection.scanFile(
                this,
                arrayOf(f.path),
                arrayOf("image/jpeg"), null
            )
            fo.close()
            return f.absolutePath
        } catch (e1: IOException) {
            e1.printStackTrace()
        }
        return ""
    }


    fun jobType(view: View) {
        jobTypeArray.clear()
        jobArray.clear()
        val dialogBuilder = Dialog(this)
        val inflater = LayoutInflater.from(this)
        val binding: DialogJobTypeBinding = DataBindingUtil.inflate(
            inflater,
            R.layout.dialog_job_type,
            null,
            false
        )
        dialogBuilder.setContentView(binding.root)
        val mWindow = dialogBuilder.window
        mWindow!!.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.MATCH_PARENT
        )
        mWindow.setDimAmount(0.2f)
        mWindow.setGravity(Gravity.BOTTOM)
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(mWindow.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        mWindow.attributes = lp
        dialogBuilder.setCancelable(false)
        dialogBuilder.setCanceledOnTouchOutside(true)
        dialogBuilder.show()
        binding.rvLayoutDialog
        val layoutManager = LinearLayoutManager(this@SignUpActivity)
        binding.rvLayoutDialog.layoutManager = layoutManager
        binding.rvLayoutDialog.setHasFixedSize(true)
        ViewCompat.setNestedScrollingEnabled(binding.rvLayoutDialog, false)
        val jobAdapter = JobTypeAdapter(this@SignUpActivity, categoryData) { id, value, checked ->

            if (checked.equals("check")) {
                jobArray.add(id.toString())
                jobTypeArray.add(value)
            } else {
                jobArray.remove(id.toString())
                jobTypeArray.remove(value)
            }
        }
        binding.rvLayoutDialog.adapter = jobAdapter
        binding.btnCancel.setOnClickListener { v ->
            dialogBuilder.dismiss()
        }
        binding.btnAccept.setOnClickListener { v ->
            System.out.println(jobTypeArray);
            val result = jobTypeArray.joinToString(", ")
            signUpBinding.etSignUpJobType.setText(result)
            dialogBuilder.dismiss()

        }

    }
}