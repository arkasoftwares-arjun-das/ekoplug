package com.ekoplug.service.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.annotation.Nullable
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.ekoplug.model.BookingRequestModel
import com.ekoplug.service.R
import com.ekoplug.service.adapters.BookingRequestAdapter
import com.ekoplug.service.basesetup.BaseResponseArrayModel
import com.ekoplug.service.databinding.ActivityCurrentBinding
import com.ekoplug.service.viewmodel.BookingRequestViewModel
import com.ekoplug.utils.AppPreference
import com.ekoplug.utils.Utility
import com.ekoplug.utils.Watting

class CurrentActivity : AppCompatActivity() {

    lateinit var currentBinding : ActivityCurrentBinding
    private var mWaitingDialog: Watting? = null
    lateinit var bookingRequestViewModel: BookingRequestViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        currentBinding = DataBindingUtil.setContentView(this,R.layout.activity_current)
        mWaitingDialog = Watting(this, "")
        val token = AppPreference.getPreferenceValueByKey(this, "Token").toString()
        callApi()
        mWaitingDialog!!.show()
        bookingRequestViewModel.setCurrentBookingModel(token)
    }

    private fun callApi() {
        bookingRequestViewModel =
            ViewModelProviders.of(this).get(BookingRequestViewModel::class.java)

        bookingRequestViewModel.getCurrentBookingData()
            .observe(
                this,
                object : Observer<BaseResponseArrayModel<BookingRequestModel>> {
                    override
                    fun onChanged(@Nullable currentRequestModel: BaseResponseArrayModel<BookingRequestModel>?) {
                        mWaitingDialog!!.dismiss()
                        if (currentRequestModel != null) {
                            if (currentRequestModel.getCode() == 200) {

                                if (currentRequestModel.getData()!!.isEmpty()) {
                                    currentBinding.tvDataNotFoundCategory.visibility =
                                        View.VISIBLE
                                    currentBinding.rvCurrentBooking.visibility = View.GONE
                                } else {
                                    currentBinding.rvCurrentBooking.visibility =
                                        View.VISIBLE
                                    currentBinding.tvDataNotFoundCategory.visibility =
                                        View.GONE
                                    setAdapter(currentRequestModel.getData()!!)
                                }
                            } else {
                                Utility.showSnackBar(
                                    currentBinding.tvDataNotFoundCategory,
                                    currentRequestModel.getMessage(),
                                    this@CurrentActivity
                                )
                            }
                        } else {
                            Utility.showSnackBar(
                                currentBinding.tvDataNotFoundCategory,
                                getString(R.string.server_error),
                                this@CurrentActivity
                            )
                        }
                    }
                })

        bookingRequestViewModel.error.observe(this, Observer {
            mWaitingDialog!!.dismiss()
            Utility.showSnackBar(
                currentBinding.tvDataNotFoundCategory,
                bookingRequestViewModel.error.value.toString(),
                this
            )
        })
    }


    private fun setAdapter(data: ArrayList<BookingRequestModel>) {
        currentBinding.rvCurrentBooking.layoutManager =
            LinearLayoutManager(this)
        val bookingRequestAdapter =
            BookingRequestAdapter(
                this,
                data,
                "current"
            )
        currentBinding.rvCurrentBooking.adapter = bookingRequestAdapter

    }

    fun backBtn(view: View) {
        finish()
    }

}
