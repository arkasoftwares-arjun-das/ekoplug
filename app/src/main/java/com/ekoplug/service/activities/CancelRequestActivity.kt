package com.ekoplug.service.activities

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.text.TextUtils
import android.view.View
import androidx.annotation.Nullable
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.ekoplug.service.R
import com.ekoplug.service.basesetup.BaseResponseModel
import com.ekoplug.service.databinding.ActivityCancelRequestBinding
import com.ekoplug.service.model.BookingDetailsModel
import com.ekoplug.service.viewmodel.CancelRequestViewModel
import com.ekoplug.utils.AppPreference
import com.ekoplug.utils.Utility
import com.ekoplug.utils.Watting

class CancelRequestActivity : AppCompatActivity() {

    lateinit var activityCancelRequestActivity : ActivityCancelRequestBinding
    var selectedReasonText = ""
    private var mWaitingDialog: Watting? = null
    lateinit var viewModel: CancelRequestViewModel
    var id =0
    var token = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityCancelRequestActivity = DataBindingUtil.setContentView(this,R.layout.activity_cancel_request)
        mWaitingDialog = Watting(this, "")
        id = intent!!.getIntExtra("id",0)
        token = AppPreference.getPreferenceValueByKey(this, "Token").toString()
        callApi()
        init()
    }

    private fun callApi() {
        viewModel =
            ViewModelProviders.of(this).get(CancelRequestViewModel::class.java)

        viewModel.getCancelRequest()
            .observe(
                this,
                object : Observer<BaseResponseModel<BookingDetailsModel>> {
                    override
                    fun onChanged(@Nullable bookingRequestAcceptModel: BaseResponseModel<BookingDetailsModel>?) {
                        mWaitingDialog!!.dismiss()
                        if (bookingRequestAcceptModel != null) {
                            if (bookingRequestAcceptModel.getCode() == 200) {
                                Utility.showSnackBar(
                                    activityCancelRequestActivity.tvCancelBooking,
                                    bookingRequestAcceptModel.getMessage(),
                                    this@CancelRequestActivity
                                )
                                Handler().postDelayed({
                                    startActivity(Intent(this@CancelRequestActivity,HomeActivity::class.java))
                                    finishAffinity()
                                }, 1000)
                            } else {
                                Utility.showSnackBar(
                                    activityCancelRequestActivity.tvCancelBooking,
                                    bookingRequestAcceptModel.getMessage(),
                                    this@CancelRequestActivity
                                )
                            }
                        } else {
                            Utility.showSnackBar(
                                activityCancelRequestActivity.tvCancelBooking,
                                getString(R.string.server_error),
                                this@CancelRequestActivity
                            )
                        }
                    }
                })

        viewModel.error.observe(this, Observer {
            mWaitingDialog!!.dismiss()
            Utility.showSnackBar(
                activityCancelRequestActivity.tvCancelBooking,
                viewModel.error.value.toString(),
                this
            )
        })
    }

    private fun init() {
        activityCancelRequestActivity.etCancelAdditonalComment.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                activityCancelRequestActivity.etCancelAdditonalComment.setBackgroundResource(R.drawable.login_textview_blue)
                activityCancelRequestActivity.etCancelAdditonalComment.setTextColor(resources.getColor(R.color.colorPrimary))
                activityCancelRequestActivity.etCancelAdditonalComment.setHintTextColor(resources.getColor(R.color.colorPrimary))

            } else {
                activityCancelRequestActivity.etCancelAdditonalComment.setBackgroundResource(R.drawable.login_textview_grey)
                activityCancelRequestActivity.etCancelAdditonalComment.setTextColor(resources.getColor(R.color.grey))
                activityCancelRequestActivity.etCancelAdditonalComment.setHintTextColor(resources.getColor(R.color.grey))
            }
        }
        activityCancelRequestActivity.rgRasonCancel.setOnCheckedChangeListener { group, checkedId ->
            if(checkedId == R.id.rb_reasonOne){
                selectedReasonText = activityCancelRequestActivity.rbReasonOne.text.toString()
                activityCancelRequestActivity.rbReasonOne.setTextColor(resources.getColor(R.color.colorPrimary))
                activityCancelRequestActivity.rbReasonTwo.setTextColor(resources.getColor(R.color.grey))
                activityCancelRequestActivity.rbReasonThree.setTextColor(resources.getColor(R.color.grey))
                activityCancelRequestActivity.rbReasonFour.setTextColor(resources.getColor(R.color.grey))
                activityCancelRequestActivity.rbReasonOther.setTextColor(resources.getColor(R.color.grey))
            }
            else if(checkedId == R.id.rb_reasonTwo){
                selectedReasonText = activityCancelRequestActivity.rbReasonTwo.text.toString()
                activityCancelRequestActivity.rbReasonTwo.setTextColor(resources.getColor(R.color.colorPrimary))
                activityCancelRequestActivity.rbReasonOne.setTextColor(resources.getColor(R.color.grey))
                activityCancelRequestActivity.rbReasonThree.setTextColor(resources.getColor(R.color.grey))
                activityCancelRequestActivity.rbReasonFour.setTextColor(resources.getColor(R.color.grey))
                activityCancelRequestActivity.rbReasonOther.setTextColor(resources.getColor(R.color.grey))
            }
            else if(checkedId == R.id.rb_reasonThree){
                selectedReasonText = activityCancelRequestActivity.rbReasonThree.text.toString()
                activityCancelRequestActivity.rbReasonThree.setTextColor(resources.getColor(R.color.colorPrimary))
                activityCancelRequestActivity.rbReasonTwo.setTextColor(resources.getColor(R.color.grey))
                activityCancelRequestActivity.rbReasonOne.setTextColor(resources.getColor(R.color.grey))
                activityCancelRequestActivity.rbReasonFour.setTextColor(resources.getColor(R.color.grey))
                activityCancelRequestActivity.rbReasonOther.setTextColor(resources.getColor(R.color.grey))
            }
            else if(checkedId == R.id.rb_reasonFour){
                selectedReasonText = activityCancelRequestActivity.rbReasonFour.text.toString()
                activityCancelRequestActivity.rbReasonFour.setTextColor(resources.getColor(R.color.colorPrimary))
                activityCancelRequestActivity.rbReasonTwo.setTextColor(resources.getColor(R.color.grey))
                activityCancelRequestActivity.rbReasonThree.setTextColor(resources.getColor(R.color.grey))
                activityCancelRequestActivity.rbReasonOne.setTextColor(resources.getColor(R.color.grey))
                activityCancelRequestActivity.rbReasonOther.setTextColor(resources.getColor(R.color.grey))
            }
            else if(checkedId == R.id.rb_reasonOther){
                selectedReasonText = activityCancelRequestActivity.rbReasonOther.text.toString()
                activityCancelRequestActivity.rbReasonOther.setTextColor(resources.getColor(R.color.colorPrimary))
                activityCancelRequestActivity.rbReasonTwo.setTextColor(resources.getColor(R.color.grey))
                activityCancelRequestActivity.rbReasonThree.setTextColor(resources.getColor(R.color.grey))
                activityCancelRequestActivity.rbReasonFour.setTextColor(resources.getColor(R.color.grey))
                activityCancelRequestActivity.rbReasonOne.setTextColor(resources.getColor(R.color.grey))
            }
        }
    }

    fun submitBookingBtn(view: View) {
        if(selectedReasonText.equals("")){
            Utility.showSnackBar(
                activityCancelRequestActivity.tvCancelBooking,
                "Select reason",
                this
            )
        }else if(TextUtils.isEmpty(activityCancelRequestActivity.etCancelAdditonalComment.text.toString())){
            Utility.showSnackBar(
                activityCancelRequestActivity.tvCancelBooking,
                "Enter comment",
                this
            )
        }else{
            mWaitingDialog!!.show()
            viewModel.setCancelBooking(selectedReasonText,activityCancelRequestActivity.etCancelAdditonalComment.text.toString(),token,id)
        }
    }

    fun cancelBackBtn(view: View) {
        finish()
    }
}
