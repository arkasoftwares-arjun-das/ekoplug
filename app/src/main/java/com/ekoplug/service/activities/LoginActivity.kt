package com.ekoplug.service.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.InputType
import android.text.method.PasswordTransformationMethod
import android.view.View
import android.widget.Toast
import androidx.annotation.Nullable
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.ekoplug.service.R
import com.ekoplug.service.basesetup.BaseResponseModel
import com.ekoplug.service.databinding.ActivityLoginBinding
import com.ekoplug.service.model.LoginResponseModel
import com.ekoplug.service.viewmodel.LoginViewModel
import com.ekoplug.utils.AppPreference
import com.ekoplug.utils.Utility
import com.ekoplug.utils.Watting
import kotlinx.android.synthetic.main.activity_login.*


class LoginActivity : AppCompatActivity() {

    lateinit var loginActivityBinding: ActivityLoginBinding
    lateinit var ctx: Context
    var passwordSee: Boolean = false
    lateinit var loginViewModel: LoginViewModel
    private var mWaitingDialog: Watting? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loginActivityBinding = DataBindingUtil.setContentView(
            this,
            R.layout.activity_login
        )
        ctx = this
        mWaitingDialog = Watting(this, "")

        val connected = Utility.isNetworkConnected(this@LoginActivity)
        if (!connected) {
            Toast.makeText(this, "Please check your internet connection !", Toast.LENGTH_LONG)
                .show()
        }
        init()
        callApi()
    }
    private fun callApi() {
        loginViewModel = ViewModelProviders.of(this).get(LoginViewModel::class.java)
        loginActivityBinding.loginViewModel = loginViewModel

        loginActivityBinding.loginViewModel!!.getLoginResponseModel()
            .observe(this, object : Observer<BaseResponseModel<LoginResponseModel>> {
                override
                fun onChanged(@Nullable loginModel: BaseResponseModel<LoginResponseModel>?) {
                    mWaitingDialog!!.dismiss()
                    if (loginModel != null) {

                        if (loginModel.getCode() == 200) {
                            Utility.showSnackBar(
                                et_email,
                                loginModel.getMessage(),
                                this@LoginActivity
                            )
                            loginActivityBinding.etPassword.setText("")
                            loginActivityBinding.etEmail.setText("")
                            AppPreference.saveIntPreference(
                                this@LoginActivity, "Id",
                                loginModel.getData()!!.id
                            )
                            AppPreference.savePreference(
                                this@LoginActivity, "Email",
                                loginModel.getData()!!.email
                            )
                            AppPreference.savePreference(
                                this@LoginActivity, "Token",
                                loginModel.getData()!!.auth_token
                            )
                            AppPreference.savePreference(
                                this@LoginActivity, "Name",
                                loginModel.getData()!!.username
                            )
                            startActivity(Intent(this@LoginActivity,HomeActivity::class.java))
                            finish()

                        } else if (loginModel.getCode() == 401) {
                            if (!loginModel.getData()!!.is_active) {
                                startActivity(
                                    Intent(
                                        this@LoginActivity,
                                        OtpVerifyActivity::class.java
                                    ).putExtra(
                                        "Email",
                                        loginActivityBinding.etEmail.text.toString()
                                    ).putExtra("activity", "Login")
                                )
                            }
                        } else {
                            Utility.showSnackBar(
                                et_email,
                                loginModel.getErrorMessage(),
                                this@LoginActivity
                            )
                        }
                    } else {
                        Utility.showSnackBar(
                            et_email,
                            getString(R.string.server_error),
                            this@LoginActivity
                        )

                    }
                }
            })
        loginActivityBinding.loginViewModel!!.error.observe(this, Observer {
            mWaitingDialog!!.dismiss()
            Utility.showSnackBar(
                et_email,
                loginViewModel.error.value.toString(),
                this@LoginActivity
            )
        })
    }

    private fun init() {
        loginActivityBinding.etEmail.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                loginActivityBinding.etEmail.setCompoundDrawablesWithIntrinsicBounds(
                    ContextCompat.getDrawable(
                        ctx,
                        R.drawable.mail_blue_icon
                    ),
                    null,
                    null,
                    null
                )
                loginActivityBinding.etEmail.setBackgroundResource(R.drawable.login_textview_blue)
                loginActivityBinding.etEmail.setTextColor(resources.getColor(R.color.colorPrimary))
                loginActivityBinding.etEmail.setHintTextColor(resources.getColor(R.color.colorPrimary))

            } else {
                loginActivityBinding.etEmail.setCompoundDrawablesWithIntrinsicBounds(
                    ContextCompat.getDrawable(
                        ctx,
                        R.drawable.mail_grey_icon
                    ),
                    null,
                    null,
                    null
                )
                loginActivityBinding.etEmail.setBackgroundResource(R.drawable.login_textview_grey)
                loginActivityBinding.etEmail.setTextColor(resources.getColor(R.color.grey))
                loginActivityBinding.etEmail.setHintTextColor(resources.getColor(R.color.grey))
            }
        }
        loginActivityBinding.etPassword.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                loginActivityBinding.etPassword.setCompoundDrawablesWithIntrinsicBounds(
                    ContextCompat.getDrawable(
                        ctx,
                        R.drawable.password_blue_icon
                    ),
                    null,
                    null,
                    null
                )

                loginActivityBinding.etPasswordParent.setBackgroundResource(R.drawable.login_textview_blue)
                loginActivityBinding.etPassword.setTextColor(resources.getColor(R.color.colorPrimary))
                loginActivityBinding.etPassword.setHintTextColor(
                    resources.getColor(
                        R.color.colorPrimary
                    )
                )

            } else {
                loginActivityBinding.etPassword.setCompoundDrawablesWithIntrinsicBounds(
                    ContextCompat.getDrawable(
                        ctx,
                        R.drawable.password_grey_icon
                    ),
                    null,
                    null,
                    null
                )
                loginActivityBinding.etPasswordParent.setBackgroundResource(R.drawable.login_textview_grey)
                loginActivityBinding.etPassword.setTextColor(resources.getColor(R.color.grey))
                loginActivityBinding.etPassword.setHintTextColor(
                    resources.getColor(
                        R.color.grey
                    )
                )
            }
        }
    }

    fun passwordVisible(v: View) {
        if (passwordSee) {
            passwordSee = !passwordSee
            loginActivityBinding.etPassword.transformationMethod =
                PasswordTransformationMethod.getInstance()
            loginActivityBinding.ivPassword.setImageResource(R.drawable.eye_icon)
        } else {
            passwordSee = !passwordSee
            loginActivityBinding.etPassword.inputType =
                InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD
            loginActivityBinding.etPassword.transformationMethod = null
            loginActivityBinding.ivPassword.setImageResource(R.drawable.ic_open_eye_24dp)
        }
    }

    fun loginBtn(v: View) {
        mWaitingDialog!!.show()
        loginActivityBinding.loginViewModel!!.setLoginData()
    }

    fun signUp(v: View) {
        startActivity(Intent(this, SignUpActivity::class.java))
    }

    fun forgotPassword(v: View) {
        startActivity(Intent(this, ForgotPasswordActivity::class.java))
    }
    fun loginParent(v: View) {
        Utility.hideKeyboard(this, loginActivityBinding.etEmail)
    }
}
