package com.ekoplug.service.activities

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.text.InputType
import android.text.method.PasswordTransformationMethod
import android.view.View
import androidx.annotation.Nullable
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.ekoplug.service.R
import com.ekoplug.service.basesetup.BaseResponseModel
import com.ekoplug.service.databinding.ActivityChangePasswordBinding
import com.ekoplug.service.model.LoginResponseModel
import com.ekoplug.service.viewmodel.ChangePasswordViewModel
import com.ekoplug.utils.AppPreference
import com.ekoplug.utils.Utility
import com.ekoplug.utils.Watting
import kotlinx.android.synthetic.main.activity_change_password.*
import java.util.*

class ChangePasswordActivity : AppCompatActivity() {

    lateinit var changePasswordBinding : ActivityChangePasswordBinding
    lateinit var ctx : Context
    var passwordSee: Boolean = false
    var confirmpasswordSee: Boolean = false
    private var mWaitingDialog: Watting? = null
    var token : String = ""
    lateinit var changePasswordViewModel : ChangePasswordViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        changePasswordBinding = DataBindingUtil.setContentView(this,
            R.layout.activity_change_password
        )
        ctx = this
        mWaitingDialog = Watting(this,"")
        token = intent.getStringExtra("Token").toString()
        init()
        callApi()
    }

    private fun callApi() {
        changePasswordViewModel = ViewModelProviders.of(this).get(ChangePasswordViewModel::class.java)
        changePasswordBinding.changePasswordViewModel = changePasswordViewModel

        changePasswordBinding.changePasswordViewModel!!.getChangePasswordResponseModel()
            .observe(this, object : Observer<BaseResponseModel<LoginResponseModel>> {
                override
                fun onChanged(@Nullable changeModel: BaseResponseModel<LoginResponseModel>?) {
                    mWaitingDialog!!.dismiss()
                    if (changeModel != null) {
                        if (changeModel.getCode() == 200) {
                            Utility.showSnackBar(
                                et_cpPassword,
                                changeModel.getMessage(),
                                this@ChangePasswordActivity
                            )
                            Handler().postDelayed({
                                changePasswordBinding.etCpPassword.setText("")
                                changePasswordBinding.etCpconfirmPassword.setText("")
                                finish()
                            }, 1000)

                        } else {
                            Utility.showSnackBar(
                                et_cpPassword,
                                changeModel.getErrorMessage(),
                                this@ChangePasswordActivity
                            )
                        }

                    } else {
                        Utility.showSnackBar(
                            et_cpPassword,
                            getString(R.string.server_error),
                            this@ChangePasswordActivity
                        )

                    }
                }
            })


        changePasswordBinding.changePasswordViewModel!!.error.observe(this, Observer {
            mWaitingDialog!!.dismiss()
            Utility.showSnackBar(
                et_cpPassword,
                changePasswordViewModel!!.error.value.toString(),
                this@ChangePasswordActivity
            )
        })
    }

    private fun init() {
        changePasswordBinding.etCpPassword.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                changePasswordBinding.etCpPassword.setCompoundDrawablesWithIntrinsicBounds(
                    ContextCompat.getDrawable(ctx,
                        R.drawable.password_blue_icon
                    ),
                    null,
                    null,
                    null
                )
                changePasswordBinding.rlChangePasswordParent.setBackgroundResource(
                    R.drawable.login_textview_blue
                )
                changePasswordBinding.etCpPassword.setTextColor(resources.getColor(
                    R.color.colorPrimary
                ))
                changePasswordBinding.etCpPassword.setHintTextColor(resources.getColor(
                    R.color.colorPrimary
                ))

            } else {
                changePasswordBinding.etCpPassword.setCompoundDrawablesWithIntrinsicBounds(
                    ContextCompat.getDrawable(ctx,
                        R.drawable.password_grey_icon
                    ),
                    null,
                    null,
                    null
                )
                changePasswordBinding.rlChangePasswordParent.setBackgroundResource(
                    R.drawable.login_textview_grey
                )
                changePasswordBinding.etCpPassword.setTextColor(resources.getColor(
                    R.color.grey
                ))
                changePasswordBinding.etCpPassword.setHintTextColor(resources.getColor(
                    R.color.grey
                ))
            }

        }
        changePasswordBinding.etCpconfirmPassword.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                changePasswordBinding.etCpconfirmPassword.setCompoundDrawablesWithIntrinsicBounds(
                    ContextCompat.getDrawable(ctx,
                        R.drawable.password_blue_icon
                    ),
                    null,
                    null,
                    null
                )
                changePasswordBinding.rlChangePasswordConfirmParent.setBackgroundResource(
                    R.drawable.login_textview_blue
                )
                changePasswordBinding.etCpconfirmPassword.setTextColor(resources.getColor(
                    R.color.colorPrimary
                ))
                changePasswordBinding.etCpconfirmPassword.setHintTextColor(resources.getColor(
                    R.color.colorPrimary
                ))

            } else {
                changePasswordBinding.etCpconfirmPassword.setCompoundDrawablesWithIntrinsicBounds(
                    ContextCompat.getDrawable(ctx,
                        R.drawable.password_grey_icon
                    ),
                    null,
                    null,
                    null
                )
                changePasswordBinding.rlChangePasswordConfirmParent.setBackgroundResource(
                    R.drawable.login_textview_grey
                )
                changePasswordBinding.etCpconfirmPassword.setTextColor(resources.getColor(
                    R.color.grey
                ))
                changePasswordBinding.etCpconfirmPassword.setHintTextColor(resources.getColor(
                    R.color.grey
                ))
            }

        }
    }

    fun cpConfirmPassword(v: View){
        if (confirmpasswordSee) {
            confirmpasswordSee = !confirmpasswordSee
            changePasswordBinding.etCpconfirmPassword.transformationMethod =
                PasswordTransformationMethod.getInstance()
            changePasswordBinding.ivCpconfirmPassword.setImageResource(R.drawable.eye_icon)
        } else {
            confirmpasswordSee = !confirmpasswordSee
            changePasswordBinding.etCpconfirmPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
            changePasswordBinding.etCpconfirmPassword.setTransformationMethod(null);
            changePasswordBinding.ivCpconfirmPassword.setImageResource(R.drawable.ic_open_eye_24dp)
        }
    }

    fun cpPassword(v:View){
        if (passwordSee) {
            passwordSee = !passwordSee
            changePasswordBinding.etCpPassword.transformationMethod =
                PasswordTransformationMethod.getInstance()
            changePasswordBinding.ivCpPassword.setImageResource(R.drawable.eye_icon)
        } else {
            passwordSee = !passwordSee
            changePasswordBinding.etCpPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
            changePasswordBinding.etCpPassword.setTransformationMethod(null);
            changePasswordBinding.ivCpPassword.setImageResource(R.drawable.ic_open_eye_24dp)
        }

    }

    fun changePasswordBack(v:View){
        finish()
    }

    fun changePasswordSubmit(v:View){
        mWaitingDialog!!.show()
        changePasswordBinding.changePasswordViewModel!!.setLoginData(token)
    }
}
