package com.ekoplug.service.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.ekoplug.service.R
import com.ekoplug.service.adapters.EditProfileAdapter
import com.ekoplug.service.adapters.MyBookingTabAdapter
import com.ekoplug.service.databinding.ActivityEditProfileBinding

class EditProfileActivity : AppCompatActivity() {

    lateinit var editProfileBinding : ActivityEditProfileBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        editProfileBinding = DataBindingUtil.setContentView(this,R.layout.activity_edit_profile)

        val editProfileAdapter = EditProfileAdapter(supportFragmentManager)
        editProfileBinding.viewPagerEditProfile.setAdapter(editProfileAdapter)
        editProfileBinding.tabLayout.setupWithViewPager(editProfileBinding.viewPagerEditProfile)
    }

    fun backBtnEdit(view: View) {
        finish()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        for (fragment: Fragment? in supportFragmentManager.fragments) {
            fragment!!.onActivityResult(requestCode, resultCode, data)
        }
    }
}
