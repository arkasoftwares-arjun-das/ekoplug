package com.ekoplug.service.activities

import android.os.Bundle
import android.view.View
import androidx.annotation.Nullable
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.ekoplug.model.StaticPageModel
import com.ekoplug.service.R
import com.ekoplug.service.basesetup.BaseResponseModel
import com.ekoplug.service.databinding.ActivityTermsConditionBinding
import com.ekoplug.service.viewmodel.ContactUsViewModel
import com.ekoplug.utils.AppPreference
import com.ekoplug.utils.Utility
import com.ekoplug.utils.Watting

class TermsConditionActivity : AppCompatActivity() {

    lateinit var activityTermsConditionBinding: ActivityTermsConditionBinding
    private var mWaitingDialog: Watting? = null
    private lateinit var viewModel: ContactUsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityTermsConditionBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_terms_condition)
        mWaitingDialog = Watting(this, "")
        val token = AppPreference.getPreferenceValueByKey(this, "Token").toString()
        callApi()
        mWaitingDialog!!.show()
        viewModel.setStaticPageData(token, "terms and conditions")
    }

    private fun callApi() {
        viewModel = ViewModelProviders.of(this).get(ContactUsViewModel::class.java)

        viewModel.getStaticPageData()
            .observe(this, object : Observer<BaseResponseModel<StaticPageModel>> {
                override
                fun onChanged(@Nullable staticModel: BaseResponseModel<StaticPageModel>?) {
                    mWaitingDialog!!.dismiss()
                    if (staticModel != null) {

                        if (staticModel.getCode() == 200) {

                            activityTermsConditionBinding.tvTermsShortDescription.text =
                                staticModel.getData()!!.short_description
                            activityTermsConditionBinding.tvTermsDescription.text =
                                staticModel.getData()!!.content

                        } else {
                            Utility.showSnackBar(
                                activityTermsConditionBinding.ivTermsConditionBack,
                                staticModel.getMessage(),
                                this@TermsConditionActivity
                            )
                        }

                    } else {
                        Utility.showSnackBar(
                            activityTermsConditionBinding.ivTermsConditionBack,
                            getString(R.string.server_error),
                            this@TermsConditionActivity
                        )
                    }
                }
            })

        viewModel.error.observe(this, Observer {
            mWaitingDialog!!.dismiss()
            Utility.showSnackBar(
                activityTermsConditionBinding.ivTermsConditionBack,
                viewModel.error.value.toString(),
                this@TermsConditionActivity
            )
        })
    }

    fun termsBackBtn(view: View) {
        finish()
    }
}
