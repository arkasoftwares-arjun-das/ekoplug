package com.ekoplug.service.activities

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Typeface
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.provider.Settings
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.ekoplug.service.R
import com.ekoplug.utils.AppPreference
import com.ekoplug.utils.Utility
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_splash.*

class SplashActivity : AppCompatActivity() {

    private val PermissionsRequestCode = 123
    private var mDelayHandler: Handler? = null
    private val mDelay: Long = 3000
    private var allPermission: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        init();
    }

    private fun init() {
        mDelayHandler = Handler()
        mDelayHandler!!.postDelayed(mRunnable, mDelay)

    }

    private val mRunnable: Runnable = Runnable {
        if (!isFinishing) {
            allPermission = checkPermission()
            if (!checkPermission()) {
                makeRequestPermissions()
            } else {
                checkIsLogin()
            }

        }
    }

    private fun checkIsLogin() {
        if (Utility.isNetworkConnected(this@SplashActivity)) {
            if (!AppPreference.getPreferenceValueByKey(this@SplashActivity, "Email").equals("")) {
                var notificationData = intent.extras
                if (notificationData != null) {
                    for (key in intent.extras!!.keySet()) {
                        val value = intent.extras!!.getString(key)
                    }
                    val userId = notificationData!!.get("userID")
                    val type = notificationData.get("notificationType").toString()
                    if (type!!.equals("STOP SERVICE", true)) {
                        startActivity(
                            Intent(
                                this@SplashActivity,
                                SubmitReviewActivity::class.java
                            ).putExtra("userId", userId.toString())
                        )
                        finish()
                    } else {
                        startActivity(Intent(this@SplashActivity, HomeActivity::class.java))
                        finish()
                    }
                } else {
                    startActivity(Intent(this@SplashActivity, HomeActivity::class.java))
                    finish()
                }
            } else {
                startActivity(Intent(this@SplashActivity, LoginActivity::class.java))
                finish()
            }
        } else {
            Toast.makeText(this, "Please check your internet connection !", Toast.LENGTH_LONG)
                .show()
        }

    }

    private fun checkPermission(): Boolean {
        val firstPermission: Int =
            ContextCompat.checkSelfPermission(applicationContext, Manifest.permission.CAMERA)
        val secondPermission: Int =
            ContextCompat.checkSelfPermission(
                applicationContext,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
        val thirdPermission: Int =
            ContextCompat.checkSelfPermission(
                applicationContext,
                Manifest.permission.ACCESS_COARSE_LOCATION
            )
        val fourPermission: Int =
            ContextCompat.checkSelfPermission(
                applicationContext,
                Manifest.permission.READ_EXTERNAL_STORAGE
            )
        val fivePermission: Int = ContextCompat.checkSelfPermission(
            applicationContext,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        )
        return firstPermission == PackageManager.PERMISSION_GRANTED &&
                secondPermission == PackageManager.PERMISSION_GRANTED &&
                thirdPermission == PackageManager.PERMISSION_GRANTED &&
                fourPermission == PackageManager.PERMISSION_GRANTED &&
                fivePermission == PackageManager.PERMISSION_GRANTED
    }

    private fun makeRequestPermissions() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(
                Manifest.permission.CAMERA,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ),
            PermissionsRequestCode
        )

    }

    fun showSnackBarOnError(view: View, message: String, context: Context) {
        val snackBar = Snackbar.make(
            view, message, Snackbar.LENGTH_INDEFINITE
        )
        snackBar.changeFont()
        snackBar.setBackgroundTint(ContextCompat.getColor(context, R.color.grey))
        snackBar.setTextColor(ContextCompat.getColor(context, R.color.white))
        snackBar.setActionTextColor(ContextCompat.getColor(this, R.color.white))
        snackBar.setAction("Ok") {
            val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
            val uri = Uri.fromParts("package", packageName, null)
            intent.data = uri
            startActivity(intent)
            snackBar.dismiss()
        }
        snackBar.show()
    }

    private fun Snackbar.changeFont() {
        val tv = view.findViewById(com.google.android.material.R.id.snackbar_text) as TextView
        val font = Typeface.createFromAsset(context.assets, "font/muli_light.ttf")
        tv.typeface = font
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PermissionsRequestCode) {
            var permissionAccepted = false
            if (grantResults.isNotEmpty()) {
                for (i in grantResults.indices) {
                    permissionAccepted = grantResults[i] == PackageManager.PERMISSION_GRANTED
                }
                if (permissionAccepted) {
                    checkIsLogin()
                } else {
                    showSnackBarOnError(
                        splashLayout,
                        resources.getString(R.string.permission),
                        this
                    )
                }
            }
        }
    }

    override fun onRestart() {
        super.onRestart()
        init()
    }

    override fun onStop() {
        super.onStop()
        finish()
    }
}
