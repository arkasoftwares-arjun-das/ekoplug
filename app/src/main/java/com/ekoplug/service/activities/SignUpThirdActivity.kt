package com.ekoplug.service.activities

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.media.MediaScannerConnection
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.provider.MediaStore
import android.view.View
import androidx.annotation.Nullable
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.ekoplug.service.R
import com.ekoplug.service.basesetup.BaseResponseModel
import com.ekoplug.service.databinding.ActivitySignUpThirdBinding
import com.ekoplug.service.model.SignUpResquestModel
import com.ekoplug.service.model.SignupResponseModel
import com.ekoplug.service.viewmodel.SignUpViewModel
import com.ekoplug.utils.Utility
import com.ekoplug.utils.Watting
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.*

class SignUpThirdActivity : AppCompatActivity() {

    lateinit var activityThirdSignUpBinding: ActivitySignUpThirdBinding
    private var mWaitingDialog: Watting? = null
    private var imageUrl: Uri? = null
    private var addressPic: File? = null
    private var idPic: File? = null
    private var certificatePic: File? = null
    var resultUri: Uri? = null
    var signUpResquestModel: SignUpResquestModel? = null
    var btnValue: Int = 0
    var imageOne = ""
    var imageTwo = ""
    var imageThree = ""
    lateinit var signUpViewModel: SignUpViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityThirdSignUpBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_sign_up_third)
        mWaitingDialog = Watting(this, "")
        signUpResquestModel = intent!!.getSerializableExtra("sendData") as SignUpResquestModel?
        callApi()

    }

    private fun callApi() {
        signUpViewModel = ViewModelProviders.of(this).get(SignUpViewModel::class.java)

        signUpViewModel.getSignUpResponseModel()
            .observe(this, object :
                androidx.lifecycle.Observer<BaseResponseModel<SignupResponseModel>> {
                override
                fun onChanged(@Nullable signUpModel: BaseResponseModel<SignupResponseModel>?) {
                    mWaitingDialog!!.dismiss()
                    if (signUpModel != null) {
                        if (signUpModel.getCode() == 200) {
                            Utility.showSnackBar(
                                activityThirdSignUpBinding.tvAddressFile,
                                signUpModel.getMessage(),
                                this@SignUpThirdActivity
                            )

                            Handler().postDelayed({
                                startActivity(Intent(this@SignUpThirdActivity,OtpVerifyActivity::class.java)
                                    .putExtra("Email",signUpModel.getData()!!.email).putExtra("activity","signup"))
                                finishAffinity()
                            }, 1000)
                        } else {
                            Utility.showSnackBar(
                                activityThirdSignUpBinding.tvAddressFile,
                                signUpModel.getMessage(),
                                this@SignUpThirdActivity
                            )
                        }
                    } else {
                        Utility.showSnackBar(
                            activityThirdSignUpBinding.tvAddressFile,
                            getString(R.string.server_error),
                            this@SignUpThirdActivity
                        )
                    }
                }
            })

        signUpViewModel.error.observe(this, androidx.lifecycle.Observer {
            mWaitingDialog!!.dismiss()
            Utility.showSnackBar(
                activityThirdSignUpBinding.tvAddressFile, signUpViewModel.error.value.toString(),
                this@SignUpThirdActivity
            )
        })

    }

    fun loginInSignUp(view: View) {
        startActivity(Intent(this, LoginActivity::class.java))
        finishAffinity()
    }

    fun signUpThirdBtn(view: View) {
        if (imageOne.equals("")) {
            Utility.showSnackBar(
                activityThirdSignUpBinding.tvAddressFile,
                "Select address proof",
                this@SignUpThirdActivity
            )
        } else if (imageTwo.equals("")) {
            Utility.showSnackBar(
                activityThirdSignUpBinding.tvAddressFile,
                "Select id proof",
                this@SignUpThirdActivity
            )
        } else if (imageThree.equals("")) {
            Utility.showSnackBar(
                activityThirdSignUpBinding.tvAddressFile,
                "Select certificate",
                this@SignUpThirdActivity
            )
        } else {
            uploadPictureApi()
        }
    }

    fun btnAddressProof(view: View) {
        btnValue = 1
        onSelectImage()
    }

    fun btnIdProof(view: View) {
        btnValue = 2
        onSelectImage()
    }

    fun btnCertificateProof(view: View) {
        btnValue = 3
        onSelectImage()
    }

    fun signUpSecondBack(view: View) {
        finish()
    }

    private fun onSelectImage() {
        CropImage.startPickImageActivity(this)
    }

    private fun startCropActivity(imageUri: Uri) {
        CropImage.activity(imageUri).setGuidelines(CropImageView.Guidelines.ON)
            .setMultiTouchEnabled(true)
            .setAspectRatio(1, 1)
            .start(this)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            val imageUri = CropImage.getPickImageResultUri(this, data)
            if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri)) {
                // mCropImageUri = imageUri
                requestPermissions(
                    listOf(Manifest.permission.READ_EXTERNAL_STORAGE).toTypedArray(),
                    0
                )
            } else {
                startCropActivity(imageUri)
            }
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            val result = CropImage.getActivityResult(data)
            if (resultCode == Activity.RESULT_OK) {
                imageUrl = result.originalUri
                resultUri = result.uri
                println("Image url >>>>>>>>>>>>>$resultUri")
                try {
                    val bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, resultUri)
                    if (btnValue == 1) {
                        activityThirdSignUpBinding.tvAddressFile.setImageURI(resultUri)
                        val filePath = saveImage(bitmap)
                        addressPic = File(filePath)
                        imageOne = "Upload"
                    } else if (btnValue == 2) {
                        activityThirdSignUpBinding.tvIdFile.setImageURI(resultUri)
                        val filePath = saveImage(bitmap)
                        idPic = File(filePath)
                        imageTwo = "Upload"
                    } else if (btnValue == 3) {
                        activityThirdSignUpBinding.tvCertificateFile.setImageURI(resultUri)
                        val filePath = saveImage(bitmap)
                        certificatePic = File(filePath)
                        imageThree = "Upload"
                    }
//                    signUpBinding.civImage.setImageURI(resultUri)

                    /*  reqFile = RequestBody.create(
                          MediaType.parse("multipart/form-data"),
                          profilePic!!
                      )
                      body = MultipartBody.Part.createFormData(
                          "profile_image",
                          profilePic!!.name,
                          reqFile
                      )*/
//                    uploadPictureApi(profilePic!!)
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }
        }

    }

    private fun uploadPictureApi() {
        val builder = MultipartBody.Builder()
        builder.setType(MultipartBody.FORM)
        builder.addFormDataPart("username", signUpResquestModel!!.username)
        builder.addFormDataPart("email", signUpResquestModel!!.email)
        builder.addFormDataPart("phone_number", signUpResquestModel!!.phone_number)
        builder.addFormDataPart("gender", signUpResquestModel!!.gender)
        builder.addFormDataPart("age", signUpResquestModel!!.age)
        builder.addFormDataPart("password", signUpResquestModel!!.password)
        builder.addFormDataPart("confirm_password", signUpResquestModel!!.confirm_password)
        builder.addFormDataPart("jobcategories", signUpResquestModel!!.jobcategories)
        builder.addFormDataPart("address[address]", signUpResquestModel!!.address)
        builder.addFormDataPart("address[zip_code]", signUpResquestModel!!.zip_code)
        builder.addFormDataPart("address[city]", signUpResquestModel!!.city)
        builder.addFormDataPart("address[state]", signUpResquestModel!!.state)
        builder.addFormDataPart("address[country]", "")
        builder.addFormDataPart("address[latitude]", signUpResquestModel!!.latitude.toString())
        builder.addFormDataPart("address[longitude]", signUpResquestModel!!.longitude.toString())
        builder.addFormDataPart("bank_detail[holder_name]", signUpResquestModel!!.holder_name)
        builder.addFormDataPart("bank_detail[bank_name]", signUpResquestModel!!.bank_name)
        builder.addFormDataPart("bank_detail[account_number]", signUpResquestModel!!.account_number)
       /* builder.addFormDataPart("documents[address_proof]", addressPic!!.name)
        builder.addFormDataPart("documents[criminal_record]", "")
        builder.addFormDataPart("documents[immunization_proof]", idPic!!.name)
        builder.addFormDataPart("documents[certificates]", certificatePic!!.name)
        builder.addFormDataPart("profile_image", signUpResquestModel!!.profile_image!!.name)*/

        if (!addressPic!!.equals("") &&
            !idPic!!.equals("") &&
            !certificatePic!!.equals("") &&
            !signUpResquestModel!!.profile_image!!.equals("")
        ) {
            val addressfile = addressPic
            val idfile = idPic
            val certificatefile = certificatePic
            val profilePic = signUpResquestModel!!.profile_image
            val requestFile: RequestBody = RequestBody.create(
                MediaType.parse("multipart/form-data"),
                addressfile!!
            )
            val multipartBody = MultipartBody.Part.createFormData(
                "documents[address_proof]", addressPic!!.name, requestFile
            )
            val requestFile2: RequestBody = RequestBody.create(
                MediaType.parse("multipart/form-data"),
                idfile!!
            )
            val multipartBody2 = MultipartBody.Part.createFormData(
                "documents[immunization_proof]", idPic!!.name, requestFile2
            )
            val requestFile3: RequestBody = RequestBody.create(
                MediaType.parse("multipart/form-data"),
                certificatefile!!
            )
            val multipartBody3 = MultipartBody.Part.createFormData(
                "documents[certificates]", certificatePic!!.name, requestFile3
            )
            val requestFile4: RequestBody = RequestBody.create(
                MediaType.parse("multipart/form-data"),
                profilePic!!
            )
            val multipartBody4 = MultipartBody.Part.createFormData(
                "profile_image", profilePic.name, requestFile4
            )
            builder.addPart(multipartBody)
            builder.addPart(multipartBody2)
            builder.addPart(multipartBody3)
            builder.addPart(multipartBody4)
        }
        val requestBody: MultipartBody = builder.build()
        mWaitingDialog!!.show()
        signUpViewModel.setSignUpData(requestBody)
    }

    private fun saveImage(myBitmap: Bitmap): String {
        val bytes = ByteArrayOutputStream()
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes)
        val wallpaperDirectory = getDir("images", Context.MODE_PRIVATE)

        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs()
        }
        try {
            val f = File(
                wallpaperDirectory,
                ((Calendar.getInstance().timeInMillis).toString() + ".jpg")
            )
            f.createNewFile()
            val fo = FileOutputStream(f)
            fo.write(bytes.toByteArray())
            MediaScannerConnection.scanFile(
                this,
                arrayOf(f.path),
                arrayOf("image/jpeg"), null
            )
            fo.close()
            return f.absolutePath
        } catch (e1: IOException) {
            e1.printStackTrace()
        }
        return ""
    }
}
