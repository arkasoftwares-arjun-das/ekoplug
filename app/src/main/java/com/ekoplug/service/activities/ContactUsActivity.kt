package com.ekoplug.service.activities

import android.os.Bundle
import android.view.View
import androidx.annotation.Nullable
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.ekoplug.model.StaticPageModel
import com.ekoplug.service.R
import com.ekoplug.service.basesetup.BaseResponseModel
import com.ekoplug.service.databinding.ActivityContactUsBinding
import com.ekoplug.service.viewmodel.ContactUsViewModel
import com.ekoplug.utils.AppPreference
import com.ekoplug.utils.Utility
import com.ekoplug.utils.Watting

class ContactUsActivity : AppCompatActivity() {

    lateinit var activityContactUsBinding: ActivityContactUsBinding
    private var mWaitingDialog: Watting? = null
    private lateinit var viewModel: ContactUsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityContactUsBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_contact_us)
        mWaitingDialog = Watting(this, "")
        val token = AppPreference.getPreferenceValueByKey(this, "Token").toString()
        callApi()
        mWaitingDialog!!.show()
        viewModel.setStaticPageData(token,"Contact Us")
    }

    private fun callApi() {
        viewModel = ViewModelProviders.of(this).get(ContactUsViewModel::class.java)

        viewModel.getStaticPageData()
            .observe(this, object : Observer<BaseResponseModel<StaticPageModel>> {
                override
                fun onChanged(@Nullable staticModel: BaseResponseModel<StaticPageModel>?) {
                    mWaitingDialog!!.dismiss()
                    if (staticModel != null) {

                        if (staticModel.getCode() == 200) {
                            activityContactUsBinding.tvContactUsContent.text = staticModel.getData()!!.content

                        } else {
                            Utility.showSnackBar(
                                activityContactUsBinding.tvContactUs,
                                staticModel.getMessage(),
                                this@ContactUsActivity
                            )
                        }

                    } else {
                        Utility.showSnackBar(
                            activityContactUsBinding.tvContactUs,
                            getString(R.string.server_error),
                            this@ContactUsActivity
                        )
                    }
                }
            })

        viewModel.error.observe(this, Observer {
            mWaitingDialog!!.dismiss()
            Utility.showSnackBar(
                activityContactUsBinding.tvContactUs, viewModel.error.value.toString(),
                this@ContactUsActivity
            )
        })
    }


    fun backContactBtn(view: View) {
        finish()
    }
}
