package com.ekoplug.service.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.ekoplug.fragments.CompleteFragment
import com.ekoplug.service.fragments.BankProfileFragment
import com.ekoplug.service.fragments.OtherProfileFragment
import com.ekoplug.service.fragments.PersonalProfileFragment
import com.ekoplug.service.fragments.UpcomingFragment

class EditProfileAdapter(fm: FragmentManager?) :
    FragmentStatePagerAdapter(fm!!) {
    var frag = arrayOf("Personal","Bank","Other")
    override fun getPageTitle(position: Int): CharSequence? {
        return frag[position]
    }

    override fun getItem(position: Int): Fragment {
        when (position) {
            0 -> return PersonalProfileFragment()
            1 -> return BankProfileFragment()
            2 -> return OtherProfileFragment()
        }
        return OtherProfileFragment()
    }

    override fun getCount(): Int {
        return frag.size
    }
}
