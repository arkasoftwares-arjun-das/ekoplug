package com.ekoplug.service.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.CompoundButton
import androidx.recyclerview.widget.RecyclerView
import com.ekoplug.service.R
import com.ekoplug.service.model.CategoryModel
import kotlinx.android.synthetic.main.layout_job_type.view.*


class JobTypeAdapter(
    private var mContext: Context,
    private var mDataset: ArrayList<CategoryModel>?,
    val itemClick: (Int, String,String) -> Unit

) : RecyclerView.Adapter<JobTypeAdapter.ViewHolder>() {
    init {
        this.mContext = mContext
        this.mDataset = mDataset
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): JobTypeAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.layout_job_type, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return mDataset!!.size
    }

    override fun onBindViewHolder(holder: JobTypeAdapter.ViewHolder, position: Int) {
        holder.mCheckBox.setText(mDataset!!.get(position).category_name)

        holder.mCheckBox.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { buttonView, isChecked ->
            if(isChecked){
                itemClick(mDataset!!.get(position).id,mDataset!!.get(position).category_name,"check")
            }else{
                itemClick(mDataset!!.get(position).id, mDataset!!.get(position).category_name,"Uncheck")

            }
        }
        )
    }

    inner class ViewHolder(v: View) : RecyclerView.ViewHolder(v) {

        var mCheckBox: CheckBox

        init {
            mCheckBox = v.checkBox
        }

    }


}