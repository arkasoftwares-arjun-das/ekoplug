package com.ekoplug.service.adapters

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.ekoplug.model.MyBookingModel
import com.ekoplug.service.R
import com.ekoplug.service.activities.BookingDetailsActivity
import com.ekoplug.service.databinding.LayoutBookingRequestBinding
import com.ekoplug.service.model.MyBookingListModel
import com.ekoplug.utils.Utility
import com.squareup.picasso.Picasso
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class UpcomingAdapters(
    private var mContext: Context,
    var data: ArrayList<MyBookingListModel>

) :
    RecyclerView.Adapter<UpcomingAdapters.ViewHolder>() {
    init {
        this.mContext = mContext
        this.data = data
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: LayoutBookingRequestBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.layout_booking_request, parent, false
        )
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var date1 = data.get(position).appointment_time
        var spf1 = SimpleDateFormat("HH:mm:ss")
        val newDate1: Date = spf1.parse(date1)
        spf1 = SimpleDateFormat("hh:mm aa")
        date1 = spf1.format(newDate1)

        holder.itmeBinding!!.tvProviderName.text = data.get(position).user
        Picasso.get()
            .load(Utility.imageURL + "" + data.get(position).user_profile)
            .into(holder.itmeBinding!!.civProviderImage)
        holder.itmeBinding!!.tvRating.text = data.get(position).rating.toString()
        holder.itmeBinding!!.tvCategory.text = data.get(position).job_category
        holder.itmeBinding!!.tvDateTime.text = data.get(position).appointment_date+" | " +date1
       holder.itmeBinding!!.clBookingParentLayout.setOnClickListener { v ->
           mContext.startActivity(Intent(mContext,BookingDetailsActivity::class.java).putExtra("bookingData",position))
       }
    }

    inner class ViewHolder(itmeBinding: LayoutBookingRequestBinding) :
        RecyclerView.ViewHolder(itmeBinding.root) {
        var itmeBinding: LayoutBookingRequestBinding? = null

        init {
            this.itmeBinding = itmeBinding
        }
    }
}