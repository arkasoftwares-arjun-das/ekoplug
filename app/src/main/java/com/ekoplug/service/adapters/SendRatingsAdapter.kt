package com.ekoplug.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.ekoplug.service.R
import com.ekoplug.service.databinding.LayoutServiceReviewListBinding
import com.ekoplug.service.model.SubmitReviewModel
import com.ekoplug.utils.Utility
import com.squareup.picasso.Picasso

class SendRatingsAdapter(
    private var mContext: Context,
    var data: ArrayList<SubmitReviewModel>?
//    var data: NotificationModel?

) :
    RecyclerView.Adapter<SendRatingsAdapter.ViewHolder>() {
    init {
        this.mContext = mContext
        this.data = data
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var binding: LayoutServiceReviewListBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.layout_service_review_list, parent, false
        )
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return data!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itmeBinding!!.tvProviderReviewName.text = data!!.get(position).username
        Picasso.get()
            .load(Utility.imageURL + "" + data!!.get(position).profile_image)
            .into(holder.itmeBinding!!.civProviderReviewImage)
        holder.itmeBinding!!.tvRatingsReviews.text = data!!.get(position).rating.toString()
        holder.itmeBinding!!.tvProviderReview.text = data!!.get(position).comment
//        holder.itmeBinding!!.tvTime.text = data.get(position).appointment_time
        /*  if(data.get(position).booking_status.equals("ACCEPTED")){
              holder.itmeBinding!!.idBookingLyout.setOnClickListener { view ->
                  val fragment = MyBookingDetailsFragment()
                  val args = Bundle()
                  args.putInt("detailsId",9)
                  fragment.arguments = args
                  (view.context as FragmentActivity).supportFragmentManager.beginTransaction()
                      .replace(R.id.frameLayout, fragment).addToBackStack(null)
                      .commit()
              }
          }*/
    }

    inner class ViewHolder(itmeBinding: LayoutServiceReviewListBinding) :
        RecyclerView.ViewHolder(itmeBinding.root) {
        var itmeBinding: LayoutServiceReviewListBinding? = null

        init {
            this.itmeBinding = itmeBinding
        }
    }
}