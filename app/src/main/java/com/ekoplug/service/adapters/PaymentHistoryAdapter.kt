package com.ekoplug.service.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.ekoplug.model.PaymentHistoryModel
import com.ekoplug.service.R
import com.ekoplug.service.databinding.LayoutPaymentHistoryBinding
import java.text.DecimalFormat


class PaymentHistoryAdapter(
    private var mContext: Context,
    var data: ArrayList<PaymentHistoryModel>?

) :
    RecyclerView.Adapter<PaymentHistoryAdapter.ViewHolder>() {

    init {
        this.mContext = mContext
        this.data = data
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: LayoutPaymentHistoryBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.layout_payment_history, parent, false
        )
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return data!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val price = data!!.get(position).amount
        holder.itmeBinding!!.tvAmountPayment.setText("₦" + DecimalFormat("##.##").format(price))
        holder.itmeBinding!!.tvDatePayment.text = data!!.get(position).created_at
        holder.itmeBinding!!.tvPaymentName.text = data!!.get(position).transaction_id
        holder.itmeBinding!!.tvBookingCategoryPayment.text = data!!.get(position).status
    }

    inner class ViewHolder(itmeBinding: LayoutPaymentHistoryBinding) :
        RecyclerView.ViewHolder(itmeBinding.root) {
        var itmeBinding: LayoutPaymentHistoryBinding? = null

        init {
            this.itmeBinding = itmeBinding
        }
    }
}