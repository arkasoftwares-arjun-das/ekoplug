package com.ekoplug.service.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.ekoplug.fragments.CompleteFragment
import com.ekoplug.service.fragments.UpcomingFragment

class MyBookingTabAdapter(fm: FragmentManager?) :
    FragmentStatePagerAdapter(fm!!) {
    var frag = arrayOf("Upcoming", "Complete")
    override fun getPageTitle(position: Int): CharSequence? {
        return frag[position]
    }

    override fun getItem(position: Int): Fragment {
        when (position) {
            0 -> return UpcomingFragment()
            1 -> return CompleteFragment()
        }
        return UpcomingFragment()
    }

    override fun getCount(): Int {
        return frag.size
    }
}
