package com.ekoplug.service.adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.ekoplug.model.BookingRequestModel
import com.ekoplug.service.R
import com.ekoplug.service.activities.BookingDetailsActivity
import com.ekoplug.service.activities.BookingReqeustDetailsActivity
import com.ekoplug.service.databinding.LayoutBookingRequestBinding
import com.ekoplug.utils.Utility
import com.squareup.picasso.Picasso
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class BookingRequestAdapter(
    private var mContext: Context,
    private var bookingRequest: ArrayList<BookingRequestModel>?,
    val from: String

) : RecyclerView.Adapter<BookingRequestAdapter.ViewHolder>() {

    init {
        this.mContext = mContext
        this.bookingRequest = bookingRequest
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: LayoutBookingRequestBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.layout_booking_request, parent, false
        )
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return bookingRequest!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        var date1 =  bookingRequest!!.get(position).appointment_time
        var spf1 = SimpleDateFormat("HH:mm:ss")
        val newDate1: Date = spf1.parse(date1)
        spf1 = SimpleDateFormat("hh:mm aa")
        date1 = spf1.format(newDate1)

        var date = bookingRequest!!.get(position).appointment_date
        var spf = SimpleDateFormat("dd-MM-yyyy")
        val newDate: Date = spf.parse(date)
        spf = SimpleDateFormat("dd MMM yyyy")
        date = spf.format(newDate)
        holder.itmeBinding!!.tvProviderName.text = bookingRequest!!.get(position).user
        holder.itmeBinding!!.tvDateTime.text =
             date+ " | " +date1
        holder.itmeBinding!!.tvRating.text = bookingRequest!!.get(position).rating.toString()
        holder.itmeBinding!!.tvCategory.text =
            bookingRequest!!.get(position).job_category.toString()

        Picasso.get()
            .load(Utility.imageURL + "" + bookingRequest!!.get(position).user_profile)
            .into(holder.itmeBinding!!.civProviderImage)

        holder.itmeBinding!!.clBookingParentLayout.setOnClickListener { v ->
            if (from.equals("bookingRequest")) {
                mContext.startActivity(
                    Intent(
                        mContext,
                        BookingReqeustDetailsActivity::class.java
                    ).putExtra("id", bookingRequest!!.get(position).id)
                )
            }else{
                mContext.startActivity(Intent(mContext, BookingDetailsActivity::class.java).putExtra("bookingData",position))

            }
        }
    }

    inner class ViewHolder(itmeBinding: LayoutBookingRequestBinding) :
        RecyclerView.ViewHolder(itmeBinding.root) {
        var itmeBinding: LayoutBookingRequestBinding? = null

        init {
            this.itmeBinding = itmeBinding
        }
    }
}