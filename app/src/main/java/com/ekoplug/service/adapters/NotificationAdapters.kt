package com.ekoplug.service.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView

import com.ekoplug.model.NotificationModel
import com.ekoplug.service.R
import com.ekoplug.service.databinding.LayoutNotificationBinding
import com.ekoplug.utils.Utility
import com.squareup.picasso.Picasso


class NotificationAdapters(
    private var mContext: Context,
    var data: ArrayList<NotificationModel>?,
    val itemCLick : (Int) -> Unit

) :
    RecyclerView.Adapter<NotificationAdapters.ViewHolder>() {
    init {
        this.mContext = mContext
        this.data = data
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var binding: LayoutNotificationBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.layout_notification, parent, false
        )
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return data!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itmeBinding!!.tvNotifictionUserName.text = data!!.get(position).sender_name
        Picasso.get()
            .load(Utility.imageURL + "" + data!!.get(position).profile_image)
            .into(holder.itmeBinding!!.civNotificationImage)
        holder.itmeBinding!!.tvNotifictionUserTemplate.text = data!!.get(position).notification_title
        holder.itmeBinding!!.tvDate.text = data!!.get(position).created_at
        holder.itmeBinding!!.ivCloseNotificationBtn.setOnClickListener { view ->
            itemCLick(data!!.get(position).id)
        }
    }

    inner class ViewHolder(itmeBinding: LayoutNotificationBinding) :
        RecyclerView.ViewHolder(itmeBinding.root) {
        var itmeBinding: LayoutNotificationBinding? = null

        init {
            this.itmeBinding = itmeBinding
        }
    }
}