package com.ekoplug.service.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.ekoplug.model.ChatFirebaseModel
import com.ekoplug.service.R
import com.ekoplug.service.databinding.LayoutRecievedChatBinding
import com.ekoplug.service.databinding.LayoutSendChatBinding
import kotlinx.android.synthetic.main.layout_recieved_chat.view.*
import kotlinx.android.synthetic.main.layout_send_chat.view.*
import java.text.SimpleDateFormat


class ChatScreenAdapter(
    var data: List<ChatFirebaseModel>,
    var ctx: Context,
    val userID: Int
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var VIEW_TYPE_MESSAGE_SENT: Int = 1;
    var VIEW_TYPE_MESSAGE_RECEIVED : Int = 2;

    init {
        this.ctx = ctx
        this.data = data
    }

    override fun getItemCount(): Int {
        return data!!.size
    }

    // Determines the appropriate ViewType according to the sender of the message.
    override fun getItemViewType(position: Int): Int {
        val message = data!!.get(position)
        return if (message.senderId == userID) {
            VIEW_TYPE_MESSAGE_SENT
        } else { // If some other user sent the message
            VIEW_TYPE_MESSAGE_RECEIVED
        }
    }

    // Inflates the appropriate layout according to the ViewType.
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        var viewSend: LayoutSendChatBinding? = null
        if (viewType == VIEW_TYPE_MESSAGE_SENT) {
            viewSend = DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.layout_send_chat,
                parent,
                false
            )
            return SentMessageHolder(viewSend!!)
        } else if (viewType == VIEW_TYPE_MESSAGE_RECEIVED) {
            val view: LayoutRecievedChatBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.context)
                , R.layout.layout_recieved_chat, parent, false
            )
            return ReceivedMessageHolder(view)
        }
        return SentMessageHolder(viewSend!!)
    }

    // Passes the message object to a ViewHolder so that the contents can be bound to UI.
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
//        val message: mMessageList.get(position) as UserMessage
        when (holder.itemViewType) {
            VIEW_TYPE_MESSAGE_SENT -> (holder as SentMessageHolder).bind(position)
            VIEW_TYPE_MESSAGE_RECEIVED -> (holder as ReceivedMessageHolder).bind(position)
        }
    }

    inner class SentMessageHolder(itemView: LayoutSendChatBinding) :
        RecyclerView.ViewHolder(itemView.root) {

        fun bind(position: Int) {
            itemView.tv_sendBody.setText(data.get(position).msg)

            val spf1 = SimpleDateFormat(" dd MMM yyyy hh:mm aa")
            val datetime = spf1.format(data.get(position).timestamp!!.toDate())

            itemView.text_message_time.setText(datetime)
        }
    }

    inner class ReceivedMessageHolder(itemView: LayoutRecievedChatBinding) :
        RecyclerView.ViewHolder(itemView.root) {

        fun bind(position: Int) {
            itemView.tv_recievedBody.setText(data.get(position).msg)

            val spf1 = SimpleDateFormat(" dd MMM yyyy hh:mm aa")
            val datetime = spf1.format(data.get(position).timestamp!!.toDate())

            itemView.text_message_recieved_time.setText(datetime)
        }
    }
}