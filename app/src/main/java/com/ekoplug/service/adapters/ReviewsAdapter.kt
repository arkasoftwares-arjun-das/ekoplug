package com.ekoplug.service.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.ekoplug.model.WorkerReviewModel
import com.ekoplug.service.R
import com.ekoplug.service.databinding.LayoutReviewListBinding
import com.ekoplug.utils.Utility
import com.squareup.picasso.Picasso

class ReviewsAdapter(
    private var mContext: Context,
    private var serviceProviderReviewModel: ArrayList<WorkerReviewModel>?
) :
    RecyclerView.Adapter<ReviewsAdapter.ViewHolder>() {
    init {
        this.mContext = mContext
        this.serviceProviderReviewModel = serviceProviderReviewModel
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var binding: LayoutReviewListBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.layout_review_list, parent, false
        )
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return serviceProviderReviewModel!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itmeBinding!!.tvProviderReviewName.text = serviceProviderReviewModel!!.get(position).username
        holder.itmeBinding!!.tvRatingsReviews.text =
            serviceProviderReviewModel!!.get(position).rating.toString()
        holder.itmeBinding!!.tvProviderReview.text =
            serviceProviderReviewModel!!.get(position).comment
        Picasso.get()
            .load(Utility.imageURL + "" + serviceProviderReviewModel!!.get(position).profile_image)
            .into(holder.itmeBinding!!.civProviderReviewImage)

    }

    inner class ViewHolder(itmeBinding: LayoutReviewListBinding) :
        RecyclerView.ViewHolder(itmeBinding.root) {
        var itmeBinding: LayoutReviewListBinding? = null

        init {
            this.itmeBinding = itmeBinding
        }
    }
}