package com.ekoplug.service.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.ekoplug.fragments.CompleteFragment
import com.ekoplug.fragments.RecievedRatingsFragment
import com.ekoplug.fragments.SendRatigsFragment

class ReviewRatingsTabAdapter(fm: FragmentManager?) :
    FragmentStatePagerAdapter(fm!!) {
    var frag = arrayOf("Send", "Recieved")
    override fun getPageTitle(position: Int): CharSequence? {
        return frag[position]
    }

    override fun getItem(position: Int): Fragment {
        when (position) {
            0 -> return SendRatigsFragment()
            1 -> return RecievedRatingsFragment()
        }
        return SendRatigsFragment()
    }

    override fun getCount(): Int {
        return frag.size
    }
}
