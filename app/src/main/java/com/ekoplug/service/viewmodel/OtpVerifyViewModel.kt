package com.ekoplug.service.viewmodel

import android.app.Application
import android.text.TextUtils
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.ekoplug.service.basesetup.BaseResponseModel
import com.ekoplug.service.basesetup.WebServiceRepository
import com.ekoplug.service.model.ForgotPasswardResponseModel
import com.ekoplug.service.model.OtpVerifyResponseModel


class OtpVerifyViewModel(application: Application) : AndroidViewModel(application) {

    var webServiceRepository: WebServiceRepository

    private var otpResponseModel: MutableLiveData<BaseResponseModel<OtpVerifyResponseModel>>? = null
    private var otpResendModel : MutableLiveData<BaseResponseModel<ForgotPasswardResponseModel>>? = null

    var codeA = MutableLiveData<String>()
    var codeB = MutableLiveData<String>()
    var codeC = MutableLiveData<String>()
    var codeD = MutableLiveData<String>()
    var error = MutableLiveData<String>()

    init {
        webServiceRepository = WebServiceRepository(application)
        otpResponseModel = webServiceRepository.getOtpVerifyResponseModel()
        otpResendModel = webServiceRepository.getResentData()
    }

    /*@getOtpVerifyResponseModel this Method return data after api call */

    fun getOtpVerifyResponseModel(): MutableLiveData<BaseResponseModel<OtpVerifyResponseModel>> {
        return otpResponseModel!!;
    }


    fun getOtpResendRessponse(): MutableLiveData<BaseResponseModel<ForgotPasswardResponseModel>> {
        return otpResendModel!!;
    }
    /*@setSignUpData this Method api call on webRepositry*/

    fun setOtpData(mail : String) {
        val otp :String = codeA.value+""+codeB.value+""+codeC.value+""+codeD.value
        if (isValidUserData(otp)) {
            webServiceRepository.setOtpVerifyData(mail,otp)
        }
    }

    fun setOtpResend(mail:String){
            webServiceRepository.setResentData(mail)
    }

    private fun isValidUserData(otp:String): Boolean {
        if (TextUtils.isEmpty(otp.trim()) || otp.length < 4 ) {
            error.value = "Enter Otp"
            return false
        }
        return true
    }
}

