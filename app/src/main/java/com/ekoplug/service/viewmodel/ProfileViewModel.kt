package com.ekoplug.service.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.ekoplug.model.AddressProfile
import com.ekoplug.model.ProfileModel
import com.ekoplug.model.UpdateProfileMdel
import com.ekoplug.service.basesetup.BaseResponseModel
import com.ekoplug.service.basesetup.WebServiceRepository
import okhttp3.MultipartBody

class ProfileViewModel(application: Application) : AndroidViewModel(application) {
    var webServiceRepository: WebServiceRepository
    private var profileModel: MutableLiveData<BaseResponseModel<ProfileModel<AddressProfile>>>
    private var updateProfilePicture: MutableLiveData<BaseResponseModel<UpdateProfileMdel>>
    var error = MutableLiveData<String>()

    init {
        webServiceRepository = WebServiceRepository(application)

        /*@getloginResPonseModel set ref. in WebRepositry class*/
        profileModel = webServiceRepository.getProfileReviewModel()
        updateProfilePicture = webServiceRepository.getProfilePictureUpdate()

    }
    fun getProfileModel(): MutableLiveData<BaseResponseModel<ProfileModel<AddressProfile>>> {
        return profileModel;
    }

    fun setProfileData(token: String, id: Int) {
        webServiceRepository.setProfileData(token,id);
    }

    fun getProfilePictureModel(): MutableLiveData<BaseResponseModel<UpdateProfileMdel>> {
        return updateProfilePicture;
    }

    fun setProfilePictureData(
        token: String,
        id: Int,
        body: MultipartBody
    ) {
        webServiceRepository.setUpdateProfilePicture(token,id,body);
    }

}
