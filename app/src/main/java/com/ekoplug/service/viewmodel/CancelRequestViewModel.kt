package com.ekoplug.service.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.ekoplug.service.basesetup.BaseResponseModel
import com.ekoplug.service.basesetup.WebServiceRepository
import com.ekoplug.service.model.BookingDetailsModel

class CancelRequestViewModel(application: Application) : AndroidViewModel(application) {
    var webServiceRepository: WebServiceRepository
    private var cancelBookingModel: MutableLiveData<BaseResponseModel<BookingDetailsModel>>

    var error = MutableLiveData<String>()

    init {
        webServiceRepository = WebServiceRepository(application)

        /*@getloginResPonseModel set ref. in WebRepositry class*/
        cancelBookingModel = webServiceRepository.getCancelBookingRequest()

    }

    /*@getLoginResponseModel this Method return data after api call */
    fun getCancelRequest(): MutableLiveData<BaseResponseModel<BookingDetailsModel>> {
        return cancelBookingModel
    }

    /*@setLoginData this Method api call on webRepositry*/
    fun setCancelBooking(
        selectedReasonText: String,
        toString: String,
        token: String,
        id: Int
    ) {
        webServiceRepository.setCancelBookingRequest("","",token,id)
    }

}
