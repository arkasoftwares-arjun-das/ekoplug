package com.ekoplug.service.viewmodel

import android.app.Application
import android.text.TextUtils
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.ekoplug.model.AddressProfile
import com.ekoplug.model.ProfileModel
import com.ekoplug.service.basesetup.BaseResponseModel
import com.ekoplug.service.basesetup.WebServiceRepository

class ChangePasswodViewModel(application: Application) : AndroidViewModel(application) {
    var webServiceRepository: WebServiceRepository
    private var changePasswordModel: MutableLiveData<BaseResponseModel<ProfileModel<AddressProfile>>>? =
        null
    var currentPassword = MutableLiveData<String>()
    var confirmPassword = MutableLiveData<String>()
    var password = MutableLiveData<String>()
    var error = MutableLiveData<String>()

    init {
        webServiceRepository = WebServiceRepository(application)
        changePasswordModel = webServiceRepository.getCurrentChangePassword()
    }

    /*@getChangePasswordResponseModel this Method return data after api call */
    fun getCurrentChangePasswordResponseModel(): MutableLiveData<BaseResponseModel<ProfileModel<AddressProfile>>> {
        return changePasswordModel!!;
    }

    /*@setLoginData this Method api call on webRepositry*/
    fun setChangePasswordData(token: String) {
        if (isValidUserData(
                currentPassword.value.toString(),
                confirmPassword.value.toString(),
                password.value.toString()
            )
        ) {
            webServiceRepository.setChangeCurrentPassword(
                currentPassword.value.toString(),
                confirmPassword.value.toString(),
                password.value.toString(),
                token
            );
        }
    }

    private fun isValidUserData(
        currentPassowrd: String,
        confirmpassword: String,
        password: String
    ): Boolean {
        if (TextUtils.isEmpty(currentPassowrd.trim())) {
            error.value = "Enter current password"
            return false
        } else if (TextUtils.isEmpty(confirmpassword.trim())) {
            error.value = "Enter new password"
            return false
        } else if (confirmpassword.length < 6) {
            error.value = "Password should be of 6 character"
            return false
        } else if (TextUtils.isEmpty(password.trim())) {
            error.value = "Enter Confirm Password"
            return false
        } else if (password.length < 6) {
            error.value = "Password should be of 6 character"
            return false
        } else if (!password.equals(confirmpassword)) {
            error.value = "Password doesn't match"
            return false
        }
        return true
    }
}
