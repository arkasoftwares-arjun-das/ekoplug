package com.ekoplug.service.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ekoplug.model.MyBookingModel
import com.ekoplug.model.PaginationModel
import com.ekoplug.service.basesetup.BaseResponseArrayModel
import com.ekoplug.service.basesetup.WebServiceRepository
import com.ekoplug.service.model.MyBookingListModel
import com.ekoplug.utils.Utility

class UpcomingViewModel(application: Application) : AndroidViewModel(application) {
    var webServiceRepository: WebServiceRepository
    var error = MutableLiveData<String>()
    var upcomingModel : MutableLiveData<BaseResponseArrayModel<MyBookingListModel>>
    init {
        webServiceRepository = WebServiceRepository(application)

        /*@upcomingModel set ref. in WebRepositry class*/
        upcomingModel = webServiceRepository.getBookingUpcomingData()
    }


    fun getMyBookingData() : MutableLiveData<BaseResponseArrayModel<MyBookingListModel>> {
        return upcomingModel
    }
    fun setMyBookingData(token :String){
        webServiceRepository.setMyBookingUpcomingData(token)
    }
}
