package com.ekoplug.service.viewmodel

import android.app.Application
import android.text.TextUtils
import android.util.Patterns
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData

import com.ekoplug.service.basesetup.BaseResponseModel
import com.ekoplug.service.basesetup.WebServiceRepository
import com.ekoplug.service.model.LoginResponseModel

class LoginViewModel(application: Application) : AndroidViewModel(application) {
    var webServiceRepository: WebServiceRepository
    private var loginResponseModel: MutableLiveData<BaseResponseModel<LoginResponseModel>>? = null
    private var socialLoginResponseModel: MutableLiveData<BaseResponseModel<LoginResponseModel>>? = null
    var email = MutableLiveData<String>()
    var password = MutableLiveData<String>()
    var error = MutableLiveData<String>()

    init {
        webServiceRepository = WebServiceRepository(application)

        /*@getloginResPonseModel set ref. in WebRepositry class*/
        loginResponseModel = webServiceRepository.getLoginResponseModel()

        socialLoginResponseModel = webServiceRepository.getSocialLogin()

    }

    /*@getLoginResponseModel this Method return data after api call */
    fun getLoginResponseModel(): MutableLiveData<BaseResponseModel<LoginResponseModel>> {
        return loginResponseModel!!;
    }

    /*@setLoginData this Method api call on webRepositry*/
    fun setLoginData() {
        if (isValidUserData(email.value.toString(),password.value.toString())) {
            webServiceRepository.setLoginData(email.value.toString(), password.value.toString());
        }
    }


    fun getSocialLoginData(): MutableLiveData<BaseResponseModel<LoginResponseModel>> {
        return socialLoginResponseModel!!;
    }

    /*@setLoginData this Method api call on webRepositry*/
    fun setSocialLogin(email : String,profile_image:String,username:String,uid:String,provider:String) {
        webServiceRepository.setSocialLogin(email,profile_image,username,uid,provider);
    }

    private fun isValidUserData(email:String, password:String): Boolean {
       if (TextUtils.isEmpty(email.trim())) {
            error.value = "Enter email"
            return false
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            error.value = "Invalid email"
            return false
        } else if (TextUtils.isEmpty(password.trim())) {
            error.value = "Enter password"
            return false
        }/*else if (password.length < 6) {
           error.value = "Password should be of 6 character"
           return false
       }*/
        return true
    }
}