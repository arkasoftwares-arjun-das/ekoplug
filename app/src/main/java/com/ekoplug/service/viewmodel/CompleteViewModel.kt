package com.ekoplug.service.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ekoplug.model.MyBookingModel
import com.ekoplug.service.basesetup.BaseResponseArrayModel
import com.ekoplug.service.basesetup.WebServiceRepository
import com.ekoplug.service.model.MyBookingListModel

class CompleteViewModel(application: Application) : AndroidViewModel(application) {
    var webServiceRepository: WebServiceRepository
    var error = MutableLiveData<String>()
    var completeModel : MutableLiveData<BaseResponseArrayModel<MyBookingListModel>>
    init {
        webServiceRepository = WebServiceRepository(application)

        /*@upcomingModel set ref. in WebRepositry class*/
        completeModel = webServiceRepository.getBookingCompleteData()
    }


    fun getMyBookingCompleteData() : MutableLiveData<BaseResponseArrayModel<MyBookingListModel>> {
        return completeModel
    }
    fun setMyBookingCompleteData(token :String){
        webServiceRepository.setMyBookingCompleteData(token)
    }
}
