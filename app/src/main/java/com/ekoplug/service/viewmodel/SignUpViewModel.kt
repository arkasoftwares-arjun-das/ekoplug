package com.ekoplug.service.viewmodel

import android.app.Application
import android.text.TextUtils
import android.util.Patterns
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData

import com.ekoplug.model.UserModel
import com.ekoplug.service.basesetup.BaseResponseModel
import com.ekoplug.service.basesetup.WebServiceRepository
import com.ekoplug.service.model.SignupResponseModel
import okhttp3.MultipartBody

class SignUpViewModel(application: Application) : AndroidViewModel(application) {
    var webServiceRepository: WebServiceRepository
    private var signupResponseModel: MutableLiveData<BaseResponseModel<SignupResponseModel>>? = null
    var name = MutableLiveData<String>()
    var email = MutableLiveData<String>()
    var password = MutableLiveData<String>()
    var confirmPassword = MutableLiveData<String>()
    var address = MutableLiveData<String>()
    var zipCode = MutableLiveData<String>()
    var city = MutableLiveData<String>()
    var state = MutableLiveData<String>()
    var country = MutableLiveData<String>()
    var latitude = MutableLiveData<Double>()
    var longitude = MutableLiveData<Double>()
    var phoneNumber = MutableLiveData<String>()
    var error = MutableLiveData<String>()

    init {
        webServiceRepository = WebServiceRepository(application)
        /*@getloginResPonseModel set ref. in WebRepositry class*/
        signupResponseModel = webServiceRepository.getSignupResponseModel()
//        setSignUpData()
    }

    /*@getSignUpResponseModel this Method return data after api call */

     fun getSignUpResponseModel(): MutableLiveData<BaseResponseModel<SignupResponseModel>> {
        return signupResponseModel!!;
    }

    /*@setSignUpData this Method api call on webRepositry*/
    fun setSignUpData(requestBody: MultipartBody) {
        webServiceRepository.setSignupData(requestBody);

      /*  if (isValidUserData(userModel)) {
            webServiceRepository.setSignupData(userModel);
        }*/
    }

    private fun isValidUserData(user: UserModel): Boolean {
        if (TextUtils.isEmpty(user.username?.trim())) {
            error.value = "Enter username"
            return false
        } else if (TextUtils.isEmpty(user.email?.trim())) {
            error.value = "Enter email"
            return false
        } else if (!Patterns.EMAIL_ADDRESS.matcher(user.email).matches()) {
            error.value = "Invalid email"
            return false
        } else if (TextUtils.isEmpty(user.password?.trim())) {
            error.value = "Enter password"
            return false
        } else if (user.password!!.length < 6) {
            error.value = "Password should be of 6 character"
            return false
        }else if (TextUtils.isEmpty(user.confirm_password?.trim())) {
            error.value = "Enter confirm password"
            return false
        } else if (!user.password?.trim().equals(user.confirm_password?.trim())) {
            error.value = "Password doesn't match"
            return false
        } else if (TextUtils.isEmpty(user.address)) {
            error.value = "Enter address"
            return false
        } else if (TextUtils.isEmpty(user.phone_number)) {
            error.value = "Enter phone number"
            return false
        }
        return true
    }
}