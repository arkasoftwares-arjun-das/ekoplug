package com.ekoplug.service.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.ekoplug.model.NotificationModel
import com.ekoplug.service.basesetup.BaseResponseModel
import com.ekoplug.service.basesetup.WebServiceRepository
import com.ekoplug.service.model.ForgotPasswardResponseModel
import com.ekoplug.service.model.HomeModel

class HomeViewModel(application: Application) : AndroidViewModel(application) {
    var webServiceRepository: WebServiceRepository
    private var homeResponseModel: MutableLiveData<BaseResponseModel<HomeModel>>
    private var notificationHomeModel: MutableLiveData<BaseResponseModel<NotificationModel>>
    private var updateInfoModelResponse: MutableLiveData<BaseResponseModel<ForgotPasswardResponseModel>>
    var error = MutableLiveData<String>()

    init {
        webServiceRepository = WebServiceRepository(application)

        /*@getloginResPonseModel set ref. in WebRepositry class*/
        homeResponseModel = webServiceRepository.getHomeResponseModel()
        notificationHomeModel = webServiceRepository.getNotificationHomeData()
        updateInfoModelResponse = webServiceRepository.getDeviceInfo()

    }

    /*@getLoginResponseModel this Method return data after api call */
    fun getHomeResponseModel(): MutableLiveData<BaseResponseModel<HomeModel>> {
        return homeResponseModel
    }

    /*@setLoginData this Method api call on webRepositry*/
    fun setHomeData(token: String) {
        webServiceRepository.setHomeData(token)
    }

    /*@getDeviceInfo this Method return data after api call */
    fun getDeviceInfo(): MutableLiveData<BaseResponseModel<ForgotPasswardResponseModel>> {
        return updateInfoModelResponse
    }

    /*@setDeviceInfo this Method api call on webRepositry*/

    fun setDeviceInfo(fcmToken: String, token: String, deviceId: String) {
        webServiceRepository.setDeviceInfo(fcmToken, token, deviceId)
    }

    // notification list
    fun getNotifidationHome(): MutableLiveData<BaseResponseModel<NotificationModel>> {
        return notificationHomeModel
    }

    fun setNotificationHomeModel(token: String) {
        webServiceRepository.setNotificationHomeData(token)
    }


}
