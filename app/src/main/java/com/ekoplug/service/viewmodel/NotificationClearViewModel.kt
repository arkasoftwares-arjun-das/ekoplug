package com.ekoplug.service.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.ekoplug.model.NotificationModel
import com.ekoplug.service.basesetup.BaseResponseModel
import com.ekoplug.service.basesetup.WebServiceRepository

class NotificationClearViewModel(application: Application) : AndroidViewModel(application) {
    var webServiceRepository: WebServiceRepository
    private var notificationModel: MutableLiveData<BaseResponseModel<NotificationModel>>? = null
    var error = MutableLiveData<String>()

    init {
        webServiceRepository = WebServiceRepository(application)
        notificationModel = webServiceRepository.getNotificationDataClear()
    }

    fun getNotificationDataClearModel(): MutableLiveData<BaseResponseModel<NotificationModel>> {
        return notificationModel!!
    }

    fun setNotificationClearData(token: String, id: String){
        webServiceRepository.setNotificationDataClear(token,id);
    }
}