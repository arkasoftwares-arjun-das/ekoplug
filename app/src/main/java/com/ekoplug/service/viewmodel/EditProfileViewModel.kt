package com.ekoplug.service.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.ekoplug.model.AddressProfile
import com.ekoplug.model.ProfileModel
import com.ekoplug.service.basesetup.BaseResponseModel
import com.ekoplug.service.basesetup.WebServiceRepository

class EditProfileViewModel(application: Application) : AndroidViewModel(application) {
    var webServiceRepository: WebServiceRepository
    private lateinit var updateModel: MutableLiveData<BaseResponseModel<ProfileModel<AddressProfile>>>
    var error = MutableLiveData<String>()

    init {
        webServiceRepository = WebServiceRepository(application)

        /*@getloginResPonseModel set ref. in WebRepositry class*/
        updateModel = webServiceRepository.getUpdateProfile()
    }

    fun getUpdateProfile(): MutableLiveData<BaseResponseModel<ProfileModel<AddressProfile>>> {
        return updateModel
    }

    fun setUpdateProfilePersonal(
        name: String,
        email: String,
        age: String,
        phone: String,
        address: String,
        jobType: String,
        gender: String,
        token: String,
        userId: Int,
        latitude: Double,
        longitude: Double
    ) {
        webServiceRepository.setUpdateProfilePersonal(name,email,age,phone,address,jobType,gender,token,userId,latitude,longitude)
    }

    fun setUpdateProfileBank(
        token: String,
        userId: Int,
        bankHolderName: String,
        bankName: String,
        accountNumber: String
    ) {
        webServiceRepository.setUpdateProfileBank(token,userId,bankHolderName,bankName,accountNumber)
    }
}