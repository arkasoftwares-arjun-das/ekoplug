package com.ekoplug.service.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData

import com.ekoplug.model.UpdateProfileMdel
import com.ekoplug.service.basesetup.BaseResponseModel
import com.ekoplug.service.basesetup.WebServiceRepository

class EstimateViewModel(application: Application) : AndroidViewModel(application) {

    var webServiceRepository: WebServiceRepository
    private var estimationModel: MutableLiveData<BaseResponseModel<UpdateProfileMdel>>? = null
    var error = MutableLiveData<String>()

    init {
        webServiceRepository = WebServiceRepository(application)
        estimationModel = webServiceRepository.getEstimationData()
    }

    fun getEstimationData():MutableLiveData<BaseResponseModel<UpdateProfileMdel>>{
        return estimationModel!!
    }

    fun setEstimationData(
        token: String,
        id: Int,
        detailsWork: String,
        time: String,
        cost: String
    ){
        webServiceRepository.setEstimationData(
            token, id,detailsWork,time,cost);
    }
}
