package com.ekoplug.service.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.ekoplug.model.PaymentHistoryModel
import com.ekoplug.service.basesetup.BaseResponseArrayModel
import com.ekoplug.service.basesetup.WebServiceRepository

class PaymentHistoryViewModel(application: Application) : AndroidViewModel(application) {
    var webServiceRepository: WebServiceRepository
    private var paymentHistory: MutableLiveData<BaseResponseArrayModel<PaymentHistoryModel>>? = null

    var error = MutableLiveData<String>()

    init {
        webServiceRepository = WebServiceRepository(application)
        paymentHistory = webServiceRepository.getPaymentHistory()
    }

    fun getPaymentHistory(): MutableLiveData<BaseResponseArrayModel<PaymentHistoryModel>> {
        return paymentHistory!!
    }

    fun setPaymentHistory(token: String) {
        webServiceRepository.setPaymentHistory(token);
    }
}
