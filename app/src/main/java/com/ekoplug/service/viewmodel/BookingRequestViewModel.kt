package com.ekoplug.service.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.ekoplug.model.BookingRequestModel
import com.ekoplug.service.basesetup.BaseResponseArrayModel
import com.ekoplug.service.basesetup.WebServiceRepository

class BookingRequestViewModel(application: Application) : AndroidViewModel(application) {
    var webServiceRepository: WebServiceRepository
    private lateinit var bookingModel: MutableLiveData<BaseResponseArrayModel<BookingRequestModel>>
    private lateinit var currentModel: MutableLiveData<BaseResponseArrayModel<BookingRequestModel>>
    var error = MutableLiveData<String>()

    init {
        webServiceRepository = WebServiceRepository(application)

        /*@getloginResPonseModel set ref. in WebRepositry class*/
        bookingModel = webServiceRepository.getBookingRequestModel()
        currentModel = webServiceRepository.getCurrentBookingModel()
    }

    fun getBookingData(): MutableLiveData<BaseResponseArrayModel<BookingRequestModel>> {
        return bookingModel
    }

    fun setBookingRequestModel(
        token: String
    ) {
        webServiceRepository.setBookingRequestData(token)
    }

    fun getCurrentBookingData(): MutableLiveData<BaseResponseArrayModel<BookingRequestModel>> {
        return currentModel
    }

    fun setCurrentBookingModel(
        token: String
    ) {
        webServiceRepository.setCurrentBookingData(token)
    }


}
