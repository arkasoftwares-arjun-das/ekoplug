package com.ekoplug.service.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.ekoplug.model.StaticPageModel
import com.ekoplug.service.basesetup.BaseResponseModel
import com.ekoplug.service.basesetup.WebServiceRepository

class ContactUsViewModel(application: Application) : AndroidViewModel(application) {
    var webServiceRepository: WebServiceRepository
    private var contactUsModel: MutableLiveData<BaseResponseModel<StaticPageModel>>? =
        null
    var error = MutableLiveData<String>()

    init {
        webServiceRepository = WebServiceRepository(application)

        /*@getloginResPonseModel set ref. in WebRepositry class*/
        contactUsModel = webServiceRepository.getStaticPageRespnse()

    }

    fun getStaticPageData(): MutableLiveData<BaseResponseModel<StaticPageModel>> {
        return contactUsModel!!;
    }

    fun setStaticPageData(token: String, s: String) {
        webServiceRepository.setStaticPage(token,s);
    }
}
