package com.ekoplug.service.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.ekoplug.model.AddressProfile
import com.ekoplug.model.ProfileModel
import com.ekoplug.model.UpdateProfileMdel
import com.ekoplug.service.basesetup.BaseResponseModel
import com.ekoplug.service.basesetup.WebServiceRepository
import okhttp3.MultipartBody

class EstimationUploadViewModel(application: Application) : AndroidViewModel(application) {
    var webServiceRepository: WebServiceRepository
    private var estimationUploadModel: MutableLiveData<BaseResponseModel<UpdateProfileMdel>>
    var error = MutableLiveData<String>()

    init {
        webServiceRepository = WebServiceRepository(application)

        /*@getloginResPonseModel set ref. in WebRepositry class*/
        estimationUploadModel = webServiceRepository.getUploadEstimatinPicture()

    }

    fun getUploadEstimatinPictureModel(): MutableLiveData<BaseResponseModel<UpdateProfileMdel>> {
        return estimationUploadModel;
    }

    fun setUploadEstimatinPicture(
        token: String,
        id: Int,
        body: MultipartBody
    ) {
        webServiceRepository.setUploadEstimation(token,id,body);
    }
}