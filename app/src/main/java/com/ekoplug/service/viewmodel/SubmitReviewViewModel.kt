package com.ekoplug.service.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.ekoplug.model.MyBookingModel
import com.ekoplug.service.basesetup.BaseResponseModel
import com.ekoplug.service.basesetup.WebServiceRepository

class SubmitReviewViewModel(application: Application) : AndroidViewModel(application) {
    var webServiceRepository: WebServiceRepository
    var error = MutableLiveData<String>()

    var submitReviewModel: MutableLiveData<BaseResponseModel<MyBookingModel>>

    init {
        webServiceRepository = WebServiceRepository(application)

        submitReviewModel = webServiceRepository.getReviewData()
    }

    fun getReviewData(): MutableLiveData<BaseResponseModel<MyBookingModel>> {
        return submitReviewModel
    }

    fun setReviewData(
        token: String,
        workerId: Int,
        userId: Int,
        ratings: String,
        comments: String,
        appComments: String,
        prefrenceSelecting: Int
    ) {
        webServiceRepository.setReviewData(token, workerId, userId,ratings, comments,appComments,prefrenceSelecting)
    }

}
