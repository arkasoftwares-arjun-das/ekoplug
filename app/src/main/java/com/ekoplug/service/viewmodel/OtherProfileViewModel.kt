package com.ekoplug.service.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.ekoplug.model.AddressProfile
import com.ekoplug.model.ProfileModel
import com.ekoplug.service.basesetup.BaseResponseModel
import com.ekoplug.service.basesetup.WebServiceRepository
import okhttp3.MultipartBody

class OtherProfileViewModel(application: Application) : AndroidViewModel(application) {
    var webServiceRepository: WebServiceRepository
    private lateinit var updateModel: MutableLiveData<BaseResponseModel<ProfileModel<AddressProfile>>>
    var error = MutableLiveData<String>()

    init {
        webServiceRepository = WebServiceRepository(application)

        /*@getloginResPonseModel set ref. in WebRepositry class*/
        updateModel = webServiceRepository.getUpdateProfile()
    }

    fun getUpdateProfile(): MutableLiveData<BaseResponseModel<ProfileModel<AddressProfile>>> {
        return updateModel
    }

    fun setUpdateProfileOther(
        token: String,
        userId: Int,
        requestBody: MultipartBody
        ) {
        webServiceRepository.setUpdateProfileOther(token,userId,requestBody)
    }
}
