package com.ekoplug.service.viewmodel

import android.app.Application
import android.text.TextUtils
import android.util.Patterns
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData

import com.ekoplug.service.basesetup.BaseResponseModel
import com.ekoplug.service.basesetup.WebServiceRepository
import com.ekoplug.service.model.ForgotPasswardResponseModel

class ForgotPasswordViewModel(application: Application) : AndroidViewModel(application) {
    var webServiceRepository: WebServiceRepository
    private var forgotResponseModel: MutableLiveData<BaseResponseModel<ForgotPasswardResponseModel>>? =
        null
    var email = MutableLiveData<String>()
    var error = MutableLiveData<String>()

    init {
        webServiceRepository = WebServiceRepository(application)
        forgotResponseModel = webServiceRepository.getForgotPasswordResponseModel()
    }

    /*@getForgotResponseModel this Method return data after api call */

    fun getForgotResponseModel(): MutableLiveData<BaseResponseModel<ForgotPasswardResponseModel>> {
        return forgotResponseModel!!;
    }

    /*@ssetForgotData this Method api call on webRepositry*/
    fun setForgotData() {

        if (isValidUserData(email.value.toString())) {
            webServiceRepository.setForgotPassword(email.value.toString());
        }
    }

    private fun isValidUserData(email: String): Boolean {
        if (TextUtils.isEmpty(email.trim())) {
            error.value = "Enter email"
            return false
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            error.value = "Invalid email"
            return false
        }
        return true
    }
}