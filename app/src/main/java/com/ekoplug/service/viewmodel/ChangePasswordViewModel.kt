package com.ekoplug.service.viewmodel

import android.app.Application
import android.text.TextUtils
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.ekoplug.service.basesetup.BaseResponseModel
import com.ekoplug.service.basesetup.WebServiceRepository
import com.ekoplug.service.model.LoginResponseModel

class ChangePasswordViewModel(application: Application) : AndroidViewModel(application) {
    var webServiceRepository: WebServiceRepository
    private var changePasswordModel: MutableLiveData<BaseResponseModel<LoginResponseModel>>? = null
    var confirmPassword = MutableLiveData<String>()
    var password = MutableLiveData<String>()
    var error = MutableLiveData<String>()

    init {
        webServiceRepository = WebServiceRepository(application)

        changePasswordModel = webServiceRepository.getChangePasswordResponseModel()

    }

    /*@getChangePasswordResponseModel this Method return data after api call */
    fun getChangePasswordResponseModel(): MutableLiveData<BaseResponseModel<LoginResponseModel>> {
        return changePasswordModel!!;
    }

    /*@setLoginData this Method api call on webRepositry*/
    fun setLoginData(token: String) {
        if (isValidUserData(confirmPassword.value.toString(), password.value.toString())) {
            webServiceRepository.setChangePassword(
                confirmPassword.value.toString(),
                password.value.toString(),token
            );
        }
    }

    private fun isValidUserData(confirmpassword: String, password: String): Boolean {
        if (TextUtils.isEmpty(confirmpassword.trim())) {
            error.value = "Enter password"
            return false
        } else if (TextUtils.isEmpty(password.trim())) {
            error.value = "Enter Confirm Password"
            return false
        }else if (password.length < 6) {
            error.value = "Password should be of 6 character"
            return false
        } else if (!password.equals(confirmpassword)) {
            error.value = "Password doesn't match"
            return false
        }
        return true
    }
}