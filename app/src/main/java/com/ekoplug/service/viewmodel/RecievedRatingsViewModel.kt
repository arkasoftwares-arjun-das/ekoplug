package com.ekoplug.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.ekoplug.service.basesetup.BaseResponseArrayModel
import com.ekoplug.service.basesetup.WebServiceRepository
import com.ekoplug.service.model.ForgotPasswardResponseModel
import com.ekoplug.service.model.SubmitReviewModel


class RecievedRatingsViewModel(application: Application) : AndroidViewModel(application) {
    var webServiceRepository: WebServiceRepository
    private var recievedRatings: MutableLiveData<BaseResponseArrayModel<SubmitReviewModel>>? =
        null
    var error = MutableLiveData<String>()

    init {
        webServiceRepository = WebServiceRepository(application)
        recievedRatings = webServiceRepository.getRecievedRatings()
    }

    fun getRecievedRatingsData(): MutableLiveData<BaseResponseArrayModel<SubmitReviewModel>> {
        return recievedRatings!!
    }

    fun setRecievedRatingsData(token: String) {
        webServiceRepository.setRecievedRatings(token);
    }
}
