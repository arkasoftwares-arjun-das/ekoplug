package com.ekoplug.service.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.ekoplug.service.basesetup.BaseResponseArrayModel
import com.ekoplug.service.basesetup.WebServiceRepository
import com.ekoplug.service.model.CategoryModel

class CategoryViewModel(application: Application) : AndroidViewModel(application) {
    var webServiceRepository: WebServiceRepository
    private var categoryResponseModel: MutableLiveData<BaseResponseArrayModel<CategoryModel>>

    var error = MutableLiveData<String>()

    init {
        webServiceRepository = WebServiceRepository(application)

        /*@getloginResPonseModel set ref. in WebRepositry class*/
        categoryResponseModel = webServiceRepository.getCategoryResponseModel()

    }

    /*@getLoginResponseModel this Method return data after api call */
    fun getCategoryResponse(): MutableLiveData<BaseResponseArrayModel<CategoryModel>> {
        return categoryResponseModel
    }

    /*@setLoginData this Method api call on webRepositry*/
    fun setCategoryResponse() {
        webServiceRepository.setCategoryData()
    }

}
