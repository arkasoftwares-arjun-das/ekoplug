package com.ekoplug.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ekoplug.model.NotificationModel
import com.ekoplug.model.UpdateProfileMdel
import com.ekoplug.service.basesetup.BaseResponseArrayModel
import com.ekoplug.service.basesetup.WebServiceRepository

class NotificationListViewModel(application: Application) : AndroidViewModel(application) {
    var webServiceRepository: WebServiceRepository
    private var notificationModel: MutableLiveData<BaseResponseArrayModel<NotificationModel>>? = null
    var error = MutableLiveData<String>()

    init {
        webServiceRepository = WebServiceRepository(application)
        notificationModel = webServiceRepository.getNotificationData()
    }

    fun getNotificationDataModel(): MutableLiveData<BaseResponseArrayModel<NotificationModel>> {
        return notificationModel!!
    }
    fun setNotificationData(token:String){
        webServiceRepository.setNotificationData(
            token);
    }
}
