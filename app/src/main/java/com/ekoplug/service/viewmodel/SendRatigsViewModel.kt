package com.ekoplug.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData

import com.ekoplug.service.basesetup.BaseResponseArrayModel
import com.ekoplug.service.basesetup.WebServiceRepository
import com.ekoplug.service.model.ForgotPasswardResponseModel
import com.ekoplug.service.model.SubmitReviewModel

class SendRatigsViewModel(application: Application) : AndroidViewModel(application) {
    var webServiceRepository: WebServiceRepository
    private var sendRatings: MutableLiveData<BaseResponseArrayModel<SubmitReviewModel>>? =
        null
    var error = MutableLiveData<String>()

    init {
        webServiceRepository = WebServiceRepository(application)
        sendRatings = webServiceRepository.getSendRatings()
    }

    fun getSendRatingsData(): MutableLiveData<BaseResponseArrayModel<SubmitReviewModel>> {
        return sendRatings!!
    }

    fun setSendRatingsData(token: String) {
        webServiceRepository.setSendRatings(token);
    }}
