package com.ekoplug.service.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.ekoplug.model.WorkerReviewModel
import com.ekoplug.service.basesetup.BaseResponseArrayModel
import com.ekoplug.service.basesetup.BaseResponseModel
import com.ekoplug.service.basesetup.WebServiceRepository
import com.ekoplug.service.model.BookingDetailsModel

class BookingRequestDetailsViewModel(application: Application) : AndroidViewModel(application) {
    var webServiceRepository: WebServiceRepository
    private lateinit var bookingDetailsModel: MutableLiveData<BaseResponseModel<BookingDetailsModel>>
    private lateinit var bookingAcceptModel: MutableLiveData<BaseResponseModel<BookingDetailsModel>>
    private lateinit var reviewListModel: MutableLiveData<BaseResponseArrayModel<WorkerReviewModel>>

    var error = MutableLiveData<String>()

    init {
        webServiceRepository = WebServiceRepository(application)

        /*@getloginResPonseModel set ref. in WebRepositry class*/
        bookingDetailsModel = webServiceRepository.getBookingDetails()
        bookingAcceptModel = webServiceRepository.getReqeustAccept()
        reviewListModel = webServiceRepository.getReviewList()
    }

    fun getBookingDetailsData(): MutableLiveData<BaseResponseModel<BookingDetailsModel>> {
        return bookingDetailsModel
    }

    fun setBookingDetailsData(token: String, id: Int) {
        webServiceRepository.setBookingDetails(token, id)
    }

    // booking accept
    fun getAcceptRequest(): MutableLiveData<BaseResponseModel<BookingDetailsModel>> {
        return bookingAcceptModel
    }

    fun setAcceptRequest(token: String, id: Int, accept: String) {
        webServiceRepository.setRequestAccept(token, id,accept)
    }

    // Reviews list
    fun getReviewList(): MutableLiveData<BaseResponseArrayModel<WorkerReviewModel>> {
        return reviewListModel
    }

    fun setReviewList(token: String, id: Int) {
        webServiceRepository.setReviewList(token, id)
    }
}
