package com.ekoplug.service.basesetup

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.ekoplug.model.*
import com.ekoplug.service.model.*
import com.google.gson.JsonObject
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class WebServiceRepository(val application: Application) {
    private val TAG = WebServiceRepository::class.java.simpleName
    private val signupResponseModel = MutableLiveData<BaseResponseModel<SignupResponseModel>>()
    private val otpVerifyModel = MutableLiveData<BaseResponseModel<OtpVerifyResponseModel>>()
    private val loginResponseModel = MutableLiveData<BaseResponseModel<LoginResponseModel>>()
    private val forgotPasswordModel =
        MutableLiveData<BaseResponseModel<ForgotPasswardResponseModel>>()
    private val changePasswordModel = MutableLiveData<BaseResponseModel<LoginResponseModel>>()
    private val categoryModel = MutableLiveData<BaseResponseArrayModel<CategoryModel>>()
    private val homeModel = MutableLiveData<BaseResponseModel<HomeModel>>()


    private val homeSearchModelResponse = MutableLiveData<BaseResponseModel<PaginationModel>>()
    private val allCategoryModel = MutableLiveData<BaseResponseModel<PaginationModel>>()
    private val allCategorySearchModel = MutableLiveData<BaseResponseModel<PaginationModel>>()
    private val bookingRequestModel =
        MutableLiveData<BaseResponseArrayModel<BookingRequestModel>>()
    private val currentBookingModel =
        MutableLiveData<BaseResponseArrayModel<BookingRequestModel>>()
    val bookingDetailsModel = MutableLiveData<BaseResponseModel<BookingDetailsModel>>()
    val bookingAcceptModel = MutableLiveData<BaseResponseModel<BookingDetailsModel>>()
    val reviewListModel = MutableLiveData<BaseResponseArrayModel<WorkerReviewModel>>()
    private val serviceProviderReviewModel =
        MutableLiveData<BaseResponseArrayModel<WorkerReviewModel>>()
    private val profileModel = MutableLiveData<BaseResponseModel<ProfileModel<AddressProfile>>>()
    private val changeCurrentPasswordModel =
        MutableLiveData<BaseResponseModel<ProfileModel<AddressProfile>>>()
    private val cancelRequestModel =
        MutableLiveData<BaseResponseModel<BookingDetailsModel>>()
    private val updateProfileModel = MutableLiveData<BaseResponseModel<ProfileModel<AddressProfile>>>()
    //    private val bookingRequestModel = MutableLiveData<BaseResponseModel<BookingRequestModel>>()
    private val myBookingUpcomingModel = MutableLiveData<BaseResponseArrayModel<MyBookingListModel>>()
    private val myBookingCompleteModel = MutableLiveData<BaseResponseArrayModel<MyBookingListModel>>()
    private val myBookingDetailsModel = MutableLiveData<BaseResponseModel<MyBookingDetailsModel>>()
    private val cancelBookingModel = MutableLiveData<BaseResponseModel<MyBookingModel>>()
    private val resendCodeModel = MutableLiveData<BaseResponseModel<ForgotPasswardResponseModel>>()
    private val estimationModel = MutableLiveData<BaseResponseModel<UpdateProfileMdel>>()
    private val notificationModel = MutableLiveData<BaseResponseArrayModel<NotificationModel>>()
    private val notificationClearModel = MutableLiveData<BaseResponseModel<NotificationModel>>()
    private val notificationHomeModel = MutableLiveData<BaseResponseModel<NotificationModel>>()
    private val updateInfoModel = MutableLiveData<BaseResponseModel<ForgotPasswardResponseModel>>()
    private val recievedRatingsModel =
        MutableLiveData<BaseResponseArrayModel<SubmitReviewModel>>()
    private val sendRatingsModel =
        MutableLiveData<BaseResponseArrayModel<SubmitReviewModel>>()
    private val userOfferModel = MutableLiveData<BaseResponseArrayModel<OfferModel>>()
    private val addCardResponeModel = MutableLiveData<BaseResponseModel<AddCardResponseModel>>()
    private val cardListResponeModel = MutableLiveData<BaseResponseArrayModel<AddCardResponseModel>>()
    private val socialLogin = MutableLiveData<BaseResponseModel<LoginResponseModel>>()
    private val updateProfilePicture = MutableLiveData<BaseResponseModel<UpdateProfileMdel>>()
    private val staticPageModel = MutableLiveData<BaseResponseModel<StaticPageModel>>()
    private var paymentHistoryModel = MutableLiveData<BaseResponseArrayModel<PaymentHistoryModel>>()
    private val submitReivewModel = MutableLiveData<BaseResponseModel<MyBookingModel>>()


    // SIGN UP REPOSITORY
    fun getSignupResponseModel(): MutableLiveData<BaseResponseModel<SignupResponseModel>> {
        return signupResponseModel;
    }

    fun setSignupData(
        body: MultipartBody
    ) {
        val call = ApiService.getRestService().signUpApi(body)
        call.enqueue(object : Callback<BaseResponseModel<SignupResponseModel>> {
            override fun onResponse(
                call: Call<BaseResponseModel<SignupResponseModel>>,
                response: Response<BaseResponseModel<SignupResponseModel>>
            ) {
                signupResponseModel.postValue(response.body())
            }

            override fun onFailure(
                call: Call<BaseResponseModel<SignupResponseModel>>,
                t: Throwable
            ) {
                signupResponseModel.postValue(null)
                t.printStackTrace()
            }
        })
    }

    // OTP VERIFY REPOSITORY
    fun getOtpVerifyResponseModel(): MutableLiveData<BaseResponseModel<OtpVerifyResponseModel>> {
        return otpVerifyModel
    }

    fun setOtpVerifyData(email: String, otp: String) {

        val jsonData = JsonObject()
        jsonData.addProperty("code", otp)
        jsonData.addProperty("email", email)

        val functionals = ApiService.getRestService().otpVerify("application/json", jsonData)
        functionals.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<BaseResponseModel<OtpVerifyResponseModel>> {
                override fun onSubscribe(d: Disposable) {

                }

                override fun onNext(baseResponseBody: BaseResponseModel<OtpVerifyResponseModel>) {
                    otpVerifyModel.postValue(baseResponseBody)
                }

                override fun onError(e: Throwable) {
                    otpVerifyModel.postValue(null)
                }

                override fun onComplete() {

                }
            })

    }

    // LOGIN REPOSITORY
    fun getLoginResponseModel(): MutableLiveData<BaseResponseModel<LoginResponseModel>> {
        return loginResponseModel
    }

    fun setLoginData(email: String, password: String) {

        val jsonData = JsonObject()
        jsonData.addProperty("email", email)
        jsonData.addProperty("password", password)

        val functionals = ApiService.getRestService().login("application/json", "worker", jsonData)
        functionals.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<BaseResponseModel<LoginResponseModel>> {
                override fun onSubscribe(d: Disposable) {

                }

                override fun onNext(baseResponseBody: BaseResponseModel<LoginResponseModel>) {
                    loginResponseModel.postValue(baseResponseBody)
                }

                override fun onError(e: Throwable) {
                    loginResponseModel.postValue(null)
                }

                override fun onComplete() {

                }
            })

    }

    // FORGOT PASSWORD REPOSITORY
    fun getForgotPasswordResponseModel(): MutableLiveData<BaseResponseModel<ForgotPasswardResponseModel>> {
        return forgotPasswordModel
    }

    fun setForgotPassword(email: String) {

        val jsonData = JsonObject()
        jsonData.addProperty("email", email)

        val functionals =
            ApiService.getRestService().setForgotPassword("application/json", "worker", jsonData)
        functionals.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<BaseResponseModel<ForgotPasswardResponseModel>> {
                override fun onSubscribe(d: Disposable) {

                }

                override fun onNext(baseResponseBody: BaseResponseModel<ForgotPasswardResponseModel>) {
                    forgotPasswordModel.postValue(baseResponseBody)
                }

                override fun onError(e: Throwable) {
                    forgotPasswordModel.postValue(null)
                }

                override fun onComplete() {

                }
            })

    }

    // CHANGE PASSWORD REPOSITORY
    fun getChangePasswordResponseModel(): MutableLiveData<BaseResponseModel<LoginResponseModel>> {
        return changePasswordModel
    }

    fun setChangePassword(
        password: String,
        confirmpassword: String,
        token: String
    ) {

        val jsonData = JsonObject()
        jsonData.addProperty("new_password", password)
        jsonData.addProperty("confirm_password", confirmpassword)

        val functionals = ApiService.getRestService()
            .changePassword("application/json","Token " + token,  jsonData)
        functionals.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<BaseResponseModel<LoginResponseModel>> {
                override fun onSubscribe(d: Disposable) {

                }

                override fun onNext(baseResponseBody: BaseResponseModel<LoginResponseModel>) {
                    changePasswordModel.postValue(baseResponseBody)
                }

                override fun onError(e: Throwable) {
                    changePasswordModel.postValue(null)
                }

                override fun onComplete() {

                }
            })

    }

        // Category REPOSITORY
        fun getCategoryResponseModel(): MutableLiveData<BaseResponseArrayModel<CategoryModel>> {
            return categoryModel
        }

        fun setCategoryData() {
            val functionals = ApiService.getRestService().categoryModel("application/json"
                )
            functionals.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<BaseResponseArrayModel<CategoryModel>> {
                    override fun onSubscribe(d: Disposable) {

                    }

                    override fun onNext(baseResponseBody: BaseResponseArrayModel<CategoryModel>) {
                        categoryModel.postValue(baseResponseBody)
                    }

                    override fun onError(e: Throwable) {
                        categoryModel.postValue(null)
                    }

                    override fun onComplete() {

                    }
                })
        }

    // Home Search
    fun getHomeResponseModel(): MutableLiveData<BaseResponseModel<HomeModel>> {
        return homeModel
    }

    fun setHomeData(token: String) {
        val call = ApiService.getRestService().homeApi("application/json", "Token " + token)
        call.enqueue(object : Callback<BaseResponseModel<HomeModel>> {
            override fun onResponse(
                call: Call<BaseResponseModel<HomeModel>>,
                response: Response<BaseResponseModel<HomeModel>>
            ) {
                homeModel.postValue(response.body())
            }

            override fun onFailure(call: Call<BaseResponseModel<HomeModel>>, t: Throwable) {
                homeModel.postValue(null)
                t.printStackTrace()
            }
        })
    }


    //     Booking Request
    fun getBookingRequestModel(): MutableLiveData<BaseResponseArrayModel<BookingRequestModel>> {
        return bookingRequestModel
    }

    fun setBookingRequestData(
        token: String
    ) {
        val functionals =
            ApiService.getRestService().bookingRequestData("application/json", "Token " + token)
        functionals.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object :
                Observer<BaseResponseArrayModel<BookingRequestModel>> {
                override fun onSubscribe(d: Disposable) {

                }

                override fun onNext(baseResponseBody: BaseResponseArrayModel<BookingRequestModel>) {
                    bookingRequestModel.postValue(baseResponseBody)
                }

                override fun onError(e: Throwable) {
                    bookingRequestModel.postValue(null)
                }

                override fun onComplete() {

                }
            })
    }

    //  Current Booking Request
    fun getCurrentBookingModel(): MutableLiveData<BaseResponseArrayModel<BookingRequestModel>> {
        return currentBookingModel
    }

    fun setCurrentBookingData(
        token: String
    ) {
        val functionals =
            ApiService.getRestService().currentBookingData("application/json", "Token " + token)
        functionals.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object :
                Observer<BaseResponseArrayModel<BookingRequestModel>> {
                override fun onSubscribe(d: Disposable) {

                }

                override fun onNext(baseResponseBody: BaseResponseArrayModel<BookingRequestModel>) {
                    currentBookingModel.postValue(baseResponseBody)
                }

                override fun onError(e: Throwable) {
                    currentBookingModel.postValue(null)
                }

                override fun onComplete() {

                }
            })
    }

    // Booking Request details
    fun getBookingDetails(): MutableLiveData<BaseResponseModel<BookingDetailsModel>> {
        return bookingDetailsModel
    }

    fun setBookingDetails(token: String, id: Int) {
        val functionals =
            ApiService.getRestService().bookingDetails("application/json", "Token " + token, id)
        functionals.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<BaseResponseModel<BookingDetailsModel>> {
                override fun onSubscribe(d: Disposable) {

                }

                override fun onNext(baseResponseBody: BaseResponseModel<BookingDetailsModel>) {
                    bookingDetailsModel.postValue(baseResponseBody)
                }

                override fun onError(e: Throwable) {
                    bookingDetailsModel.postValue(null)
                }

                override fun onComplete() {

                }

            })
    }

    // Service Provider Review
    fun getServiceProviderReview(): MutableLiveData<BaseResponseArrayModel<WorkerReviewModel>> {
        return serviceProviderReviewModel
    }

    fun setServiceProviderReviewData(id: Int) {
        val functionals = ApiService.getRestService().serviceProviderReview("application/json", id)
        functionals.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<BaseResponseArrayModel<WorkerReviewModel>> {
                override fun onSubscribe(d: Disposable) {

                }

                override fun onNext(baseResponseBody: BaseResponseArrayModel<WorkerReviewModel>) {
                    serviceProviderReviewModel.postValue(baseResponseBody)
                }

                override fun onError(e: Throwable) {
                    serviceProviderReviewModel.postValue(null)
                }

                override fun onComplete() {

                }
            })
    }

    // Profile Api
    fun getProfileReviewModel(): MutableLiveData<BaseResponseModel<ProfileModel<AddressProfile>>> {
        return profileModel
    }

    fun setProfileData(token: String, id: Int) {
        val functionals =
            ApiService.getRestService().profileData("application/json", "Token " + token, id)
        functionals.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<BaseResponseModel<ProfileModel<AddressProfile>>> {
                override fun onSubscribe(d: Disposable) {

                }

                override fun onNext(baseResponseBody: BaseResponseModel<ProfileModel<AddressProfile>>) {
                    profileModel.postValue(baseResponseBody)
                }

                override fun onError(e: Throwable) {
                    profileModel.postValue(null)
                }

                override fun onComplete() {

                }
            })
    }

    // Profile picture update Repository
    fun getProfilePictureUpdate(): MutableLiveData<BaseResponseModel<UpdateProfileMdel>> {
        return updateProfilePicture
    }

    fun setUpdateProfilePicture(
        token: String,
        id: Int,
        body: MultipartBody
    ) {
        val call = ApiService.getRestService().updateProfilePicture("Token " + token, id, body)
        call.enqueue(object : Callback<BaseResponseModel<UpdateProfileMdel>> {
            override fun onResponse(
                call: Call<BaseResponseModel<UpdateProfileMdel>>,
                response: Response<BaseResponseModel<UpdateProfileMdel>>
            ) {
                updateProfilePicture.postValue(response.body())
            }

            override fun onFailure(call: Call<BaseResponseModel<UpdateProfileMdel>>, t: Throwable) {
                updateProfilePicture.postValue(null)
                t.printStackTrace()
            }
        })
    }

    // CHANGE PASSWORD REPOSITORY
    fun getCurrentChangePassword(): MutableLiveData<BaseResponseModel<ProfileModel<AddressProfile>>> {
        return changeCurrentPasswordModel
    }

    fun setChangeCurrentPassword(
        currentPassword: String,
        password: String,
        confirmpassword: String,
        token: String
    ) {
        val jsonData = JsonObject()
        jsonData.addProperty("old_password", currentPassword)
        jsonData.addProperty("new_password", password)
        jsonData.addProperty("confirm_password", confirmpassword)

        val functionals = ApiService.getRestService()
            .changeCurrentPassword("application/json", "Token " + token, jsonData)
        functionals.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<BaseResponseModel<ProfileModel<AddressProfile>>> {
                override fun onSubscribe(d: Disposable) {

                }

                override fun onNext(baseResponseBody: BaseResponseModel<ProfileModel<AddressProfile>>) {
                    changeCurrentPasswordModel.postValue(baseResponseBody)
                }

                override fun onError(e: Throwable) {
                    changeCurrentPasswordModel.postValue(null)

                }

                override fun onComplete() {

                }
            })
    }

    // Cancel booking REPOSITORY
    fun getCancelBookingRequest(): MutableLiveData<BaseResponseModel<BookingDetailsModel>> {
        return cancelRequestModel
    }

    fun setCancelBookingRequest(
        cancelReason: String,
        cancelComment: String,
        token: String,
        id:Int
    ) {
        val jsonData = JsonObject()
        jsonData.addProperty("cancel_reason", cancelReason)
        jsonData.addProperty("cancel_comment", cancelComment)
        jsonData.addProperty("booking_status", "REJECT")

        val functionals = ApiService.getRestService()
            .cancelRequest("application/json", "Token " + token,id, jsonData)
        functionals.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<BaseResponseModel<BookingDetailsModel>> {
                override fun onSubscribe(d: Disposable) {

                }

                override fun onNext(baseResponseBody: BaseResponseModel<BookingDetailsModel>) {
                    cancelRequestModel.postValue(baseResponseBody)
                }

                override fun onError(e: Throwable) {
                    cancelRequestModel.postValue(null)

                }

                override fun onComplete() {

                }
            })
    }

    // Complete Booking Repository
    fun getResentData(): MutableLiveData<BaseResponseModel<ForgotPasswardResponseModel>> {
        return resendCodeModel
    }

    fun setResentData(email: String) {
        val jsonData = JsonObject()
        jsonData.addProperty("email", email)

        val functionals = ApiService.getRestService().resentCode("application/json", jsonData)
        functionals.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<BaseResponseModel<ForgotPasswardResponseModel>> {
                override fun onSubscribe(d: Disposable) {

                }

                override fun onNext(baseResponseBody: BaseResponseModel<ForgotPasswardResponseModel>) {
                    resendCodeModel.postValue(baseResponseBody)
                }

                override fun onError(e: Throwable) {
                    resendCodeModel.postValue(null)
                }

                override fun onComplete() {

                }
            })
    }

    // Complete Booking Repository
    fun getBookingCompleteData(): MutableLiveData<BaseResponseArrayModel<MyBookingListModel>> {
        return myBookingCompleteModel
    }

    fun setMyBookingCompleteData(token: String) {
        val functionals =
            ApiService.getRestService().completeJob("application/json", "Token " + token)
        functionals.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<BaseResponseArrayModel<MyBookingListModel>> {
                override fun onSubscribe(d: Disposable) {

                }

                override fun onNext(baseResponseBody: BaseResponseArrayModel<MyBookingListModel>) {
                    myBookingCompleteModel.postValue(baseResponseBody)
                }

                override fun onError(e: Throwable) {
                    myBookingCompleteModel.postValue(null)
                }

                override fun onComplete() {

                }
            })
    }

    // UpcomingBooking
    fun getBookingUpcomingData(): MutableLiveData<BaseResponseArrayModel<MyBookingListModel>> {
        return myBookingUpcomingModel
    }

    fun setMyBookingUpcomingData(token: String) {
        val functionals = ApiService.getRestService()
            .upcomingJob("application/json", "Token " + token)
        functionals.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<BaseResponseArrayModel<MyBookingListModel>> {
                override fun onSubscribe(d: Disposable) {

                }

                override fun onNext(baseResponseBody: BaseResponseArrayModel<MyBookingListModel>) {
                    myBookingUpcomingModel.postValue(baseResponseBody)
                }

                override fun onError(e: Throwable) {
                    myBookingUpcomingModel.postValue(null)
                }

                override fun onComplete() {

                }
            })
    }

    // cancel Booking Repository
    fun getCancelBookingData(): MutableLiveData<BaseResponseModel<MyBookingModel>> {
        return cancelBookingModel
    }

    fun setCancelBookingData(token: String, id: Int, cancelReason: String, cancelComment: String) {
        val jsonData = JsonObject()
        jsonData.addProperty("cancel_reason", cancelReason)
        jsonData.addProperty("cancel_comment", cancelComment)

        val functionals = ApiService.getRestService()
            .cancelBooking("application/json", "Token " + token, id, jsonData)
        functionals.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<BaseResponseModel<MyBookingModel>> {
                override fun onSubscribe(d: Disposable) {

                }

                override fun onNext(baseResponseBody: BaseResponseModel<MyBookingModel>) {
                    cancelBookingModel.postValue(baseResponseBody)
                }

                override fun onError(e: Throwable) {
                    cancelBookingModel.postValue(null)
                }

                override fun onComplete() {

                }
            })
    }

    // Booking Accept Request details
    fun getReqeustAccept(): MutableLiveData<BaseResponseModel<BookingDetailsModel>> {
        return bookingAcceptModel
    }

    fun setRequestAccept(token: String, id: Int, accept: String) {
        val jsonData = JsonObject()
        jsonData.addProperty("booking_status", accept)
        val functionals =
            ApiService.getRestService().bookingAccept("application/json", "Token " + token, id,jsonData)
        functionals.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<BaseResponseModel<BookingDetailsModel>> {
                override fun onSubscribe(d: Disposable) {

                }

                override fun onNext(baseResponseBody: BaseResponseModel<BookingDetailsModel>) {
                    bookingAcceptModel.postValue(baseResponseBody)
                }

                override fun onError(e: Throwable) {
                    bookingAcceptModel.postValue(null)
                }

                override fun onComplete() {

                }
            })
    }

    // Notification home Repository
    fun getNotificationHomeData(): MutableLiveData<BaseResponseModel<NotificationModel>> {
        return notificationHomeModel
    }

    fun setNotificationHomeData(token: String) {
        val functionals =
            ApiService.getRestService().notificationHomeData("application/json", "Token " + token)
        functionals.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<BaseResponseModel<NotificationModel>> {
                override fun onSubscribe(d: Disposable) {

                }

                override fun onNext(baseResponseBody: BaseResponseModel<NotificationModel>) {
                    notificationHomeModel.postValue(baseResponseBody)
                }

                override fun onError(e: Throwable) {
                    notificationHomeModel.postValue(null)
                }

                override fun onComplete() {

                }
            })
    }

    // Reviews List model
    fun getReviewList(): MutableLiveData<BaseResponseArrayModel<WorkerReviewModel>> {
        return reviewListModel
    }

    fun setReviewList(token: String, id: Int) {

        val functionals =
            ApiService.getRestService().reviewList("application/json", "Token " + token, id)
        functionals.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<BaseResponseArrayModel<WorkerReviewModel>> {
                override fun onSubscribe(d: Disposable) {

                }

                override fun onNext(baseResponseBody: BaseResponseArrayModel<WorkerReviewModel>) {
                    reviewListModel.postValue(baseResponseBody)
                }

                override fun onError(e: Throwable) {
                    reviewListModel.postValue(null)
                }

                override fun onComplete() {

                }
            })
    }

    // Notification List Repository
    fun getNotificationData(): MutableLiveData<BaseResponseArrayModel<NotificationModel>> {
        return notificationModel
    }

    fun setNotificationData(token: String) {
        val functionals =
            ApiService.getRestService().notificationData("application/json", "Token " + token)
        functionals.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<BaseResponseArrayModel<NotificationModel>> {
                override fun onSubscribe(d: Disposable) {

                }

                override fun onNext(baseResponseBody: BaseResponseArrayModel<NotificationModel>) {
                    notificationModel.postValue(baseResponseBody)
                }

                override fun onError(e: Throwable) {
                    notificationModel.postValue(null)
                }

                override fun onComplete() {

                }
            })
    }

    // Notification List Repository
    fun getNotificationDataClear(): MutableLiveData<BaseResponseModel<NotificationModel>> {
        return notificationClearModel
    }

    fun setNotificationDataClear(token: String,id : String) {

        val jsonData = JsonObject()
        jsonData.addProperty("notifications", id)

        val functionals =
            ApiService.getRestService().notificationClear("application/json", "Token " + token,jsonData)
        functionals.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<BaseResponseModel<NotificationModel>> {
                override fun onSubscribe(d: Disposable) {

                }

                override fun onNext(baseResponseBody: BaseResponseModel<NotificationModel>) {
                    notificationClearModel.postValue(baseResponseBody)
                }

                override fun onError(e: Throwable) {
                    notificationClearModel.postValue(null)
                }

                override fun onComplete() {

                }
            })
    }

    // Static page Repository
    fun getStaticPageRespnse(): MutableLiveData<BaseResponseModel<StaticPageModel>> {
        return staticPageModel
    }

    fun setStaticPage(token: String, query: String) {
        val functionals =
            ApiService.getRestService().staticPage("application/json", "Token " + token, query)
        functionals.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<BaseResponseModel<StaticPageModel>> {
                override fun onSubscribe(d: Disposable) {

                }

                override fun onNext(baseResponseBody: BaseResponseModel<StaticPageModel>) {
                    staticPageModel.postValue(baseResponseBody)
                }

                override fun onError(e: Throwable) {
                    staticPageModel.postValue(null)
                }

                override fun onComplete() {

                }
            })
    }

    // PROFILE UPDATE REPOSITORY
    fun getUpdateProfile(): MutableLiveData<BaseResponseModel<ProfileModel<AddressProfile>>> {
        return updateProfileModel
    }

    fun setUpdateProfilePersonal(
        name: String,
        email: String,
        age: String,
        phone: String,
        address: String,
        jobType: String,
        gender: String,
        token: String,
        userId: Int,
        latitude: Double,
        longitude: Double
    ) {
        val jsonData = JsonObject()
        jsonData.addProperty("username", name)
        jsonData.addProperty("email", email)
        jsonData.addProperty("gender", gender)
        jsonData.addProperty("age", age)
        jsonData.addProperty("jobcategories",jobType)
        jsonData.addProperty("phone_number", phone)
        jsonData.addProperty("address[address]", address)
        jsonData.addProperty("address[zip_code]", "")
        jsonData.addProperty("address[city]", "")
        jsonData.addProperty("address[state]", "")
        jsonData.addProperty("address[country]", "")
        jsonData.addProperty("address[latitude]", latitude)
        jsonData.addProperty("address[longitude]", longitude)

        val functionals = ApiService.getRestService()
            .updateWorkerProfile("application/json", "Token " + token, userId, jsonData)
        functionals.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<BaseResponseModel<ProfileModel<AddressProfile>>> {
                override fun onSubscribe(d: Disposable) {

                }

                override fun onNext(baseResponseBody: BaseResponseModel<ProfileModel<AddressProfile>>) {
                    updateProfileModel.postValue(baseResponseBody)
                }

                override fun onError(e: Throwable) {
                    updateProfileModel.postValue(null)

                }

                override fun onComplete() {

                }
            })
    }

    fun setUpdateProfileBank(
        token: String,
        userId: Int,
        bankHolderName: String,
        bankName: String,
        accountNumber: String
    ) {
        val jsonData = JsonObject()

        jsonData.addProperty("bank_detail[holder_name]", bankHolderName)
        jsonData.addProperty("bank_detail[bank_name]", bankName)
        jsonData.addProperty("bank_detail[account_number]", accountNumber)


        val functionals = ApiService.getRestService()
            .updateWorkerProfile("application/json", "Token " + token, userId, jsonData)
        functionals.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<BaseResponseModel<ProfileModel<AddressProfile>>> {
                override fun onSubscribe(d: Disposable) {

                }

                override fun onNext(baseResponseBody: BaseResponseModel<ProfileModel<AddressProfile>>) {
                    updateProfileModel.postValue(baseResponseBody)
                }

                override fun onError(e: Throwable) {
                    updateProfileModel.postValue(null)

                }

                override fun onComplete() {

                }
            })
    }


    fun setUpdateProfileOther(
        token: String,
        userId: Int,
        body: MultipartBody

    ) {
        val call = ApiService.getRestService()
            .updateProfilePictureDoucument("Token " + token, userId, body)
        call.enqueue(object : Callback<BaseResponseModel<ProfileModel<AddressProfile>>> {
            override fun onResponse(
                call: Call<BaseResponseModel<ProfileModel<AddressProfile>>>,
                response: Response<BaseResponseModel<ProfileModel<AddressProfile>>>
            ) {
                updateProfileModel.postValue(response.body())
            }

            override fun onFailure(call: Call<BaseResponseModel<ProfileModel<AddressProfile>>>, t: Throwable) {
                updateProfileModel.postValue(null)
                t.printStackTrace()
            }
        })
    }



    // Estimation Booking Repository
    fun getEstimationData(): MutableLiveData<BaseResponseModel<UpdateProfileMdel>> {
        return estimationModel
    }

    fun setEstimationData(
        token: String,
        id: Int,
        detailsWork: String,
        time: String,
        cost: String
    ) {
        val jsonData = JsonObject()
        jsonData.addProperty("additional_charge", cost)
        jsonData.addProperty("additional_charge_desc", detailsWork)
        jsonData.addProperty("time_duration", time)
        jsonData.addProperty("estimated", true)
        val functionals =
            ApiService.getRestService().estimationData("application/json", "Token " + token, id,jsonData)
        functionals.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<BaseResponseModel<UpdateProfileMdel>> {
                override fun onSubscribe(d: Disposable) {

                }

                override fun onNext(baseResponseBody: BaseResponseModel<UpdateProfileMdel>) {
                    estimationModel.postValue(baseResponseBody)
                }

                override fun onError(e: Throwable) {
                    estimationModel.postValue(null)
                }

                override fun onComplete() {

                }
            })
    }

    // Device Update Repository
    fun getDeviceInfo(): MutableLiveData<BaseResponseModel<ForgotPasswardResponseModel>> {
        return updateInfoModel
    }

    fun setDeviceInfo(fcmToken: String, token: String, deviceId: String) {
        val jsonData = JsonObject()
        jsonData.addProperty("registration_id", fcmToken)
        jsonData.addProperty("type", "android")
        jsonData.addProperty("device_id", deviceId)

        val functionals = ApiService.getRestService()
            .updateInfoDevice("application/json", "Token " + token, jsonData)

        functionals.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<BaseResponseModel<ForgotPasswardResponseModel>> {
                override fun onSubscribe(d: Disposable) {

                }

                override fun onNext(baseResponseBody: BaseResponseModel<ForgotPasswardResponseModel>) {
                    updateInfoModel.postValue(baseResponseBody)
                }

                override fun onError(e: Throwable) {
                    updateInfoModel.postValue(null)
                }

                override fun onComplete() {

                }
            })
    }

    fun getUploadEstimatinPicture(): MutableLiveData<BaseResponseModel<UpdateProfileMdel>> {
        return updateProfilePicture
    }

    fun setUploadEstimation(
        token: String,
        id: Int,
        body: MultipartBody
    ) {
        val call = ApiService.getRestService().uploadEstimationPicture("Token " + token, id, body)
        call.enqueue(object : Callback<BaseResponseModel<UpdateProfileMdel>> {
            override fun onResponse(
                call: Call<BaseResponseModel<UpdateProfileMdel>>,
                response: Response<BaseResponseModel<UpdateProfileMdel>>
            ) {
                updateProfilePicture.postValue(response.body())
            }

            override fun onFailure(call: Call<BaseResponseModel<UpdateProfileMdel>>, t: Throwable) {
                updateProfilePicture.postValue(null)
                t.printStackTrace()
            }
        })
    }
// End


    // Recieved Ratings Repository
    fun getRecievedRatings(): MutableLiveData<BaseResponseArrayModel<SubmitReviewModel>> {
        return recievedRatingsModel
    }

    fun setRecievedRatings(token: String) {
        val functionals =
            ApiService.getRestService().recievedRatings("application/json", "Token " + token)

        functionals.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<BaseResponseArrayModel<SubmitReviewModel>> {
                override fun onSubscribe(d: Disposable) {

                }

                override fun onNext(baseResponseBody: BaseResponseArrayModel<SubmitReviewModel>) {
                    recievedRatingsModel.postValue(baseResponseBody)
                }

                override fun onError(e: Throwable) {
                    recievedRatingsModel.postValue(null)
                }

                override fun onComplete() {

                }
            })
    }

    // send Ratings Repository
    fun getSendRatings(): MutableLiveData<BaseResponseArrayModel<SubmitReviewModel>> {
        return sendRatingsModel
    }

    fun setSendRatings(token: String) {
        val functionals =
            ApiService.getRestService().givenRatings("application/json", "Token " + token)

        functionals.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<BaseResponseArrayModel<SubmitReviewModel>> {
                override fun onSubscribe(d: Disposable) {

                }

                override fun onNext(baseResponseBody: BaseResponseArrayModel<SubmitReviewModel>) {
                    sendRatingsModel.postValue(baseResponseBody)
                }

                override fun onError(e: Throwable) {
                    sendRatingsModel.postValue(null)
                }

                override fun onComplete() {

                }
            })
    }

    // Payment history
    fun getPaymentHistory(): MutableLiveData<BaseResponseArrayModel<PaymentHistoryModel>> {
        return paymentHistoryModel
    }

    fun setPaymentHistory(token: String) {
        val functionals = ApiService
            .getRestService().paymentHistory("application/json", "Token " + token)
        functionals.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<BaseResponseArrayModel<PaymentHistoryModel>> {
                override fun onSubscribe(d: Disposable) {

                }

                override fun onNext(baseResponseBody: BaseResponseArrayModel<PaymentHistoryModel>) {
                    paymentHistoryModel.postValue(baseResponseBody)
                }

                override fun onError(e: Throwable) {
                    paymentHistoryModel.postValue(null)
                }

                override fun onComplete() {

                }
            })
    }

    // card add  Repository
    fun getCardResponse(): MutableLiveData<BaseResponseModel<AddCardResponseModel>> {
        return addCardResponeModel
    }

    fun setCardData(
        idUser: Int,
        token: String,
        cardNumber: String,
        cardName: String,
        monthExpiry: Int,
        yearExpiry: Int
    ) {
        val jsonData = JsonObject()
        jsonData.addProperty("user", idUser)
        jsonData.addProperty("card_number", cardNumber)
        jsonData.addProperty("card_name", cardName)
        jsonData.addProperty("expiry_month", monthExpiry)
        jsonData.addProperty("expiry_year", yearExpiry)

        val functionals = ApiService.getRestService()
            .addCardDetails("application/json", "Token " + token, jsonData)

        functionals.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<BaseResponseModel<AddCardResponseModel>> {
                override fun onSubscribe(d: Disposable) {

                }

                override fun onNext(baseResponseBody: BaseResponseModel<AddCardResponseModel>) {
                    addCardResponeModel.postValue(baseResponseBody)
                }

                override fun onError(e: Throwable) {
                    addCardResponeModel.postValue(null)
                }

                override fun onComplete() {

                }
            })
    }

    // card List  Repository
    fun getCardListResponse(): MutableLiveData<BaseResponseArrayModel<AddCardResponseModel>> {
        return cardListResponeModel
    }

    fun setCardListData(token: String) {

        val functionals = ApiService.getRestService().cardList("application/json", "Token " + token)

        functionals.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<BaseResponseArrayModel<AddCardResponseModel>> {
                override fun onSubscribe(d: Disposable) {

                }

                override fun onNext(baseResponseBody: BaseResponseArrayModel<AddCardResponseModel>) {
                    cardListResponeModel.postValue(baseResponseBody)
                }

                override fun onError(e: Throwable) {
                    cardListResponeModel.postValue(null)
                }

                override fun onComplete() {

                }
            })
    }

    // Social login Repository
    fun getSocialLogin(): MutableLiveData<BaseResponseModel<LoginResponseModel>> {
        return socialLogin
    }

    fun setSocialLogin(
        email: String,
        profileImage: String,
        username: String,
        uid: String,
        provider: String
    ) {
        val jsonData = JsonObject()
        jsonData.addProperty("email", email)
        jsonData.addProperty("profile_image", profileImage)
        jsonData.addProperty("username", username)
        jsonData.addProperty("uid", uid)
        jsonData.addProperty("provider", provider)

        val functionals = ApiService.getRestService()
            .socailLogin("application/json", jsonData)

        functionals.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<BaseResponseModel<LoginResponseModel>> {
                override fun onSubscribe(d: Disposable) {

                }

                override fun onNext(baseResponseBody: BaseResponseModel<LoginResponseModel>) {
                    socialLogin.postValue(baseResponseBody)
                }

                override fun onError(e: Throwable) {
                    socialLogin.postValue(null)
                }

                override fun onComplete() {

                }
            })
    }

    // offer  Repository
    fun getUserOffer(): MutableLiveData<BaseResponseArrayModel<OfferModel>> {
        return userOfferModel
    }

    fun setUserOffer(token: String) {
        val functionals =
            ApiService.getRestService().userOffer("application/json", "Token " + token)

        functionals.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<BaseResponseArrayModel<OfferModel>> {
                override fun onSubscribe(d: Disposable) {

                }

                override fun onNext(baseResponseBody: BaseResponseArrayModel<OfferModel>) {
                    userOfferModel.postValue(baseResponseBody)
                }

                override fun onError(e: Throwable) {
                    userOfferModel.postValue(null)
                }

                override fun onComplete() {

                }
            })
    }

    // Review submit Repository
    fun getReviewData(): MutableLiveData<BaseResponseModel<MyBookingModel>> {
        return submitReivewModel
    }

    fun setReviewData(
        token: String,
        workerId: Int,
        userID: Int,
        ratings: String,
        comment: String,
        appComments: String,
        prefrenceSelecting: Int
    ) {
        val jsonData = JsonObject()
        jsonData.addProperty("worker", workerId)
        jsonData.addProperty("user", userID)
        jsonData.addProperty("rating", ratings)
        jsonData.addProperty("comment", comment)
        jsonData.addProperty("app_rating", prefrenceSelecting)
        jsonData.addProperty("app_comment", appComments)
        jsonData.addProperty("given_by","WORKER")

        val functionals = ApiService.getRestService()
            .submitReview("application/json", "Token " + token, jsonData)
        functionals.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<BaseResponseModel<MyBookingModel>> {
                override fun onSubscribe(d: Disposable) {

                }

                override fun onNext(baseResponseBody: BaseResponseModel<MyBookingModel>) {
                    submitReivewModel.postValue(baseResponseBody)
                }

                override fun onError(e: Throwable) {
                    submitReivewModel.postValue(null)
                }

                override fun onComplete() {

                }
            })
    }


    /* // Static page Repository
     fun getStaticPageRespnse(): MutableLiveData<BaseResponseModel<StaticPageModel>> {
         return staticPageModel
     }

     fun setStaticPage(token: String, s: String) {
         val functionals =
             ApiService.getRestService().staticPage("application/json", "Token " + token, s)
         functionals.subscribeOn(Schedulers.io())
             .observeOn(AndroidSchedulers.mainThread())
             .subscribe(object : Observer<BaseResponseModel<StaticPageModel>> {
                 override fun onSubscribe(d: Disposable) {

                 }

                 override fun onNext(baseResponseBody: BaseResponseModel<StaticPageModel>) {
                     staticPageModel.postValue(baseResponseBody)
                 }

                 override fun onError(e: Throwable) {
                     staticPageModel.postValue(null)
                 }

                 override fun onComplete() {

                 }
             })
     }

     // Primary Card Repository
     fun getPrimaryCard(): MutableLiveData<BaseResponseModel<AddCardResponseModel>> {
         return primaryCardGetModel
     }

     fun setPrimaryCard(token: String) {
         val functionals =
             ApiService.getRestService().primaryCard("application/json", "Token " + token)
         functionals.subscribeOn(Schedulers.io())
             .observeOn(AndroidSchedulers.mainThread())
             .subscribe(object : Observer<BaseResponseModel<AddCardResponseModel>> {
                 override fun onSubscribe(d: Disposable) {

                 }

                 override fun onNext(baseResponseBody: BaseResponseModel<AddCardResponseModel>) {
                     primaryCardGetModel.postValue(baseResponseBody)
                 }

                 override fun onError(e: Throwable) {
                     primaryCardGetModel.postValue(null)
                 }

                 override fun onComplete() {

                 }
             })
     }

     // Set primary card Repository
     fun getSavePrimaryCard(): MutableLiveData<BaseResponseModel<AddCardResponseModel>> {
         return primaryCardSetModel
     }

     fun setSavePrimaryCard(token: String, cardID: Int) {
         val functionals =
             ApiService.getRestService().setPrimaryCard("application/json", "Token " + token, cardID)
         functionals.subscribeOn(Schedulers.io())
             .observeOn(AndroidSchedulers.mainThread())
             .subscribe(object : Observer<BaseResponseModel<AddCardResponseModel>> {
                 override fun onSubscribe(d: Disposable) {

                 }

                 override fun onNext(baseResponseBody: BaseResponseModel<AddCardResponseModel>) {
                     primaryCardSetModel.postValue(baseResponseBody)
                 }

                 override fun onError(e: Throwable) {
                     primaryCardSetModel.postValue(null)
                 }

                 override fun onComplete() {

                 }
             })
     }

     // Set payment Repository
     fun getPaymentToken(): MutableLiveData<BaseResponseModel<PaymentTokenModel>> {
         return paymentTokenModel
     }

     fun setPaymentToken(token: String, cvv: String, amount: Double, cardId: Int) {

         val jsonData = JsonObject()
         jsonData.addProperty("cvv", cvv)
         jsonData.addProperty("amount", amount)
         jsonData.addProperty("card_id", cardId)

         val functionals =
             ApiService.getRestService().paymentToken("application/json", "Token " + token, jsonData)
         functionals.subscribeOn(Schedulers.io())
             .observeOn(AndroidSchedulers.mainThread())
             .subscribe(object : Observer<BaseResponseModel<PaymentTokenModel>> {
                 override fun onSubscribe(d: Disposable) {

                 }

                 override fun onNext(baseResponseBody: BaseResponseModel<PaymentTokenModel>) {
                     paymentTokenModel.postValue(baseResponseBody)
                 }

                 override fun onError(e: Throwable) {
                     paymentTokenModel.postValue(null)
                 }

                 override fun onComplete() {

                 }
             })
     }

     // Set Payment pay Repository
     fun getPaymentData(): MutableLiveData<BaseResponseModel<PaymentTokenModel>> {
         return paymentTokenModel
     }

     fun setPaymentData(
         token: String,
         otp: String,
         tokenPayment: String,
         userID: Int,
         price: Double,
         workerId: Int,
         address: String,
         time: String,
         date: String,
         where: String,
         categoryId: Int,
         lat: Double,
         lng: Double,
         amount: Double,
         cardID: Int
     ) {

         val jsonData = JsonObject()
         jsonData.addProperty("token", tokenPayment)
         jsonData.addProperty("otp", otp)
         jsonData.addProperty("card", cardID)
         jsonData.addProperty("amount", amount)
         jsonData.addProperty("user", userID)
         jsonData.addProperty("worker", workerId)
         jsonData.addProperty("job_category", categoryId)
         jsonData.addProperty("where", where)
         jsonData.addProperty("visit_address", address)
         jsonData.addProperty("latitude", lat)
         jsonData.addProperty("longitude", lng)
         jsonData.addProperty("appointment_date", date)
         jsonData.addProperty("appointment_time", time)
         jsonData.addProperty("payment_method", "CARD")

         val functionals =
             ApiService.getRestService().paymentPay("application/json", "Token " + token, jsonData)
         functionals.subscribeOn(Schedulers.io())
             .observeOn(AndroidSchedulers.mainThread())
             .subscribe(object : Observer<BaseResponseModel<PaymentTokenModel>> {
                 override fun onSubscribe(d: Disposable) {

                 }

                 override fun onNext(baseResponseBody: BaseResponseModel<PaymentTokenModel>) {
                     paymentTokenModel.postValue(baseResponseBody)
                 }

                 override fun onError(e: Throwable) {
                     paymentTokenModel.postValue(null)
                 }

                 override fun onComplete() {

                 }
             })
     }

     // Set Delete card Repository
     fun getDeleteCard(): MutableLiveData<BaseResponseModel<AddCardResponseModel>> {
         return deleteCardModel
     }

     fun setDeleteCard(token: String, cardID: Int) {
         val functionals =
             ApiService.getRestService().deleteCard("application/json", "Token " + token, cardID)
         functionals.subscribeOn(Schedulers.io())
             .observeOn(AndroidSchedulers.mainThread())
             .subscribe(object : Observer<BaseResponseModel<AddCardResponseModel>> {
                 override fun onSubscribe(d: Disposable) {

                 }

                 override fun onNext(baseResponseBody: BaseResponseModel<AddCardResponseModel>) {
                     deleteCardModel.postValue(baseResponseBody)
                 }

                 override fun onError(e: Throwable) {
                     deleteCardModel.postValue(null)
                 }

                 override fun onComplete() {

                 }
             })
     }

     // Payment history
     fun getPaymentHistory(): MutableLiveData<BaseResponseArrayModel<PaymentHistoryModel>> {
         return paymentHistoryModel
     }

     fun setPaymentHistory(token: String) {
         val functionals = ApiService
             .getRestService().paymentHistory("application/json", "Token " + token)
         functionals.subscribeOn(Schedulers.io())
             .observeOn(AndroidSchedulers.mainThread())
             .subscribe(object : Observer<BaseResponseArrayModel<PaymentHistoryModel>> {
                 override fun onSubscribe(d: Disposable) {

                 }

                 override fun onNext(baseResponseBody: BaseResponseArrayModel<PaymentHistoryModel>) {
                     paymentHistoryModel.postValue(baseResponseBody)
                 }

                 override fun onError(e: Throwable) {
                     paymentHistoryModel.postValue(null)
                 }

                 override fun onComplete() {

                 }
             })
     }
 */
/*enf of class*/
}
