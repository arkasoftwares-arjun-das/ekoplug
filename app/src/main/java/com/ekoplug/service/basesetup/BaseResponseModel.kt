package com.ekoplug.service.basesetup

import java.io.Serializable

class BaseResponseModel<T> : Serializable {
    private var message: String =""
    private var error_message : String=""
    private var code: Int = 0
    private var data: T? = null


    fun getMessage(): String {
        return message
    }

    fun setMessage(message: String) {
        this.message = message
    }

    fun getData(): T? {
       return data
    }

    fun setData(data: T) {
        this.data = data
    }

    fun setErrorMessage(error_message : String){
        this.error_message = error_message
    }

    fun getErrorMessage():String{
        return error_message
    }

    fun setCode(code: Int){
        this.code = code
    }

    fun getCode():Int
    {
        return code
    }

}