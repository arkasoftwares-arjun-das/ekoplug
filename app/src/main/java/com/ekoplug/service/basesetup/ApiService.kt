package com.ekoplug.service.basesetup


import androidx.databinding.library.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class ApiService {

   companion object {
       private val CONNECTION_TIME = 180
//       const val BASE_URL = "http://192.168.2.217:8000/api/v1/"
       const val BASE_URL = "https://ekopluggroup.com/api/v1/"
       fun getRestService(): ApiInterface {
           val okHttpClient = getOkHttpClint()

           return Retrofit.Builder()
               .baseUrl(BASE_URL)
               .client(okHttpClient)
               .addConverterFactory(GsonConverterFactory.create())
               .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
               .build().create(ApiInterface::class.java)
       }


       fun getOkHttpClint(): OkHttpClient {
           val builder = OkHttpClient.Builder()

           if (BuildConfig.DEBUG) {
               val loggingInterceptor = HttpLoggingInterceptor()
               loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
               builder.addInterceptor(loggingInterceptor)

           }

           builder.connectTimeout(CONNECTION_TIME.toLong(), TimeUnit.SECONDS)
           builder.readTimeout(CONNECTION_TIME.toLong(), TimeUnit.SECONDS)
           builder.writeTimeout(CONNECTION_TIME.toLong(), TimeUnit.SECONDS)

           return builder.build()
       }
   }
}