package com.ekoplug.service.basesetup;

import com.ekoplug.model.*
import com.ekoplug.service.model.*
import com.google.gson.JsonObject
import io.reactivex.Observable
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.http.*
import java.util.*

interface ApiInterface {

    @POST("workers/create")
    fun signUpApi(
        @Body requestBody: MultipartBody
    ): Call<BaseResponseModel<SignupResponseModel>>

    @POST("auth/validate-otp")
    fun otpVerify(
        @Header("Content-Type") s: String,
        @Body body: JsonObject
    ): Observable<BaseResponseModel<OtpVerifyResponseModel>>

    @POST("auth/login")
    fun login(
        @Header("Content-Type") s: String,
        @Header("client-name") type: String,
        @Body body: JsonObject
    ): Observable<BaseResponseModel<LoginResponseModel>>

    @POST("auth/forget-password")
    fun setForgotPassword(
        @Header("Content-Type") s: String,
        @Header("client-name") type: String,
        @Body body: JsonObject
    ): Observable<BaseResponseModel<ForgotPasswardResponseModel>>

    @POST("auth/reset-password")
    fun changePassword(
        @Header("Content-Type") s: String,
        @Header("Authorization") token: String,
        @Body body: JsonObject
    ): Observable<BaseResponseModel<LoginResponseModel>>

    @GET("workers/job-category-list")
    fun categoryModel(
        @Header("Content-Type") s: String
    ): Observable<BaseResponseArrayModel<CategoryModel>>

    @GET("workers/home")
    fun homeApi(
        @Header("Content-Type") s: String,
        @Header("Authorization") token: String
    ): Call<BaseResponseModel<HomeModel>>

    @GET("workers/booking-request")
    fun bookingRequestData(
        @Header("Content-Type") s: String,
        @Header("Authorization") token: String
    ): Observable<BaseResponseArrayModel<BookingRequestModel>>

    @GET("workers/current-job")
    fun currentBookingData(
        @Header("Content-Type") s: String,
        @Header("Authorization") token: String
    ): Observable<BaseResponseArrayModel<BookingRequestModel>>

    @GET("workers/{id}/profile")
    fun profileData(
        @Header("Content-Type") s: String,
        @Header("Authorization") token: String,
        @Path("id") id: Int
    ): Observable<BaseResponseModel<ProfileModel<AddressProfile>>>

    @POST("auth/change-password")
    fun changeCurrentPassword(
        @Header("Content-Type") s: String,
        @Header("Authorization") token: String,
        @Body body: JsonObject
    ): Observable<BaseResponseModel<ProfileModel<AddressProfile>>>

    @POST("auth/resent-code")
    fun resentCode(
        @Header("Content-Type") s: String,
        @Body body: JsonObject
    ): Observable<BaseResponseModel<ForgotPasswardResponseModel>>

    @GET("workers/booking/{id}/detail")
    fun bookingDetails(
        @Header("Content-Type") s: String,
        @Header("Authorization") token: String,
        @Path("id") id: Int
    ): Observable<BaseResponseModel<BookingDetailsModel>>

    @GET("workers/notifications")
    fun notificationData(
        @Header("Content-Type") s: String,
        @Header("Authorization") token: String
    ): Observable<BaseResponseArrayModel<NotificationModel>>

    @GET("workers/home-notifications-list")
    fun notificationHomeData(
        @Header("Content-Type") s: String,
        @Header("Authorization") token: String
    ): Observable<BaseResponseModel<NotificationModel>>

    @GET("workers/upcoming-job")
    fun upcomingJob(
        @Header("Content-Type") s: String,
        @Header("Authorization") token: String
    ): Observable<BaseResponseArrayModel<MyBookingListModel>>

    @GET("workers/completed-job")
    fun completeJob(
        @Header("Content-Type") s: String,
        @Header("Authorization") token: String
    ): Observable<BaseResponseArrayModel<MyBookingListModel>>

    @POST("workers/booking/{id}/update")
    fun bookingAccept(
        @Header("Content-Type") s: String,
        @Header("Authorization") token: String,
        @Path("id") id: Int,
        @Body body: JsonObject
    ): Observable<BaseResponseModel<BookingDetailsModel>>

    @GET("workers//home-notifications-list")
    fun homeNotification(
        @Header("Content-Type") s: String,
        @Header("Authorization") token: String
    ): Observable<BaseResponseModel<NotificationModel>>

    @PUT("workers/booking/{id}/booking-reject")
    fun cancelRequest(
        @Header("Content-Type") s: String,
        @Header("Authorization") token: String,
        @Path("id") id: Int,
        @Body body: JsonObject
    ): Observable<BaseResponseModel<BookingDetailsModel>>

    @GET("workers/review/{id}/user")
    fun reviewList(
        @Header("Content-Type") s: String,
        @Header("Authorization") token: String,
        @Path("id") id: Int
    ): Observable<BaseResponseArrayModel<WorkerReviewModel>>

    @POST("workers/notifications-clear")
    fun notificationClear(
        @Header("Content-Type") s: String,
        @Header("Authorization") token: String,
        @Body body: JsonObject
    ): Observable<BaseResponseModel<NotificationModel>>

    @GET("static-pages")
    fun staticPage(
        @Header("Content-Type") s: String,
        @Header("Authorization") token: String,
        @Query("q") query: String
    ): Observable<BaseResponseModel<StaticPageModel>>

    @PUT("workers/{id}/profile/update")
    fun updateWorkerProfile(
        @Header("Content-Type") s: String,
        @Header("Authorization") token: String,
        @Path("id") id: Int,
        @Body body: JsonObject
    ): Observable<BaseResponseModel<ProfileModel<AddressProfile>>>

    @PUT("workers/{id}/profile/update")
    fun updateProfilePictureDoucument(
        @Header("Authorization") token: String,
        @Path("id") id: Int,
        @Body requestBody: MultipartBody
    ): Call<BaseResponseModel<ProfileModel<AddressProfile>>>


    @GET("workers/received-rating")
    fun recievedRatings(
        @Header("Content-Type") s: String,
        @Header("Authorization") token: String
    ): Observable<BaseResponseArrayModel<SubmitReviewModel>>

    @GET("workers/given-rating")
    fun givenRatings(
        @Header("Content-Type") s: String,
        @Header("Authorization") token: String
    ): Observable<BaseResponseArrayModel<SubmitReviewModel>>

    @PUT("workers/booking/{bookingID}/estimation")
    fun estimationData(
        @Header("Content-Type") s: String,
        @Header("Authorization") token: String,
        @Path("bookingID") bookingId: Int,
        @Body body: JsonObject
    ): Observable<BaseResponseModel<UpdateProfileMdel>>

    @POST("workers/{id}/profile-update")
    fun updateProfilePicture(
        @Header("Authorization") token: String,
        @Path("id") id: Int,
        @Body requestBody: MultipartBody
    ): Call<BaseResponseModel<UpdateProfileMdel>>

    @POST("update-device-info")
    fun updateInfoDevice(
        @Header("Content-Type") s: String,
        @Header("Authorization") token: String,
        @Body body: JsonObject
    ): Observable<BaseResponseModel<ForgotPasswardResponseModel>>

    @POST("workers/booking/booking-service-image/{id}")
    fun uploadEstimationPicture(
        @Header("Authorization") token: String,
        @Path("id") id: Int,
        @Body requestBody: MultipartBody
    ): Call<BaseResponseModel<UpdateProfileMdel>>

    @GET("workers/payment-history")
    fun paymentHistory(
        @Header("Content-Type") s: String,
        @Header("Authorization") token: String
    ): Observable<BaseResponseArrayModel<PaymentHistoryModel>>

    @POST("workers/rating/customer")
    fun submitReview(
        @Header("Content-Type") s: String,
        @Header("Authorization") token: String,
        @Body body: JsonObject
    ): Observable<BaseResponseModel<MyBookingModel>>

    /*   @GET("workers/notifications")
       fun notificationData(
           @Header("Content-Type") s: String,
           @Header("Authorization") token: String
       ): Observable<BaseResponseArrayModel<NotificationModel>>
   */

    // end
    @GET("users/review/{id}/worker")
    fun serviceProviderReview(
        @Header("Content-Type") s: String,
        @Path("id") id: Int
    ): Observable<BaseResponseArrayModel<WorkerReviewModel>>

    @PUT("users/{id}/profile/update")
    fun updateProfile(
        @Header("Content-Type") s: String,
        @Header("Authorization") token: String,
        @Path("id") id: Int,
        @Body body: JsonObject
    ): Observable<BaseResponseModel<UpdateProfileMdel>>

    @POST("users/booking/create")
    fun bookingRequest(
        @Header("Content-Type") s: String,
        @Header("Authorization") token: String,
        @Body body: JsonObject
    ): Observable<BaseResponseModel<BookingRequestModel>>

    @GET("users/booking/my-bookings")
    fun bookingData(
        @Header("Content-Type") s: String,
        @Header("Authorization") token: String
    ): Observable<BaseResponseArrayModel<MyBookingModel>>

    @GET("users/booking/completed")
    fun completeBookingData(
        @Header("Content-Type") s: String,
        @Header("Authorization") token: String
    ): Observable<BaseResponseArrayModel<MyBookingModel>>

    @GET("booking/{detailsId}/detail")
    fun myBookingDetailsData(
        @Header("Content-Type") s: String,
        @Header("Authorization") token: String,
        @Path("detailsId") detailsId: Int
    ): Observable<BaseResponseModel<MyBookingDetailsModel>>

    @PUT("users/booking/{id}/cancel")
    fun cancelBooking(
        @Header("Content-Type") s: String,
        @Header("Authorization") token: String,
        @Path("id") id: Int,
        @Body body: JsonObject
    ): Observable<BaseResponseModel<MyBookingModel>>

    //Payment history
//    @GET("update-device-info")
//    fun paymentHistory(
//        @Header("Content-Type") s: String,
//        @Header("Authorization") token: String
//    ): Observable<BaseResponseArrayModel<PaymentHistoryModel>>


    @GET("users/user-offers")
    fun userOffer(
        @Header("Content-Type") s: String,
        @Header("Authorization") token: String
    ): Observable<BaseResponseArrayModel<OfferModel>>

    @POST("payment/card-add")
    fun addCardDetails(
        @Header("Content-Type") s: String,
        @Header("Authorization") token: String,
        @Body body: JsonObject
    ): Observable<BaseResponseModel<AddCardResponseModel>>

    @GET("payment/card-list")
    fun cardList(
        @Header("Content-Type") s: String,
        @Header("Authorization") token: String
    ): Observable<BaseResponseArrayModel<AddCardResponseModel>>

    @POST("users/auth/social-login")
    fun socailLogin(
        @Header("Content-Type") s: String,
        @Body body: JsonObject
    ): Observable<BaseResponseModel<LoginResponseModel>>



}