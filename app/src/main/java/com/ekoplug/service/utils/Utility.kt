package com.ekoplug.utils


import android.Manifest
import android.app.Activity
import android.app.PendingIntent
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.location.Address
import android.location.Geocoder
import android.location.LocationManager
import android.net.ConnectivityManager
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentTransaction
import com.ekoplug.service.R
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.material.snackbar.Snackbar
import java.text.SimpleDateFormat
import java.util.*


object Utility {

    var imageURL = "https://ekopluggroup.com"

    fun hideSoftKeyboard(activity: Activity) {
         val inputMethodManager =
             activity.getSystemService(
                 Activity.INPUT_METHOD_SERVICE
             ) as InputMethodManager
         inputMethodManager.hideSoftInputFromWindow(
             activity.currentFocus!!.windowToken, 0
         )
    }
    fun hideKeyboard(mContext: Context, editText: EditText?) {
        val imm =
            mContext.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT)
    }
    fun showSnackBar(view: View, message: String, context: Context) {
        val snackBar = Snackbar.make(
            view, message, Snackbar.LENGTH_LONG
        )
        snackBar.setBackgroundTint(ContextCompat.getColor(context, R.color.colorAccent))
        snackBar.setTextColor(ContextCompat.getColor(context, R.color.white))
        snackBar.show()
    }

    fun isNetworkConnected(mContext: Context): Boolean {
        val connectivityManager =
            mContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
        val activeNetworkInfo = connectivityManager!!.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }

    /*fun navigateToFragmentWithBackStack(ctx: FragmentActivity?, fragment: Fragment) {
        val fragmentManager = ctx!!.supportFragmentManager
        val fragmentTransaction: FragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.add(R.id.frameLayout, fragment)
        fragmentTransaction.addToBackStack(null)
        fragmentTransaction.commit()
    }

    fun navigateToFragmentWithBackStackArguments(
        ctx: FragmentActivity?,
        fragment: Fragment,
        key: String,
        value: String,
        key1: String,
        value1: String
    ) {
        val args = Bundle()
        args.putString(key, value)
        args.putString(key1, value1)
        fragment.arguments = args
        val fragmentManager = ctx!!.supportFragmentManager
        val fragmentTransaction: FragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.add(R.id.frameLayout, fragment)
        fragmentTransaction.addToBackStack(null)
        fragmentTransaction.commit()
    }

    fun navigateToFragmentWithBackStackArgumentsIntStrin(
        ctx: FragmentActivity?,
        fragment: Fragment,
        key: String,
        value: Int,
        key1: String,
        value1: String
    ) {
        val args = Bundle()
        args.putInt(key, value)
        args.putString(key1, value1)
        fragment.arguments = args
        val fragmentManager = ctx!!.supportFragmentManager
        val fragmentTransaction: FragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.add(R.id.frameLayout, fragment)
        fragmentTransaction.addToBackStack(null)
        fragmentTransaction.commit()
    }

    fun navigateToFragmentWithBackStackArgumentsInt(
        ctx: FragmentActivity?,
        fragment: Fragment,
        key: String,
        value: Int
    ) {
        val args = Bundle()
        args.putInt(key, value)
        fragment.arguments = args
        val fragmentManager = ctx!!.supportFragmentManager
        val fragmentTransaction: FragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.add(R.id.frameLayout, fragment)
        fragmentTransaction.addToBackStack(null)
        fragmentTransaction.commit()
    }

    fun navigateToFragmentWithBackStackArgumentsOne(
        ctx: FragmentActivity?,
        fragment: Fragment,
        key: String,
        value: String
    ) {
        val args = Bundle()
        args.putString(key, value)
        fragment.arguments = args
        val fragmentManager = ctx!!.supportFragmentManager
        val fragmentTransaction: FragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.add(R.id.frameLayout, fragment)
        fragmentTransaction.addToBackStack(null)
        fragmentTransaction.commit()
    }
*/
    fun bitmapDescriptorFromVector(context: Context?, vectorResId: Int): BitmapDescriptor? {
        val vectorDrawable = ContextCompat.getDrawable(context!!, vectorResId)
        vectorDrawable!!.setBounds(
            0,
            0,
            vectorDrawable.intrinsicWidth,
            vectorDrawable.intrinsicHeight
        )
        val bitmap = Bitmap.createBitmap(
            vectorDrawable.intrinsicWidth,
            vectorDrawable.intrinsicHeight,
            Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(bitmap)
        vectorDrawable.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }

    fun checkPermissions(context: Context): Boolean {
        if (ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            return true
        }
        return false
    }

    fun requestPermissions(activity: Activity) {
        ActivityCompat.requestPermissions(
            activity,
            arrayOf(
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
            ),
            101
        )
    }

    fun checkGPS(activity: Activity): Boolean {
        var locationManager: LocationManager =
            activity.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        var gps_enabled = false

        try {
            gps_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
        } catch (e: Exception) {
        }
        return gps_enabled
    }

    fun getCompleteAddressString(
        LATITUDE: Double, LONGITUDE: Double, ctx: Context
    ): String? {
        var strAdd = ""
        val geocoder = Geocoder(ctx, Locale.getDefault())
        try {
            val addresses: List<Address>? =
                geocoder.getFromLocation(LATITUDE, LONGITUDE, 1)
            if (addresses != null) {
                val returnedAddress: Address = addresses[0]
                val strReturnedAddress = StringBuilder("")
                for (i in 0..returnedAddress.maxAddressLineIndex) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n")
                }
                strAdd = strReturnedAddress.toString()
                Log.d("", strReturnedAddress.toString())
            } else {
                Log.d("address", "No Address returned!")
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
            Log.d("address", "Canont get Address!")
        }
        return strAdd
    }

}