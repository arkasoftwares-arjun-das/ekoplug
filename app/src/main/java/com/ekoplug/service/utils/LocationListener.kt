package com.ekoplug.utils

import com.google.android.gms.location.LocationResult

interface LocationListener {
    fun locationResponse(locationResult: LocationResult)
}
