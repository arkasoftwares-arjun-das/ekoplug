package com.ekoplug.service.fragments

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.*
import android.widget.RadioGroup
import android.widget.Toast
import androidx.annotation.Nullable
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.ekoplug.model.AddressProfile
import com.ekoplug.model.ProfileModel
import com.ekoplug.service.R
import com.ekoplug.service.adapters.JobTypeAdapter
import com.ekoplug.service.basesetup.BaseResponseArrayModel
import com.ekoplug.service.basesetup.BaseResponseModel
import com.ekoplug.service.databinding.DialogJobTypeBinding
import com.ekoplug.service.databinding.PersonalProfileFragmentBinding
import com.ekoplug.service.model.CategoryModel
import com.ekoplug.service.viewmodel.CategoryViewModel
import com.ekoplug.service.viewmodel.EditProfileViewModel
import com.ekoplug.service.viewmodel.ProfileViewModel
import com.ekoplug.utils.AppPreference
import com.ekoplug.utils.Utility
import com.ekoplug.utils.Watting
import com.google.android.gms.common.api.Status
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode

class PersonalProfileFragment : Fragment() {

    companion object {
        fun newInstance() = PersonalProfileFragment()
    }

    lateinit var personalProfileFragment: PersonalProfileFragmentBinding
    lateinit var updateViewModel: EditProfileViewModel
    private var mWaitingDialog: Watting? = null
    var AUTOCOMPLETE_REQUEST_CODE = 1010
    private lateinit var viewModel: ProfileViewModel
    var token = ""
    var userId = 0
    var gender = ""
    var latitude = 0.0
    var longitude = 0.0
    var jobArray = ArrayList<String>()
    var jobTypeArray = ArrayList<String>()
    lateinit var cateogryViewModel: CategoryViewModel
    var categoryData: ArrayList<CategoryModel>? = null
    var result = ""
    var resultId = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        personalProfileFragment =
            DataBindingUtil.inflate(inflater, R.layout.personal_profile_fragment, container, false)
        mWaitingDialog = Watting(requireContext(), "")
        token = AppPreference.getPreferenceValueByKey(requireContext(), "Token").toString()
        userId = AppPreference.getIntPreferenceValueByKey(requireContext(), "Id")!!
        callApi()
        init()
        viewModel.setProfileData(token, userId)
        cateogryViewModel.setCategoryResponse()
        return personalProfileFragment.root
    }

    private fun init() {
        Places.initialize(
            requireContext(),
            resources.getString(R.string.google_place_picker_key)
        )
        personalProfileFragment.etSignUpJobTypeProfile.setOnClickListener { v ->
            jobTypeArray.clear()
            jobArray.clear()
            val dialogBuilder = Dialog(requireContext())
            val inflater = LayoutInflater.from(requireContext())
            val binding: DialogJobTypeBinding = DataBindingUtil.inflate(
                inflater,
                R.layout.dialog_job_type,
                null,
                false
            )
            dialogBuilder.setContentView(binding.root)
            val mWindow = dialogBuilder.window
            mWindow!!.setLayout(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT
            )
            mWindow.setDimAmount(0.2f)
            mWindow.setGravity(Gravity.BOTTOM)
            val lp = WindowManager.LayoutParams()
            lp.copyFrom(mWindow.attributes)
            lp.width = WindowManager.LayoutParams.MATCH_PARENT
            mWindow.attributes = lp
            dialogBuilder.setCancelable(false)
            dialogBuilder.setCanceledOnTouchOutside(true)
            dialogBuilder.show()
            binding.rvLayoutDialog
            val layoutManager = LinearLayoutManager(requireContext())
            binding.rvLayoutDialog.layoutManager = layoutManager
            binding.rvLayoutDialog.setHasFixedSize(true)
            ViewCompat.setNestedScrollingEnabled(binding.rvLayoutDialog, false)
            val jobAdapter = JobTypeAdapter(requireContext(), categoryData) { id, value, checked ->

                if (checked.equals("check")) {
                    jobArray.add(id.toString())
                    jobTypeArray.add(value)
                } else {
                    jobArray.remove(id.toString())
                    jobTypeArray.remove(value)
                }
            }
            binding.rvLayoutDialog.adapter = jobAdapter
            binding.btnCancel.setOnClickListener { v ->
                dialogBuilder.dismiss()
            }
            binding.btnAccept.setOnClickListener { v ->
                System.out.println(jobTypeArray);
                if (!jobTypeArray.isEmpty()) {
                    resultId = jobArray.joinToString(",")
                    result = jobTypeArray.joinToString(", ")
                    personalProfileFragment.etSignUpJobTypeProfile.setText(result)
                }

                dialogBuilder.dismiss()
            }

        }
        personalProfileFragment.etSignUpNameProfile.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                personalProfileFragment.etSignUpNameProfile.setCompoundDrawablesWithIntrinsicBounds(
                    ContextCompat.getDrawable(
                        requireContext(),
                        R.drawable.profile_blue
                    ),
                    null,
                    null,
                    null
                )
                personalProfileFragment.etSignUpNameProfile.setBackgroundResource(R.drawable.login_textview_blue)
                personalProfileFragment.etSignUpNameProfile.setTextColor(resources.getColor(R.color.colorPrimary))
                personalProfileFragment.etSignUpNameProfile.setHintTextColor(resources.getColor(R.color.colorPrimary))

            } else {
                personalProfileFragment.etSignUpNameProfile.setCompoundDrawablesWithIntrinsicBounds(
                    ContextCompat.getDrawable(
                        requireContext(),
                        R.drawable.profile_grey
                    ),
                    null,
                    null,
                    null
                )
                personalProfileFragment.etSignUpNameProfile.setBackgroundResource(R.drawable.login_textview_grey)
                personalProfileFragment.etSignUpNameProfile.setTextColor(resources.getColor(R.color.grey))
                personalProfileFragment.etSignUpNameProfile.setHintTextColor(resources.getColor(R.color.grey))
            }

        }
        personalProfileFragment.etSignUpEmailProfile.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                personalProfileFragment.etSignUpEmailProfile.setCompoundDrawablesWithIntrinsicBounds(
                    ContextCompat.getDrawable(
                        requireContext(),
                        R.drawable.mail_blue_icon
                    ),
                    null,
                    null,
                    null
                )
                personalProfileFragment.etSignUpEmailProfile.setBackgroundResource(R.drawable.login_textview_blue)
                personalProfileFragment.etSignUpEmailProfile.setTextColor(resources.getColor(R.color.colorPrimary))
                personalProfileFragment.etSignUpEmailProfile.setHintTextColor(resources.getColor(R.color.colorPrimary))

            } else {
                personalProfileFragment.etSignUpEmailProfile.setCompoundDrawablesWithIntrinsicBounds(
                    ContextCompat.getDrawable(
                        requireContext(),
                        R.drawable.mail_grey_icon
                    ),
                    null,
                    null,
                    null
                )
                personalProfileFragment.etSignUpEmailProfile.setBackgroundResource(R.drawable.login_textview_grey)
                personalProfileFragment.etSignUpEmailProfile.setTextColor(resources.getColor(R.color.grey))
                personalProfileFragment.etSignUpEmailProfile.setHintTextColor(resources.getColor(R.color.grey))
            }
        }
        personalProfileFragment.etEditProfilePhone.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                personalProfileFragment.etEditProfilePhone.setCompoundDrawablesWithIntrinsicBounds(
                    ContextCompat.getDrawable(
                        requireContext(),
                        R.drawable.phone_blue
                    ),
                    null,
                    null,
                    null
                )
                personalProfileFragment.etEditProfilePhone.setBackgroundResource(R.drawable.login_textview_blue)
                personalProfileFragment.etEditProfilePhone.setTextColor(resources.getColor(R.color.colorPrimary))
                personalProfileFragment.etEditProfilePhone.setHintTextColor(resources.getColor(R.color.colorPrimary))

            } else {
                personalProfileFragment.etEditProfilePhone.setCompoundDrawablesWithIntrinsicBounds(
                    ContextCompat.getDrawable(
                        requireContext(),
                        R.drawable.phone_grey
                    ),
                    null,
                    null,
                    null
                )
                personalProfileFragment.etEditProfilePhone.setBackgroundResource(R.drawable.login_textview_grey)
                personalProfileFragment.etEditProfilePhone.setTextColor(resources.getColor(R.color.grey))
                personalProfileFragment.etEditProfilePhone.setHintTextColor(resources.getColor(R.color.grey))
            }
        }
        personalProfileFragment.etSignUpAgeProfile.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                personalProfileFragment.etSignUpAgeProfile.setCompoundDrawablesWithIntrinsicBounds(
                    ContextCompat.getDrawable(
                        requireContext(),
                        R.drawable.calendar_blue
                    ),
                    null,
                    null,
                    null
                )
                personalProfileFragment.etSignUpAgeProfile.setBackgroundResource(R.drawable.login_textview_blue)
                personalProfileFragment.etSignUpAgeProfile.setTextColor(resources.getColor(R.color.colorPrimary))
                personalProfileFragment.etSignUpAgeProfile.setHintTextColor(resources.getColor(R.color.colorPrimary))

            } else {
                personalProfileFragment.etSignUpAgeProfile.setCompoundDrawablesWithIntrinsicBounds(
                    ContextCompat.getDrawable(
                        requireContext(),
                        R.drawable.calendar_grey
                    ),
                    null,
                    null,
                    null
                )
                personalProfileFragment.etSignUpAgeProfile.setBackgroundResource(R.drawable.login_textview_grey)
                personalProfileFragment.etSignUpAgeProfile.setTextColor(resources.getColor(R.color.grey))
                personalProfileFragment.etSignUpAgeProfile.setHintTextColor(resources.getColor(R.color.grey))
            }
        }
        personalProfileFragment.etSignUpAddressProfile.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                personalProfileFragment.etSignUpAddressProfile.setCompoundDrawablesWithIntrinsicBounds(
                    ContextCompat.getDrawable(
                        requireContext(),
                        R.drawable.ic_location_blue
                    ),
                    null,
                    null,
                    null
                )
                personalProfileFragment.etSignUpAddressProfile.setBackgroundResource(R.drawable.login_textview_blue)
                personalProfileFragment.etSignUpAddressProfile.setTextColor(resources.getColor(R.color.colorPrimary))
                personalProfileFragment.etSignUpAddressProfile.setHintTextColor(resources.getColor(R.color.colorPrimary))

            } else {
                personalProfileFragment.etSignUpAddressProfile.setCompoundDrawablesWithIntrinsicBounds(
                    ContextCompat.getDrawable(
                        requireContext(),
                        R.drawable.ic_location_grey
                    ),
                    null,
                    null,
                    null
                )
                personalProfileFragment.etSignUpAddressProfile.setBackgroundResource(R.drawable.login_textview_grey)
                personalProfileFragment.etSignUpAddressProfile.setTextColor(resources.getColor(R.color.grey))
                personalProfileFragment.etSignUpAddressProfile.setHintTextColor(resources.getColor(R.color.grey))
            }
        }
        personalProfileFragment.etSignUpJobTypeProfile.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                personalProfileFragment.etSignUpJobTypeProfile.setCompoundDrawablesWithIntrinsicBounds(
                    ContextCompat.getDrawable(
                        requireContext(),
                        R.drawable.job_type_blue
                    ),
                    null,
                    null,
                    null
                )
                personalProfileFragment.etSignUpJobTypeProfile.setBackgroundResource(R.drawable.login_textview_blue)
                personalProfileFragment.etSignUpJobTypeProfile.setTextColor(resources.getColor(R.color.colorPrimary))
                personalProfileFragment.etSignUpJobTypeProfile.setHintTextColor(resources.getColor(R.color.colorPrimary))

            } else {
                personalProfileFragment.etSignUpJobTypeProfile.setCompoundDrawablesWithIntrinsicBounds(
                    ContextCompat.getDrawable(
                        requireContext(),
                        R.drawable.job_type_grey
                    ),
                    null,
                    null,
                    null
                )
                personalProfileFragment.etSignUpJobTypeProfile.setBackgroundResource(R.drawable.login_textview_grey)
                personalProfileFragment.etSignUpJobTypeProfile.setTextColor(resources.getColor(R.color.grey))
                personalProfileFragment.etSignUpJobTypeProfile.setHintTextColor(resources.getColor(R.color.grey))
            }
        }
        personalProfileFragment.etSignUpAddressProfile.setOnClickListener{v ->
            callAutoSearch()
        }
        personalProfileFragment.rgGenderProfile.setOnCheckedChangeListener(
            RadioGroup.OnCheckedChangeListener { group, checkedId ->
                when (checkedId) {
                    R.id.rbMale -> {
                        gender = "MALE"
                        personalProfileFragment.rbMaleProfile.setTextColor(resources.getColor(R.color.colorPrimary))
                        personalProfileFragment.rbFemaleProfile.setTextColor(resources.getColor(R.color.grey))
                    }
                    R.id.rbFemale -> {
                        gender = "FEMALE"
                        personalProfileFragment.rbMaleProfile.setTextColor(resources.getColor(R.color.grey))
                        personalProfileFragment.rbFemaleProfile.setTextColor(resources.getColor(R.color.colorPrimary))
                    }
                }
            })
        personalProfileFragment.btnSubmitProfile.setOnClickListener { v ->
            submitProfile()
        }
    }

    private fun submitProfile() {
        mWaitingDialog!!.show()
        updateViewModel.setUpdateProfilePersonal(
            personalProfileFragment.etSignUpNameProfile.text.toString(),
            personalProfileFragment.etSignUpEmailProfile.text.toString(),
            personalProfileFragment.etSignUpAgeProfile.text.toString(),
            personalProfileFragment.etEditProfilePhone.text.toString(),
            personalProfileFragment.etSignUpAddressProfile.text.toString(),
            resultId,gender,token,userId,latitude,longitude)

    }

    private fun callApi() {
        viewModel = ViewModelProviders.of(this).get(ProfileViewModel::class.java)

        viewModel.getProfileModel()
            .observe(requireActivity(), object :
                Observer<BaseResponseModel<ProfileModel<AddressProfile>>> {
                override
                fun onChanged(@Nullable profileModel: BaseResponseModel<ProfileModel<AddressProfile>>?) {
                    mWaitingDialog!!.dismiss()
                    if (profileModel != null) {
                        if (profileModel.getCode() == 200) {
                            setData(profileModel.getData())
                        } else {
                            Utility.showSnackBar(
                                personalProfileFragment.tvEpGenderProfile,
                                profileModel.getMessage(),
                                requireContext()
                            )
                        }
                    } else {
                        Utility.showSnackBar(
                            personalProfileFragment.tvEpGenderProfile,
                            getString(R.string.server_error),
                            requireContext()
                        )
                    }
                }
            })
        viewModel.error.observe(requireActivity(), Observer {
            mWaitingDialog!!.dismiss()
            Utility.showSnackBar(
                personalProfileFragment.tvEpGenderProfile, viewModel.error.value.toString(),
                requireContext()
            )
        })

        cateogryViewModel = ViewModelProviders.of(this).get(CategoryViewModel::class.java)

        cateogryViewModel.getCategoryResponse()
            .observe(requireActivity(), object :
                androidx.lifecycle.Observer<BaseResponseArrayModel<CategoryModel>> {
                override
                fun onChanged(@Nullable categoryModel: BaseResponseArrayModel<CategoryModel>?) {
                    mWaitingDialog!!.dismiss()
                    if (categoryModel != null) {
                        if (categoryModel.getCode() == 200) {
                            categoryData = categoryModel.getData()
//                            spinnerAdapter(categoryModel.getData())
                        }
                    } else {
                        Utility.showSnackBar(
                            personalProfileFragment.tvEpGenderProfile,
                            getString(R.string.server_error),
                            requireContext()
                        )
                    }
                }
            })

        cateogryViewModel.error.observe(requireActivity(), androidx.lifecycle.Observer {
            mWaitingDialog!!.dismiss()
            Utility.showSnackBar(
                personalProfileFragment.tvEpGenderProfile, cateogryViewModel.error.value.toString(),
                requireContext()
            )
        })

        updateViewModel = ViewModelProviders.of(this).get(EditProfileViewModel::class.java)

        updateViewModel.getUpdateProfile()
            .observe(requireActivity(), object :
                androidx.lifecycle.Observer<BaseResponseModel<ProfileModel<AddressProfile>>> {
                override
                fun onChanged(@Nullable categoryModel: BaseResponseModel<ProfileModel<AddressProfile>>?) {
                    mWaitingDialog!!.dismiss()
                    if (categoryModel != null) {
                        if (categoryModel.getCode() == 200) {
                            Utility.showSnackBar(
                                personalProfileFragment.tvEpGenderProfile,
                                categoryModel.getMessage(),
                                requireContext()
                            )
                            Handler().postDelayed({
                                getActivity()!!.onBackPressed();
                            }, 1000)
                        }else{

                            Utility.showSnackBar(
                                personalProfileFragment.tvEpGenderProfile,
                                categoryModel.getMessage(),
                                requireContext()
                            )
                        }
                    } else {
                        Utility.showSnackBar(
                            personalProfileFragment.tvEpGenderProfile,
                            getString(R.string.server_error),
                            requireContext()
                        )
                    }
                }
            })

        updateViewModel.error.observe(requireActivity(), androidx.lifecycle.Observer {
            mWaitingDialog!!.dismiss()
            Utility.showSnackBar(
                personalProfileFragment.tvEpGenderProfile, updateViewModel.error.value.toString(),
                requireContext()
            )
        })
    }

    private fun setData(data: ProfileModel<AddressProfile>?) {
        personalProfileFragment.etSignUpNameProfile.setText(data!!.username)
        personalProfileFragment.etSignUpEmailProfile.setText(data!!.email)
        personalProfileFragment.etEditProfilePhone.setText(data!!.phone_number)
        personalProfileFragment.etSignUpAgeProfile.setText(data!!.age.toString())
        personalProfileFragment.etSignUpAddressProfile.setText(data!!.address!!.address)
        latitude = data!!.address!!.latitude.toDouble()
        longitude = data!!.address!!.longitude.toDouble()
        if (data.gender.equals("Male", true)) {
            personalProfileFragment.rbMaleProfile.isChecked = true
            personalProfileFragment.rbMaleProfile.setTextColor(resources.getColor(R.color.colorPrimary))
            personalProfileFragment.rbFemaleProfile.setTextColor(resources.getColor(R.color.grey))
        } else if (data.gender.equals("Female", true)) {
            personalProfileFragment.rbFemaleProfile.isChecked = true
            personalProfileFragment.rbMaleProfile.setTextColor(resources.getColor(R.color.grey))
            personalProfileFragment.rbFemaleProfile.setTextColor(resources.getColor(R.color.colorPrimary))
        }

        val jobTypeData = ArrayList<String>()
        val jobId = ArrayList<String>()
        for (i in 0 until data.jobcategories.size) {
            jobId.add(data.jobcategories.get(i).id.toString())
            jobTypeData.add(data.jobcategories.get(i).category_name)
        }
        resultId = jobId.joinToString(",")
        result = jobTypeData.joinToString(", ")
        personalProfileFragment.etSignUpJobTypeProfile.setText(result)
    }

    private fun callAutoSearch() {
        val fields =
            listOf(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG)
        val intent = Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY, fields)
            .build(requireContext())
        startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                val place = Autocomplete.getPlaceFromIntent(data!!)
                latitude = place.latLng!!.latitude
                longitude = place.latLng!!.longitude
                Log.i(
                    "FragmentActivity.TAG",
                    "Place: " + place.name + ", " + place.latLng + ", " + place.address
                )
                val address = place.address

                personalProfileFragment.etSignUpAddressProfile.setText(address.toString())
                // do query with address
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                val status: Status = Autocomplete.getStatusFromIntent(data!!)
                Toast.makeText(
                    requireContext(),
                    "Error: " + status.getStatusMessage(),
                    Toast.LENGTH_LONG
                ).show()
                Log.i("FragmentActivity.TAG", status.getStatusMessage())
            } else if (resultCode == Activity.RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }
}
