package com.ekoplug.service.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.Nullable
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager

import com.ekoplug.model.MyBookingModel
import com.ekoplug.service.R
import com.ekoplug.service.adapters.UpcomingAdapters
import com.ekoplug.service.basesetup.BaseResponseArrayModel
import com.ekoplug.service.databinding.UpcomingFragmentBinding
import com.ekoplug.service.model.MyBookingListModel
import com.ekoplug.service.viewmodel.UpcomingViewModel
import com.ekoplug.utils.AppPreference
import com.ekoplug.utils.Utility
import com.ekoplug.utils.Watting
import kotlinx.android.synthetic.main.upcoming_fragment.*

class UpcomingFragment : Fragment() {

    companion object {
        fun newInstance() = UpcomingFragment()
    }

    private lateinit var viewModel: UpcomingViewModel
    lateinit var upcomingFragmentBinding : UpcomingFragmentBinding
    private var mWaitingDialog: Watting? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        upcomingFragmentBinding = DataBindingUtil.inflate(inflater,
            R.layout.upcoming_fragment, container, false)
        mWaitingDialog = Watting(requireContext(), "")
        val token = AppPreference.getPreferenceValueByKey(requireActivity(), "Token").toString()
        callApi()
        viewModel.setMyBookingData(token)
        return upcomingFragmentBinding.root
    }

    private fun callApi() {
        viewModel = ViewModelProviders.of(this).get(UpcomingViewModel::class.java)
        
        viewModel.getMyBookingData()
            .observe(requireActivity(), object : Observer<BaseResponseArrayModel<MyBookingListModel>> {
                override
                fun onChanged(@Nullable mybookingModel: BaseResponseArrayModel<MyBookingListModel>?) {
                    mWaitingDialog!!.dismiss()
                    if (mybookingModel != null) {

                        if (mybookingModel.getCode() == 200) {
                            if (mybookingModel.getData()!!.isNotEmpty()) {
                                setAdapterData(mybookingModel.getData()!!)
                                upcomingFragmentBinding.tvUpcomingBookingNotFound.visibility = View.GONE
                                upcomingFragmentBinding.rvUpcomingBooking.visibility = View.VISIBLE
                            } else {
                                upcomingFragmentBinding.rvUpcomingBooking.visibility = View.GONE
                                upcomingFragmentBinding.tvUpcomingBookingNotFound.visibility = View.VISIBLE
                            }
                        } else {
                            Utility.showSnackBar(
                                rv_upcomingBooking,
                                mybookingModel.getMessage(),
                                requireContext()
                            )
                        }

                    } else {
                        Utility.showSnackBar(
                            rv_upcomingBooking,
                            getString(R.string.server_error),
                            requireContext()
                        )
                    }
                }
            })

        viewModel.error.observe(requireActivity(), Observer {
            mWaitingDialog!!.dismiss()
            Utility.showSnackBar(
                rv_upcomingBooking, viewModel.error.value.toString(),
                requireContext()
            )
        })
    }

    private fun setAdapterData(data: ArrayList<MyBookingListModel>) {
        upcomingFragmentBinding.rvUpcomingBooking.layoutManager = LinearLayoutManager(requireContext())
        val bookingAdapter = UpcomingAdapters(requireContext(),data)
        upcomingFragmentBinding.rvUpcomingBooking.adapter = bookingAdapter
    }

}
