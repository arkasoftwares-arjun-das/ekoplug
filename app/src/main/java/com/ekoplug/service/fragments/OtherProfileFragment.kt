package com.ekoplug.service.fragments

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.media.MediaScannerConnection
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.Nullable
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.ekoplug.model.AddressProfile
import com.ekoplug.model.ProfileModel
import com.ekoplug.service.R
import com.ekoplug.service.basesetup.BaseResponseModel
import com.ekoplug.service.databinding.OtherProfileFragmentBinding
import com.ekoplug.service.viewmodel.OtherProfileViewModel
import com.ekoplug.service.viewmodel.ProfileViewModel
import com.ekoplug.utils.AppPreference
import com.ekoplug.utils.Utility
import com.ekoplug.utils.Watting
import com.squareup.picasso.Picasso
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.*

class OtherProfileFragment : Fragment() {

    companion object {
        fun newInstance() = OtherProfileFragment()
    }

    private lateinit var otherViewModel: OtherProfileViewModel
    lateinit var otherProfileBinding: OtherProfileFragmentBinding
    private lateinit var viewModel: ProfileViewModel
    var profileData: ProfileModel<AddressProfile>? = null
    private var mWaitingDialog: Watting? = null
    private var imageUrl: Uri? = null
    private var profilePic: File? = null
    private var addressPic: File? = null
    private var idPic: File? = null
    private var certificatePic: File? = null
    var resultUri: Uri? = null
    var token = ""
    var imageOne = ""
    var imageTwo = ""
    var imageThree = ""
    var userId = 0
    var count = 0

    private lateinit var documentFile: RequestBody
    var documentBody: MultipartBody.Part? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        otherProfileBinding =
            DataBindingUtil.inflate(inflater, R.layout.other_profile_fragment, container, false)
        mWaitingDialog = Watting(requireContext(), "")
        token = AppPreference.getPreferenceValueByKey(requireContext(), "Token").toString()
        userId = AppPreference.getIntPreferenceValueByKey(requireContext(), "Id")!!
        callApi()
        init()
        viewModel.setProfileData(token, userId)
        return otherProfileBinding.root
    }

    private fun init() {
        otherProfileBinding.ivAddressProof.setOnClickListener { v ->
            count = 1
            onSelectImage()
        }
        otherProfileBinding.ivIdProof.setOnClickListener { v ->
            count = 2
            onSelectImage()
        }
        otherProfileBinding.ivCertificateProof.setOnClickListener { v ->
            count = 3
            onSelectImage()
        }
        otherProfileBinding.btnEditProfile.setOnClickListener { v ->
            uploadPictureApi()
        }
    }

    private fun callApi() {
        viewModel = ViewModelProviders.of(this).get(ProfileViewModel::class.java)

        viewModel.getProfileModel()
            .observe(requireActivity(), object :
                Observer<BaseResponseModel<ProfileModel<AddressProfile>>> {
                override
                fun onChanged(@Nullable profileModel: BaseResponseModel<ProfileModel<AddressProfile>>?) {
                    mWaitingDialog!!.dismiss()
                    if (profileModel != null) {
                        if (profileModel.getCode() == 200) {
                            setData(profileModel.getData())
                            profileData = profileModel.getData()
                        } else {
                            Utility.showSnackBar(
                                otherProfileBinding.tvUploadAddressProfile,
                                profileModel.getMessage(),
                                requireContext()
                            )
                        }
                    } else {
                        Utility.showSnackBar(
                            otherProfileBinding.tvUploadAddressProfile,
                            getString(R.string.server_error),
                            requireContext()
                        )
                    }
                }
            })
        viewModel.error.observe(requireActivity(), Observer {
            mWaitingDialog!!.dismiss()
            Utility.showSnackBar(
                otherProfileBinding.tvUploadAddressProfile, viewModel.error.value.toString(),
                requireContext()
            )
        })
        otherViewModel = ViewModelProviders.of(this).get(OtherProfileViewModel::class.java)

        otherViewModel.getUpdateProfile()
            .observe(requireActivity(), object :
                androidx.lifecycle.Observer<BaseResponseModel<ProfileModel<AddressProfile>>> {
                override
                fun onChanged(@Nullable categoryModel: BaseResponseModel<ProfileModel<AddressProfile>>?) {
                    mWaitingDialog!!.dismiss()
                    if (categoryModel != null) {
                        if (categoryModel.getCode() == 200) {
                            Utility.showSnackBar(
                                otherProfileBinding.tvUploadAddressProfile,
                                categoryModel.getMessage(),
                                requireContext()
                            )
                            Handler().postDelayed({
                                getActivity()!!.onBackPressed();
                            }, 1000)
                        } else {
                            Utility.showSnackBar(
                                otherProfileBinding.tvUploadAddressProfile,
                                categoryModel.getMessage(),
                                requireContext()
                            )
                        }
                    } else {
                        Utility.showSnackBar(
                            otherProfileBinding.tvUploadAddressProfile,
                            getString(R.string.server_error),
                            requireContext()
                        )
                    }
                }
            })

        otherViewModel.error.observe(requireActivity(), androidx.lifecycle.Observer {
            mWaitingDialog!!.dismiss()
            Utility.showSnackBar(
                otherProfileBinding.tvUploadAddressProfile, otherViewModel.error.value.toString(),
                requireContext()
            )
        })

    }

    private fun setData(data: ProfileModel<AddressProfile>?) {
        Picasso.get()
            .load(Utility.imageURL + "" + data!!.document!!.address_proof)
            .into(otherProfileBinding.imgOne)
        Picasso.get()
            .load(Utility.imageURL + "" + data!!.document!!.immunization_proof)
            .into(otherProfileBinding.imgTwo)
        Picasso.get()
            .load(Utility.imageURL + "" + data!!.document!!.certificates)
            .into(otherProfileBinding.imgThree)
    }


    private fun uploadPictureApi() {
        val builder = MultipartBody.Builder()
        builder.setType(MultipartBody.FORM)

        if (!imageOne.equals("") && !imageTwo.equals("") && !imageThree.equals("")) {
            if (!addressPic!!.equals("") && !idPic!!.equals("") && !certificatePic!!.equals("")) {
                builder.addFormDataPart("documents[address_proof]", addressPic!!.name)
                builder.addFormDataPart("documents[immunization_proof]", idPic!!.name)
                builder.addFormDataPart("documents[certificates]", certificatePic!!.name)

                val addressfile = addressPic
                val idfile = idPic
                val certificatefile = certificatePic
                val requestFile: RequestBody = RequestBody.create(
                    MediaType.parse("multipart/form-data"), addressfile!!
                )
                val multipartBody = MultipartBody.Part.createFormData(
                    "document[address_proof]", addressPic!!.name, requestFile
                )
                val requestFile2: RequestBody = RequestBody.create(
                    MediaType.parse("multipart/form-data"),
                    idfile!!
                )
                val multipartBody2 = MultipartBody.Part.createFormData(
                    "documents[immunization_proof]", idPic!!.name, requestFile2
                )
                val requestFile3: RequestBody = RequestBody.create(
                    MediaType.parse("multipart/form-data"),
                    certificatefile!!
                )
                val multipartBody3 = MultipartBody.Part.createFormData(
                    "document[certificates]", certificatePic!!.name, requestFile3
                )

                builder.addPart(multipartBody)
                builder.addPart(multipartBody2)
                builder.addPart(multipartBody3)
            }
            val requestBody: MultipartBody = builder.build()
            mWaitingDialog!!.show()
            otherViewModel.setUpdateProfileOther(token, userId, requestBody)
        }
        else if (!imageOne.equals("") && !imageTwo.equals("")) {
            builder.addFormDataPart("documents[address_proof]", addressPic!!.name)
            builder.addFormDataPart("documents[immunization_proof]", idPic!!.name)

            val addressfile = addressPic
            val idfile = idPic
            val requestFile: RequestBody = RequestBody.create(
                MediaType.parse("multipart/form-data"), addressfile!!
            )
            val multipartBody = MultipartBody.Part.createFormData(
                "document[address_proof]", addressPic!!.name, requestFile
            )
            val requestFile2: RequestBody = RequestBody.create(
                MediaType.parse("multipart/form-data"),
                idfile!!
            )
            val multipartBody2 = MultipartBody.Part.createFormData(
                "documents[immunization_proof]", idPic!!.name, requestFile2
            )

            builder.addPart(multipartBody)
            builder.addPart(multipartBody2)

            val requestBody: MultipartBody = builder.build()
            mWaitingDialog!!.show()
            otherViewModel.setUpdateProfileOther(token, userId, requestBody)

        }
        else if (!imageOne.equals("") && !imageThree.equals("")) {
            builder.addFormDataPart("documents[address_proof]", addressPic!!.name)
            builder.addFormDataPart("documents[certificates]", certificatePic!!.name)
            val addressfile = addressPic
            val certificatefile = certificatePic
            val requestFile: RequestBody = RequestBody.create(
                MediaType.parse("multipart/form-data"), addressfile!!
            )
            val multipartBody = MultipartBody.Part.createFormData(
                "document[address_proof]", addressPic!!.name, requestFile
            )
            val requestFile3: RequestBody = RequestBody.create(
                MediaType.parse("multipart/form-data"),
                certificatefile!!
            )
            val multipartBody3 = MultipartBody.Part.createFormData(
                "document[certificates]", certificatePic!!.name, requestFile3
            )

            builder.addPart(multipartBody)
            builder.addPart(multipartBody3)

            val requestBody: MultipartBody = builder.build()
            mWaitingDialog!!.show()
            otherViewModel.setUpdateProfileOther(token, userId, requestBody)
        }
        else if (!imageTwo.equals("") && !imageThree.equals("")) {

            builder.addFormDataPart("documents[immunization_proof]", idPic!!.name)
            builder.addFormDataPart("documents[certificates]", certificatePic!!.name)
            val idfile = idPic
            val certificatefile = certificatePic

            val requestFile2: RequestBody = RequestBody.create(
                MediaType.parse("multipart/form-data"),
                idfile!!
            )
            val multipartBody2 = MultipartBody.Part.createFormData(
                "documents[immunization_proof]", idPic!!.name, requestFile2
            )

            val requestFile3: RequestBody = RequestBody.create(
                MediaType.parse("multipart/form-data"),
                certificatefile!!
            )
            val multipartBody3 = MultipartBody.Part.createFormData(
                "document[certificates]", certificatePic!!.name, requestFile3
            )

            builder.addPart(multipartBody2)
            builder.addPart(multipartBody3)

            val requestBody: MultipartBody = builder.build()
            mWaitingDialog!!.show()
            otherViewModel.setUpdateProfileOther(token, userId, requestBody)
        }
        else if (!imageTwo.equals("")) {
            builder.addFormDataPart("documents[immunization_proof]", idPic!!.name)

            val idfile = idPic

            val requestFile2: RequestBody = RequestBody.create(
                MediaType.parse("multipart/form-data"),
                idfile!!
            )
            val multipartBody2 = MultipartBody.Part.createFormData(
                "documents[immunization_proof]", idPic!!.name, requestFile2
            )

            builder.addPart(multipartBody2)
            val requestBody: MultipartBody = builder.build()
            mWaitingDialog!!.show()
            otherViewModel.setUpdateProfileOther(token, userId, requestBody)
        }
        else if (!imageThree.equals("")) {

            builder.addFormDataPart("documents[certificates]", certificatePic!!.name)
            val certificatefile = certificatePic

            val requestFile3: RequestBody = RequestBody.create(
                MediaType.parse("multipart/form-data"),
                certificatefile!!
            )
            val multipartBody3 = MultipartBody.Part.createFormData(
                "document[certificates]", certificatePic!!.name, requestFile3
            )

            builder.addPart(multipartBody3)
            val requestBody: MultipartBody = builder.build()
            mWaitingDialog!!.show()
            otherViewModel.setUpdateProfileOther(token, userId, requestBody)

        }
        else if (!imageOne.equals("")) {
            builder.addFormDataPart("documents[address_proof]", addressPic!!.name)
            val addressfile = addressPic

            val requestFile: RequestBody = RequestBody.create(
                MediaType.parse("multipart/form-data"), addressfile!!
            )
            val multipartBody = MultipartBody.Part.createFormData(
                "document[address_proof]", addressPic!!.name, requestFile
            )

            builder.addPart(multipartBody)
            val requestBody: MultipartBody = builder.build()
            mWaitingDialog!!.show()
            otherViewModel.setUpdateProfileOther(token, userId, requestBody)
        }
    }

    private fun onSelectImage() {
        CropImage.startPickImageActivity(activity!!)
    }

    private fun startCropActivity(imageUri: Uri) {
        CropImage.activity(imageUri).setGuidelines(CropImageView.Guidelines.ON)
            .setMultiTouchEnabled(true)
            .setAspectRatio(1, 1)
            .start(activity!!)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            val imageUri = CropImage.getPickImageResultUri(context!!, data)
            if (CropImage.isReadExternalStoragePermissionsRequired(context!!, imageUri)) {
                // mCropImageUri = imageUri
                requestPermissions(
                    listOf(Manifest.permission.READ_EXTERNAL_STORAGE).toTypedArray(),
                    0
                )
            } else {
                startCropActivity(imageUri)
            }
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            val result = CropImage.getActivityResult(data)
            if (resultCode == Activity.RESULT_OK) {
                imageUrl = result.originalUri
                val resultUri = result.originalUri
                println("Image url >>>>>>>>>>>>>$resultUri")
                try {
                    val bitmap =
                        MediaStore.Images.Media.getBitmap(context!!.contentResolver, resultUri)
//                    otherProfileBinding.imgOne.setImageURI(resultUri)
                    val filePath = saveImage(bitmap)
                    if (count == 1) {
                        otherProfileBinding.imgOne.setImageURI(resultUri)
                        addressPic = File(filePath)
                        imageOne = "Upload"
                    } else if (count == 2) {
                        otherProfileBinding.imgTwo.setImageURI(resultUri)
                        idPic = File(filePath)
                        imageTwo = "Upload"
                    } else if (count == 3) {
                        otherProfileBinding.imgThree.setImageURI(resultUri)
                        certificatePic = File(filePath)
                        imageThree = "Upload"
                    }

                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }
        }
    }

    private fun saveImage(myBitmap: Bitmap): String {
        val bytes = ByteArrayOutputStream()
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes)
        val wallpaperDirectory = activity!!.getDir("images", Context.MODE_PRIVATE)

        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs()
        }
        try {
            val f = File(
                wallpaperDirectory,
                ((Calendar.getInstance().timeInMillis).toString() + ".jpg")
            )
            f.createNewFile()
            val fo = FileOutputStream(f)
            fo.write(bytes.toByteArray())
            MediaScannerConnection.scanFile(
                requireContext(),
                arrayOf(f.path),
                arrayOf("image/jpeg"), null
            )
            fo.close()
            return f.absolutePath
        } catch (e1: IOException) {
            e1.printStackTrace()
        }
        return ""
    }


}
