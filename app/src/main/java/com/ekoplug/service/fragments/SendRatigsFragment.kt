package com.ekoplug.fragments

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.Nullable
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.ekoplug.adapters.SendRatingsAdapter
import com.ekoplug.service.R
import com.ekoplug.service.basesetup.BaseResponseArrayModel
import com.ekoplug.service.databinding.SendRatigsFragmentBinding
import com.ekoplug.service.model.SubmitReviewModel
import com.ekoplug.utils.AppPreference
import com.ekoplug.utils.Utility
import com.ekoplug.utils.Watting
import com.ekoplug.viewmodel.SendRatigsViewModel
import kotlinx.android.synthetic.main.send_ratigs_fragment.*

class SendRatigsFragment : Fragment() {


    private lateinit var viewModel: SendRatigsViewModel
    lateinit var sendRatigsFragmentBinding: SendRatigsFragmentBinding
    private var mWaitingDialog: Watting? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        sendRatigsFragmentBinding = DataBindingUtil.inflate(inflater,
            R.layout.send_ratigs_fragment, container, false)
        mWaitingDialog = Watting(requireContext(), "")
        val token = AppPreference.getPreferenceValueByKey(requireActivity(), "Token").toString()
        callApi()
        mWaitingDialog!!.show()
        viewModel.setSendRatingsData(token) 
        return sendRatigsFragmentBinding.root
    }

    private fun callApi() {
        viewModel = ViewModelProviders.of(this).get(SendRatigsViewModel::class.java)

        viewModel.getSendRatingsData()
            .observe(requireActivity(), object : Observer<BaseResponseArrayModel<SubmitReviewModel>> {
                override
                fun onChanged(@Nullable sendRatingsModel: BaseResponseArrayModel<SubmitReviewModel>?) {
                    mWaitingDialog!!.dismiss()
                    if (sendRatingsModel != null) {

                        if (sendRatingsModel.getCode() == 200) {

                            if(sendRatingsModel.getData().isNullOrEmpty()){
                                sendRatigsFragmentBinding.rvSendRatings.visibility = View.GONE
                                sendRatigsFragmentBinding.tvSendRatings.visibility = View.VISIBLE
                            }else{
                                sendRatigsFragmentBinding.rvSendRatings.visibility = View.VISIBLE
                                sendRatigsFragmentBinding.tvSendRatings.visibility = View.GONE
                                setAdapters(sendRatingsModel.getData())
                            }


                        } else {
                            Utility.showSnackBar(
                                tv_sendRatings,
                                sendRatingsModel.getMessage(),
                                requireContext()
                            )
                        }

                    } else {
                        Utility.showSnackBar(
                            tv_sendRatings,
                            getString(R.string.server_error),
                            requireContext()
                        )
                    }
                }
            })

        viewModel.error.observe(requireActivity(), Observer {
            mWaitingDialog!!.dismiss()
            Utility.showSnackBar(
                tv_sendRatings, viewModel.error.value.toString(),
                requireContext()
            )
        })
    }

    private fun setAdapters(data: ArrayList<SubmitReviewModel>?) {
        sendRatigsFragmentBinding.rvSendRatings.layoutManager =
            LinearLayoutManager(requireContext())
        var sendRatingAdpater = SendRatingsAdapter(requireContext(), data)
        sendRatigsFragmentBinding.rvSendRatings.adapter = sendRatingAdpater
    }

}
