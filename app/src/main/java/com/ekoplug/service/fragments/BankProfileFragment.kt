package com.ekoplug.service.fragments

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.Nullable
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.ekoplug.model.AddressProfile
import com.ekoplug.model.ProfileModel
import com.ekoplug.service.R
import com.ekoplug.service.basesetup.BaseResponseModel
import com.ekoplug.service.databinding.BankProfileFragmentBinding
import com.ekoplug.service.viewmodel.EditProfileViewModel
import com.ekoplug.service.viewmodel.ProfileViewModel
import com.ekoplug.utils.AppPreference
import com.ekoplug.utils.Utility
import com.ekoplug.utils.Watting

class BankProfileFragment : Fragment() {

    companion object {
        fun newInstance() = BankProfileFragment()
    }

    lateinit var updateViewModel: EditProfileViewModel
    lateinit var bankProfileBinding: BankProfileFragmentBinding
    private lateinit var viewModel: ProfileViewModel
    private var mWaitingDialog: Watting? = null
    var token = ""
    var userId = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        bankProfileBinding = DataBindingUtil.inflate(inflater, R.layout.bank_profile_fragment, container, false)
        mWaitingDialog = Watting(requireContext(), "")
        token = AppPreference.getPreferenceValueByKey(requireContext(), "Token").toString()
        userId = AppPreference.getIntPreferenceValueByKey(requireContext(), "Id")!!
        callApi()
        init()
        viewModel.setProfileData(token, userId)
        return bankProfileBinding.root
    }

    private fun init() {
        bankProfileBinding.btnSignUpProfile.setOnClickListener { v ->
            mWaitingDialog!!.show()
            updateViewModel.setUpdateProfileBank(
                token, userId,
                bankProfileBinding.etHolderNameProfile.text.toString(),
                bankProfileBinding.etHolderBankProfile.text.toString(),
                bankProfileBinding.etHolderAccountProfile.text.toString()
            )
        }
        bankProfileBinding.etHolderNameProfile.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                bankProfileBinding.etHolderNameProfile.setCompoundDrawablesWithIntrinsicBounds(
                    ContextCompat.getDrawable(
                        requireContext(),
                        R.drawable.profile_blue
                    ),
                    null,
                    null,
                    null
                )
                bankProfileBinding.etHolderNameProfile.setBackgroundResource(R.drawable.login_textview_blue)
                bankProfileBinding.etHolderNameProfile.setTextColor(resources.getColor(R.color.colorPrimary))
                bankProfileBinding.etHolderNameProfile.setHintTextColor(resources.getColor(R.color.colorPrimary))

            } else {
                bankProfileBinding.etHolderNameProfile.setCompoundDrawablesWithIntrinsicBounds(
                    ContextCompat.getDrawable(
                        requireContext(),
                        R.drawable.profile_grey
                    ),
                    null,
                    null,
                    null
                )
                bankProfileBinding.etHolderNameProfile.setBackgroundResource(R.drawable.login_textview_grey)
                bankProfileBinding.etHolderNameProfile.setTextColor(resources.getColor(R.color.grey))
                bankProfileBinding.etHolderNameProfile.setHintTextColor(resources.getColor(R.color.grey))
            }
        }
        bankProfileBinding.etHolderBankProfile.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                bankProfileBinding.etHolderBankProfile.setCompoundDrawablesWithIntrinsicBounds(
                    ContextCompat.getDrawable(
                        requireContext(),
                        R.drawable.bank_blue
                    ),
                    null,
                    null,
                    null
                )
                bankProfileBinding.etHolderBankProfile.setBackgroundResource(R.drawable.login_textview_blue)
                bankProfileBinding.etHolderBankProfile.setTextColor(resources.getColor(R.color.colorPrimary))
                bankProfileBinding.etHolderBankProfile.setHintTextColor(resources.getColor(R.color.colorPrimary))

            } else {
                bankProfileBinding.etHolderBankProfile.setCompoundDrawablesWithIntrinsicBounds(
                    ContextCompat.getDrawable(
                        requireContext(),
                        R.drawable.bank_grey
                    ),
                    null,
                    null,
                    null
                )
                bankProfileBinding.etHolderBankProfile.setBackgroundResource(R.drawable.login_textview_grey)
                bankProfileBinding.etHolderBankProfile.setTextColor(resources.getColor(R.color.grey))
                bankProfileBinding.etHolderBankProfile.setHintTextColor(resources.getColor(R.color.grey))
            }
        }
        bankProfileBinding.etHolderAccountProfile.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                bankProfileBinding.etHolderAccountProfile.setCompoundDrawablesWithIntrinsicBounds(
                    ContextCompat.getDrawable(
                        requireContext(),
                        R.drawable.account_blue
                    ),
                    null,
                    null,
                    null
                )
                bankProfileBinding.etHolderAccountProfile.setBackgroundResource(R.drawable.login_textview_blue)
                bankProfileBinding.etHolderAccountProfile.setTextColor(resources.getColor(R.color.colorPrimary))
                bankProfileBinding.etHolderAccountProfile.setHintTextColor(resources.getColor(R.color.colorPrimary))

            } else {
                bankProfileBinding.etHolderAccountProfile.setCompoundDrawablesWithIntrinsicBounds(
                    ContextCompat.getDrawable(
                        requireContext(),
                        R.drawable.account_grey
                    ),
                    null,
                    null,
                    null
                )
                bankProfileBinding.etHolderAccountProfile.setBackgroundResource(R.drawable.login_textview_grey)
                bankProfileBinding.etHolderAccountProfile.setTextColor(resources.getColor(R.color.grey))
                bankProfileBinding.etHolderAccountProfile.setHintTextColor(resources.getColor(R.color.grey))
            }
        }
    }

    private fun callApi() {
        viewModel = ViewModelProviders.of(this).get(ProfileViewModel::class.java)

        viewModel.getProfileModel()
            .observe(requireActivity(), object :
                Observer<BaseResponseModel<ProfileModel<AddressProfile>>> {
                override
                fun onChanged(@Nullable profileModel: BaseResponseModel<ProfileModel<AddressProfile>>?) {
                    mWaitingDialog!!.dismiss()
                    if (profileModel != null) {
                        if (profileModel.getCode() == 200) {
                            setData(profileModel.getData())
                        } else {
                            Utility.showSnackBar(
                                bankProfileBinding.etHolderAccountProfile,
                                profileModel.getMessage(),
                                requireContext()
                            )
                        }
                    } else {
                        Utility.showSnackBar(
                            bankProfileBinding.etHolderAccountProfile,
                            getString(R.string.server_error),
                            requireContext()
                        )
                    }
                }
            })

        viewModel.error.observe(requireActivity(), Observer {
            mWaitingDialog!!.dismiss()
            Utility.showSnackBar(
                bankProfileBinding.etHolderAccountProfile, viewModel.error.value.toString(),
                requireContext()
            )
        })

        updateViewModel = ViewModelProviders.of(this).get(EditProfileViewModel::class.java)

        updateViewModel.getUpdateProfile()
            .observe(requireActivity(), object :
                androidx.lifecycle.Observer<BaseResponseModel<ProfileModel<AddressProfile>>> {
                override
                fun onChanged(@Nullable categoryModel: BaseResponseModel<ProfileModel<AddressProfile>>?) {
                    mWaitingDialog!!.dismiss()
                    if (categoryModel != null) {
                        if (categoryModel.getCode() == 200) {
                            Utility.showSnackBar(
                                bankProfileBinding.etHolderAccountProfile,
                                categoryModel.getMessage(),
                                requireContext()
                            )
                            Handler().postDelayed({
                                getActivity()!!.onBackPressed();
                            }, 1000)

                        } else {
                            Utility.showSnackBar(
                                bankProfileBinding.etHolderAccountProfile,
                                categoryModel.getMessage(),
                                requireContext()
                            )
                        }
                    } else {
                        Utility.showSnackBar(
                            bankProfileBinding.etHolderAccountProfile,
                            getString(R.string.server_error),
                            requireContext()
                        )
                    }
                }
            })

        updateViewModel.error.observe(requireActivity(), androidx.lifecycle.Observer {
            mWaitingDialog!!.dismiss()
            Utility.showSnackBar(
                bankProfileBinding.etHolderAccountProfile, updateViewModel.error.value.toString(),
                requireContext()
            )
        })
    }

    private fun setData(data: ProfileModel<AddressProfile>?) {
        bankProfileBinding.etHolderBankProfile.setText(data!!.bank_detail!!.bank_name)
        bankProfileBinding.etHolderNameProfile.setText(data!!.bank_detail!!.holder_name)
        bankProfileBinding.etHolderAccountProfile.setText(data!!.bank_detail!!.account_number)
    }


}
