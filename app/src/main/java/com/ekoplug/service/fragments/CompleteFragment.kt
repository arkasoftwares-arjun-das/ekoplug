package com.ekoplug.fragments

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.Nullable
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.ekoplug.service.R
import com.ekoplug.service.adapters.CompleteAdapters
import com.ekoplug.service.basesetup.BaseResponseArrayModel
import com.ekoplug.service.databinding.CompleteFragmentBinding
import com.ekoplug.service.model.MyBookingListModel
import com.ekoplug.service.viewmodel.CompleteViewModel
import com.ekoplug.utils.AppPreference
import com.ekoplug.utils.Utility
import com.ekoplug.utils.Watting
import kotlinx.android.synthetic.main.complete_fragment.*

class CompleteFragment : Fragment() {

    companion object {
        fun newInstance() = CompleteFragment()
    }

    private lateinit var viewModel: CompleteViewModel
    lateinit var completeFragmentBinding : CompleteFragmentBinding
    private var mWaitingDialog: Watting? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        completeFragmentBinding= DataBindingUtil.inflate(inflater,
            R.layout.complete_fragment, container, false)
        mWaitingDialog = Watting(requireContext(), "")
        val token = AppPreference.getPreferenceValueByKey(requireActivity(), "Token").toString()
        callApi()
        mWaitingDialog!!.show()
        viewModel.setMyBookingCompleteData(token)
        return completeFragmentBinding.root
    }

    private fun callApi() {
        viewModel = ViewModelProviders.of(this).get(CompleteViewModel::class.java)

        viewModel.getMyBookingCompleteData()
            .observe(requireActivity(), object : Observer<BaseResponseArrayModel<MyBookingListModel>> {
                override
                fun onChanged(@Nullable mybookingModel: BaseResponseArrayModel<MyBookingListModel>?) {
                    mWaitingDialog!!.dismiss()
                    if (mybookingModel != null) {
                        if (mybookingModel.getCode() == 200) {
                            if (mybookingModel.getData()!!.isNotEmpty()) {
                                setAdapterData(mybookingModel.getData()!!)
                                completeFragmentBinding.tvUpcomingBookingNotFound.visibility = View.GONE
                                completeFragmentBinding.rvCompleteBooking.visibility = View.VISIBLE
                            } else {
                                completeFragmentBinding.rvCompleteBooking.visibility = View.GONE
                                completeFragmentBinding.tvUpcomingBookingNotFound.visibility = View.VISIBLE
                            }
                        } else {
                            Utility.showSnackBar(
                                rv_completeBooking,
                                mybookingModel.getMessage(),
                                requireContext()
                            )
                        }
                    } else {
                        Utility.showSnackBar(
                            rv_completeBooking,
                            getString(R.string.server_error),
                            requireContext()
                        )
                    }
                }
            })

        viewModel.error.observe(requireActivity(), Observer {
            mWaitingDialog!!.dismiss()
            Utility.showSnackBar(
                rv_completeBooking, viewModel.error.value.toString(),
                requireContext()
            )
        })
    }

    private fun setAdapterData(data: ArrayList<MyBookingListModel>) {
        completeFragmentBinding.rvCompleteBooking.layoutManager = LinearLayoutManager(requireContext())
        var bookingAdapter = CompleteAdapters(requireContext(),data)
        completeFragmentBinding.rvCompleteBooking.adapter = bookingAdapter
    }

}
