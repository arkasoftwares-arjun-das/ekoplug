package com.ekoplug.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.Nullable
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.ekoplug.adapters.RecievedRatingsAdapter
import com.ekoplug.service.R
import com.ekoplug.service.basesetup.BaseResponseArrayModel
import com.ekoplug.service.databinding.RecievedRatingsFragmentBinding
import com.ekoplug.service.model.SubmitReviewModel

import com.ekoplug.utils.AppPreference
import com.ekoplug.utils.Utility
import com.ekoplug.utils.Watting
import com.ekoplug.viewmodel.RecievedRatingsViewModel
import kotlinx.android.synthetic.main.recieved_ratings_fragment.*

class RecievedRatingsFragment : Fragment() {

    companion object {
        fun newInstance() = RecievedRatingsFragment()
    }

    private lateinit var viewModel: RecievedRatingsViewModel
    lateinit var recievedRatingsFragmentBinding: RecievedRatingsFragmentBinding
    private var mWaitingDialog: Watting? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        recievedRatingsFragmentBinding =
            DataBindingUtil.inflate(inflater, R.layout.recieved_ratings_fragment, container, false)
        mWaitingDialog = Watting(requireContext(), "")
        val token = AppPreference.getPreferenceValueByKey(requireActivity(), "Token").toString()
        callApi()
        mWaitingDialog!!.show()
        viewModel.setRecievedRatingsData(token)
        return recievedRatingsFragmentBinding.root
    }

    private fun callApi() {
        viewModel = ViewModelProviders.of(this).get(RecievedRatingsViewModel::class.java)
        viewModel.getRecievedRatingsData()
            .observe(requireActivity(), object : Observer<BaseResponseArrayModel<SubmitReviewModel>> {
                override
                fun onChanged(@Nullable recievedRatings: BaseResponseArrayModel<SubmitReviewModel>?) {
                    mWaitingDialog!!.dismiss()
                    if (recievedRatings != null) {

                        if (recievedRatings.getCode() == 200) {

                            if(recievedRatings.getData().isNullOrEmpty()){
                                recievedRatingsFragmentBinding.rvRecievedRatings.visibility = View.GONE
                                recievedRatingsFragmentBinding.tvRecieverRatings.visibility = View.VISIBLE
                            }else{
                                recievedRatingsFragmentBinding.rvRecievedRatings.visibility = View.VISIBLE
                                recievedRatingsFragmentBinding.tvRecieverRatings.visibility = View.GONE
                                setAdapters(recievedRatings.getData())
                            }
                        } else {
                            Utility.showSnackBar(
                                tv_recieverRatings,
                                recievedRatings.getMessage(),
                                requireContext()
                            )
                        }

                    } else {
                        Utility.showSnackBar(
                            tv_recieverRatings,
                            getString(R.string.server_error),
                            requireContext()
                        )
                    }
                }
            })

        viewModel.error.observe(requireActivity(), Observer {
            mWaitingDialog!!.dismiss()
            Utility.showSnackBar(
                recievedRatingsFragmentBinding.tvRecieverRatings, viewModel.error.value.toString(),
                requireContext()
            )
        })
    }

    private fun setAdapters(data: ArrayList<SubmitReviewModel>?) {
        recievedRatingsFragmentBinding.rvRecievedRatings.layoutManager =
            LinearLayoutManager(requireContext())
        val recievedRatingAdapter = RecievedRatingsAdapter(requireContext(), data)
        recievedRatingsFragmentBinding.rvRecievedRatings.adapter = recievedRatingAdapter
    }

}
