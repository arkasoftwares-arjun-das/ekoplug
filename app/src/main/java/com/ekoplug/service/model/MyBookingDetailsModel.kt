package com.ekoplug.model

data class MyBookingDetailsModel(
    var worker_profile: String = "",
    var worker: String = "stephen",
    var worker_email: String = "",
    var rating: Int = 0,
    var booking_status: String = "",
    var job_category: Int = 0,
    var appointment_date: String = "",
    var appointment_time: String = "",
    var latitude: String = "",
    var longitude: String = ""
)
