package com.ekoplug.model

data class MyBookingModel(
    var id : Int = 0,
    var worker: String = "",
    var worker_profile: String = "",
    var appointment_date: String = "",
    var appointment_time: String = "",
    var booking_status: String = "",
    var job_category : String =  ""
)