package com.ekoplug.service.model

import java.io.File
import java.io.Serializable

data class SignUpResquestModel(
    var username: String = "",
    var email: String = "",
    var phone_number: String = "",
    var gender: String = "",
    var age: String = "",
    var password: String = "",
    var confirm_password: String = "",
    var address: String = "",
    var zip_code: String = "",
    var city: String = "",
    var state: String = "",
    var latitude: Double = 0.0,
    var longitude: Double = 0.0,
    var holder_name: String = "",
    var bank_name: String = "",
    var account_number: String = "",
    var profile_image: File? = null,
    var jobcategories: String = ""
) : Serializable