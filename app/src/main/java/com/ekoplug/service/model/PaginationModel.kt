package com.ekoplug.model

import com.ekoplug.service.model.CategoryModel
import java.io.Serializable

class PaginationModel(val limit:Int,
                      val current_page : Int,
                      val total_page: Int,
                      val total_records : Int,
                      val categories: ArrayList<CategoryModel>?) : Serializable {
   /* private var limit: Int = 0
    private var current_page : Int = 0
    private var total_page: Int = 0
    private var total_records : Int = 0
    private var categories: ArrayList<CategoryModel>? = null


    fun getLimit(): Int {
        return limit
    }

    fun setLimit(limit: Int) {
        this.limit = limit
    }

    fun getTotalRecords(): Int {
        return total_records
    }

    fun setTotalRecords(total_records: Int) {
        this.total_records = total_records
    }

    fun getCategories(): List<T>? {
        return categories
    }

    fun setCategories(categories: ArrayList<T>) {
        this.categories = categories
    }

    fun setCurrentPage(current_page : Int){
        this.current_page = current_page
    }

    fun getCurrentPage():Int{
        return current_page
    }

    fun setTotalPage(total_page: Int){
        this.total_page = total_page
    }

    fun getTotalPage():Int
    {
        return total_page
    }*/
}