package com.ekoplug.model


class BookingRequestModel(
    var user: String = "",
    var user_profile: String = "",
    var appointment_date: String = "",
    var appointment_time: String = "",
    var rating: Double = 0.0,
    var job_category : String = "",
    var id :Int = 0
) {
}

