package com.ekoplug.service.model

class MyBookingListModel(
    val id: Int = 0,
    var user_id : Int = 0,
    val user: String = "",
    val user_profile: String = "",
    val appointment_date: String = "",
    val appointment_time: String = "",
    val job_category: String = "",
    val rating: Int = 0,
    var email: String = "",
    var phone_number: String = "",
    var visit_address: String = "",
    var additional_charge: Double = 0.0,
    var additional_charge_desc: String = "",
    var estimated: Boolean = false,
    var time_duration: String = "",
    var additional_amount_paid: Boolean = false,
    var additional_pay_method: String = "",
    var additional_pay_amount: String = ""
) {
}