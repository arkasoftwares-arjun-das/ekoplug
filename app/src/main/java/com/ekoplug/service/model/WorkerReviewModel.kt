package com.ekoplug.model

data class WorkerReviewModel(
    var user: String = "",
    var rating: Double = 0.0,
    var profile_image: String = "",
    var comment: String = "",
    var username : String = ""
)