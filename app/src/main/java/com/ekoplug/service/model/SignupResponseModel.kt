package com.ekoplug.service.model

data class SignupResponseModel(
    val id: Int,
    val username: String,
    val email: String,
    val profile_image: String,
    val phone_number: String,
    val password: String,
    val otp: String,
    val age: Int,
    val gender : String
)