package com.ekoplug.service.model

class BookingDetailsModel(
    var id: Int = 0,
    var user_profile: String = "",
    var user_id : Int = 0,
    var user_name: String = "",
    var gender: String = "",
    var appointment_date: String = "",
    var appointment_time: String = "",
    var rating: Double = 0.0
) {
}