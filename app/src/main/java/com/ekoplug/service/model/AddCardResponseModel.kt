package com.ekoplug.model

data class AddCardResponseModel(
    var id: Int = 0,
    var user: Int = 0,
    var card_number: String = "",
    var card_name: String = "",
    var expiry_month: Int = 3,
    var expiry_year: Int = 2021,
    var selectedItem: Boolean = false,
    var primaryItem: Boolean = false
)