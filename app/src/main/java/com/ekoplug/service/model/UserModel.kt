package com.ekoplug.model


data class UserModel( val username: String?,
                      var email: String?,
                      var phone_number: String?,
                      var password: String?,
                      var confirm_password: String?,
                      var address: String?,
                      var zip_code: String?,
                      var city: String?,
                      var state: String?,
                      var country: String?,
                      var latitude: Double?,
                      var longitude: Double?)
//
//@SerializedName("username")
//@Expose
//var username: String? = null
//
//@SerializedName("email")
//@Expose
//var email: String? = null
//
//@SerializedName("phone_number")
//@Expose
//var phone_number: String? = null
//
//@SerializedName("password")
//@Expose
//var password: String? = null
//
//@SerializedName("confirm_password")
//@Expose
//var confirm_password: String? = null
//
//@SerializedName("address")
//@Expose
//var address: String? = null
//
//@SerializedName("zip_code")
//@Expose
//var zip_code: String? = null
//
//@SerializedName("city")
//@Expose
//var city: String? = null
//
//@SerializedName("state")
//@Expose
//var state: String? = null
//
//@SerializedName("country")
//@Expose
//var country: String? = null
//
//@SerializedName("latitude")
//@Expose
//var latitude: Double? = null
//
//@SerializedName("longitude")
//@Expose
//var longitude: Double? = null
