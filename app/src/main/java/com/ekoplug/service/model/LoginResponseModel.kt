package com.ekoplug.service.model

data class LoginResponseModel(
    var id: Int,
    var auth_token: String,
    var role: String,
    var username: String,
    var email: String,
    var phone_number: String,
    var ratings: String,
    var is_active: Boolean = true
)