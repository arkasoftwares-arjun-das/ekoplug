package com.ekoplug.model

import com.google.firebase.firestore.PropertyName
import java.io.Serializable
import java.util.*

 class ChatModel(
    var senderId: Int = 0,
     var timestamp: Date,
     var message :String = ""
    ) : Serializable{

}