package com.ekoplug.model

data class NotificationModel(
    var id: Int = 0,
    var sender_name: String = "",
    var created_at: String = "",
    var notification_title: String = "",
    var profile_image: String = "",
    var username: String = "",
    var job_category: String = "",
    var rating: Int = 0,
    var appointment_date: String = "",
    var appointment_time: String = ""
)