package com.ekoplug.model

data class OfferModel(
    var id: Int = 0,
    var offer_title: String = "",
    var short_detail: String = "",
    var offer_type: String = "",
    var category: String = "",
    var offer_detail: String = "",
    var start_from: String = "",
    var end_at: String = "",
    var percent_off: Double = 0.0,
    var max_usage: Int = 0,
    var created_at: String = "",
    var updated_at: String = "",
    var selectedItem :Boolean = false
)