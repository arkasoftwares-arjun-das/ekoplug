package com.ekoplug.service.model

import java.io.Serializable


class CategoryModel(val id:Int, val category_name:String, val category_image:String):Serializable