package com.ekoplug.service.model

data class OtpVerifyResponseModel(
    val id: String,
    val auth_token: String,
    val role: String,
    val username: String,
    val email: String,
    val phone_number: String,
    val ratings: String
)