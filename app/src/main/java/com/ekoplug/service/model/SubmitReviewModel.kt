package com.ekoplug.service.model

class SubmitReviewModel(
    var profile_image: String = "",
    var username: String = "",
    var rating: Double = 0.0,
    var comment: String = "",
    var created_at: String = "",
    var category : ArrayList<Category>? = null
){}

class Category(
    var category_name: String = ""
)