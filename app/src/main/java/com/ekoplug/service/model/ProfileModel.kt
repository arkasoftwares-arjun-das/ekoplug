package com.ekoplug.model

import java.io.Serializable

data class ProfileModel<T>(
    var id: Int,
    var profile_image: String,
    var auth_token: String,
    var role: String,
    var username: String,
    var email: String,
    var phone_number: String,
    var ratings: Double,
    var gender: String,
    var age: Int = 0,
    var first_name: String = "",
    var last_name: String = "",
    var job_completed: Int = 0,
    var address: T? = null,
    var bank_detail: BankDetails? = null,
    var document: Document? = null,
    var jobcategories: ArrayList<JobCategories>
) : Serializable

data class JobCategories(
    var category_name: String = "",
    var id: Int = 0
) : Serializable

data class BankDetails(
    var id: Int = 0,
    var holder_name: String = "",
    var bank_name: String = "",
    var account_number: String = "",
    var set_primary: Boolean = false,
    var created_at: String = "",
    var updated_at: String = "",
    var worker: Int = 15
) : Serializable

data class Document(
    var id: Int = 0,
    var address_proof: String = "",
    var criminal_record: String = "",
    var immunization_proof: String = "",
    var updated_at: String = "",
    var certificates: String = "",
    var created_at: String = "",
    var worker: Int = 15
) : Serializable

data class AddressProfile(
    var id: Int = 0,
    var address: String = "",
    var zip_code: String = "",
    var city: String = "",
    var state: String = "",
    var country: String = "",
    var reference_point: String = "",
    var latitude: String = "",
    var longitude: String = "",
    var user: Int = 0
)