package com.ekoplug.model

data class StaticPageModel(
    val id : Int = 0,
    val template_name :String = "",
    val short_description : String = "",
    val content : String = ""
)