package com.ekoplug.model

import com.google.firebase.Timestamp
import java.sql.Time

class ChatFirebaseModel {
    var msg: String = ""
    var senderId: Int = 0
    var timestamp : Timestamp? = null
}