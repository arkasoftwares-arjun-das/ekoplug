package com.ekoplug.model

import java.io.Serializable
import java.util.ArrayList

data class PaymentHistoryModel(
    var created_at: String = "",
    var booking: ArrayList<Booking>?= null,
    var worker: Int = 0,
    var payment_mode: String = "",
    var amount: Double = 0.0,
    var updated_at : String = "",
    var status : String = "",
    var transaction_id : String = "",
    var id :Int = 0
    )

class Booking(
    var id: Int = 0,
    var user: String = "",
    var email: String = "",
    var phone_number: String = "",
    var user_profile: String = "",
    var appointment_date: String = "",
    var appointment_time: String = "",
    var job_category: String = "",
    var visit_address: String = "",
    var rating: Int = 0,
    var additional_charge: Double = 0.0,
    var additional_charge_desc: String = "",
    var estimated: Boolean = false,
    var time_duration: String = "",
    var additional_amount_paid: Boolean =  false,
    var additional_pay_method: String = "",
    var additional_pay_amount: Double = 0.0
) : Serializable