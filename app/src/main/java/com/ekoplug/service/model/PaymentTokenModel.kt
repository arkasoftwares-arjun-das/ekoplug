package com.ekoplug.model

data class PaymentTokenModel(
    var token : String = ""
)