package com.ekoplug.model

data class UpdateProfileMdel(
    var id: Int,
    var profile_image: String,
    var auth_token: String,
    var role: String,
    var username: String,
    var email: String,
    var phone_number: String,
    var ratings: Double,
    var gender : String
)