package com.ekoplug.service.model

data class HomeModel(
    var id: Int = 0,
    var username: String = "",
    var booking_request: Int = 0,
    var current_jobs: Int = 0
)