package com.ekoplug.service.fcm

import com.ekoplug.utils.AppPreference
import com.google.firebase.messaging.FirebaseMessagingService

class FirebaseIhtnstanceIDService : FirebaseMessagingService() {
    override fun onNewToken(newToken: String) {
        super.onNewToken(newToken)
        System.out.println("=== inside Firebase messaging 2==="+newToken)
        AppPreference.savePreference(this,"FcmToken",newToken)
    }
}